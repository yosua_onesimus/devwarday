# README #

### Consists of application: ###

* e-Mergency (Medical Region Encyclopedia)
* e-Tourist (Tourism Inside Story)

### What is this repository for? ###

* This repository is used for devwarday purposes
* consisting of @natobing, @adityapradana, @bahrie, @arvitfaruki

### How do I get set up? ###

* [JDK SE 6 u45 (Mandatory)](http://www.oracle.com/technetwork/java/javasebusiness/downloads/java-archive-downloads-javase6-419409.html)
* [MyEclipse 6.0.1 Up (Recommended)](https://www.genuitec.com/products/myeclipse/download/)
* [Eclipse JEE Indigo (Alternative)](https://www.eclipse.org/downloads/)
* [MySQL Server 5.1 (Optional)](http://dev.mysql.com/downloads/mysql/)
* [Common ZK Library (Mandatory)](https://www.dropbox.com/s/bd88bfvfnmpae69/common-zk-lib.zip?dl=0)
* [ZK Studio 1.0.2 Helios (Recommended)](http://www.zkoss.org/download/zkstudio)

### Who do I talk to? ###

* @yosua_onesimus
* @natobing, @adityapradana, @bahrie, @arvitfaruki