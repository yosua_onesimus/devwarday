-- MySQL Administrator dump 1.4
--
-- ------------------------------------------------------
-- Server version	5.1.30-community


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


--
-- Create schema e_emergency
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ e_emergency;
USE e_emergency;

--
-- Table structure for table `e_emergency`.`ee_doctor_ratings`
--

DROP TABLE IF EXISTS `ee_doctor_ratings`;
CREATE TABLE `ee_doctor_ratings` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `logChangedBy` varchar(32) DEFAULT NULL,
  `logChangedOn` datetime DEFAULT NULL,
  `logCreatedBy` varchar(32) DEFAULT NULL,
  `logCreatedOn` datetime DEFAULT NULL,
  `logSaveTimes` int(11) DEFAULT NULL,
  `raterId` varchar(40) DEFAULT NULL,
  `rating` int(11) DEFAULT NULL,
  `remarks` varchar(255) DEFAULT NULL,
  `doctor_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKFA45B8D55C3478B3` (`doctor_id`),
  CONSTRAINT `FKFA45B8D55C3478B3` FOREIGN KEY (`doctor_id`) REFERENCES `ee_doctors` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `e_emergency`.`ee_doctor_ratings`
--

/*!40000 ALTER TABLE `ee_doctor_ratings` DISABLE KEYS */;
/*!40000 ALTER TABLE `ee_doctor_ratings` ENABLE KEYS */;


--
-- Table structure for table `e_emergency`.`ee_doctors`
--

DROP TABLE IF EXISTS `ee_doctors`;
CREATE TABLE `ee_doctors` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `active` bit(1) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `code` varchar(8) DEFAULT NULL,
  `email` varchar(32) DEFAULT NULL,
  `fax` varchar(16) DEFAULT NULL,
  `logChangedBy` varchar(32) DEFAULT NULL,
  `logChangedOn` datetime DEFAULT NULL,
  `logCreatedBy` varchar(32) DEFAULT NULL,
  `logCreatedOn` datetime DEFAULT NULL,
  `logSaveTimes` int(11) DEFAULT NULL,
  `name` varchar(64) DEFAULT NULL,
  `partnership` bit(1) DEFAULT NULL,
  `phone` varchar(16) DEFAULT NULL,
  `photo` longblob,
  `photoName` varchar(255) DEFAULT NULL,
  `strDate` date DEFAULT NULL,
  `strNumber` varchar(32) DEFAULT NULL,
  `totalRater` int(11) DEFAULT NULL,
  `totalRating` int(11) DEFAULT NULL,
  `website` varchar(48) DEFAULT NULL,
  `zipCode` varchar(5) DEFAULT NULL,
  `province_id` bigint(20) DEFAULT NULL,
  `regency_id` bigint(20) DEFAULT NULL,
  `specialist_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKD98BA69543C44CF7` (`province_id`),
  KEY `FKD98BA695309752F3` (`specialist_id`),
  KEY `FKD98BA695676C93FD` (`regency_id`),
  CONSTRAINT `FKD98BA695676C93FD` FOREIGN KEY (`regency_id`) REFERENCES `util_regencies` (`id`),
  CONSTRAINT `FKD98BA695309752F3` FOREIGN KEY (`specialist_id`) REFERENCES `ee_specialists` (`id`),
  CONSTRAINT `FKD98BA69543C44CF7` FOREIGN KEY (`province_id`) REFERENCES `util_provinces` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `e_emergency`.`ee_doctors`
--

/*!40000 ALTER TABLE `ee_doctors` DISABLE KEYS */;
INSERT INTO `ee_doctors` (`id`,`active`,`address`,`code`,`email`,`fax`,`logChangedBy`,`logChangedOn`,`logCreatedBy`,`logCreatedOn`,`logSaveTimes`,`name`,`partnership`,`phone`,`photo`,`photoName`,`strDate`,`strNumber`,`totalRater`,`totalRating`,`website`,`zipCode`,`province_id`,`regency_id`,`specialist_id`) VALUES 
 (1,'','','D001','','','admin','2015-04-16 16:19:48','',NULL,1,'Karuniawan Purwanto, Dr., SpBO (K)','\0','',NULL,'',NULL,'D001',0,0,'','',13,192,3),
 (2,'','','D002','','','admin','2015-04-16 16:19:48','',NULL,1,'Dohar Tobing, Dr., SpBO','\0','',NULL,'',NULL,'D002',0,0,'','',13,192,3),
 (3,'','','D003','','','admin','2015-04-16 16:19:48','',NULL,1,'Rahim Purba, Dr. SpBS','\0','',NULL,'',NULL,'D003',0,0,'','',13,192,5),
 (4,'','','D004','','','admin','2015-04-16 16:19:48','',NULL,1,'Nia Yuliatri, Dr., MKes, SpBS','\0','',NULL,'',NULL,'D004',0,0,'','',13,192,5),
 (5,'','','D005','','','admin','2015-04-16 16:19:48','',NULL,1,'Henry Boyke Sitompul, Dr., SpB','\0','',NULL,'',NULL,'D005',0,0,'','',13,192,8),
 (6,'','','D006','','','admin','2015-04-16 16:19:48','',NULL,1,'Tommy Halauwet, Dr., SpB','\0','',NULL,'',NULL,'D006',0,0,'','',13,192,8);
INSERT INTO `ee_doctors` (`id`,`active`,`address`,`code`,`email`,`fax`,`logChangedBy`,`logChangedOn`,`logCreatedBy`,`logCreatedOn`,`logSaveTimes`,`name`,`partnership`,`phone`,`photo`,`photoName`,`strDate`,`strNumber`,`totalRater`,`totalRating`,`website`,`zipCode`,`province_id`,`regency_id`,`specialist_id`) VALUES 
 (7,'','','D007','','','admin','2015-04-16 16:19:48','',NULL,1,'dr. Achmad Basuki,Sp.OT','\0','',NULL,'',NULL,'D007',0,0,'','',13,191,3),
 (8,'','','D008','','','admin','2015-04-16 16:19:48','',NULL,1,'dr.Firdaus,Sp.BS','\0','',NULL,'',NULL,'D008',0,0,'','',13,191,5),
 (9,'','','D009','','','admin','2015-04-16 16:19:48','',NULL,1,'dr. Hari P., Sp.BS.','\0','',NULL,'',NULL,'D009',0,0,'','',13,191,5),
 (10,'','','D010','','','admin','2015-04-16 16:19:48','',NULL,1,'dr. Ati, Sp.BThorax','\0','',NULL,'',NULL,'D010',0,0,'','',13,191,6),
 (11,'','','D011','','','admin','2015-04-16 16:19:48','',NULL,1,'dr. Devi Arofah, SpKK','\0','',NULL,'',NULL,'D011',0,0,'','',13,193,13),
 (12,'','','D012','','','admin','2015-04-16 16:19:48','',NULL,1,'dr. I Ketut Sukarata, SpKK','\0','',NULL,'',NULL,'D012',0,0,'','',13,193,13),
 (13,'','','D013','','','admin','2015-04-16 16:19:48','',NULL,1,'dr. Harry Syarif, SpOG','\0','',NULL,'',NULL,'D013',0,0,'','',13,193,15);
INSERT INTO `ee_doctors` (`id`,`active`,`address`,`code`,`email`,`fax`,`logChangedBy`,`logChangedOn`,`logCreatedBy`,`logCreatedOn`,`logSaveTimes`,`name`,`partnership`,`phone`,`photo`,`photoName`,`strDate`,`strNumber`,`totalRater`,`totalRating`,`website`,`zipCode`,`province_id`,`regency_id`,`specialist_id`) VALUES 
 (14,'','','D014','','','admin','2015-04-16 16:19:48','',NULL,1,'dr. Setyo Hermanto, SpOG','\0','',NULL,'',NULL,'D014',0,0,'','',13,193,15),
 (15,'','','D015','','','admin','2015-04-16 16:19:48','',NULL,1,'dr. Caecillia Arimah, SpP','\0','',NULL,'',NULL,'D015',0,0,'','',13,193,19),
 (16,'','','D016','','','admin','2015-04-16 16:19:48','',NULL,1,'dr. Dwi S. Andriasari, SpP','\0','',NULL,'',NULL,'D016',0,0,'','',13,193,19),
 (17,'','','D017','','','admin','2015-04-16 16:19:48','',NULL,1,'dr. Anita Gunawan, MS,Sp','\0','',NULL,'',NULL,'D017',0,0,'','',13,192,28),
 (18,'','','D018','','','admin','2015-04-16 16:19:48','',NULL,1,'dr. Liliani Wahyu, SpM','\0','',NULL,'',NULL,'D018',0,0,'','',13,192,14),
 (19,'','','D019','','','admin','2015-04-16 16:19:48','',NULL,1,'dr. Elfrita, Sp.AK','\0','',NULL,'',NULL,'D019',0,0,'','',13,193,29);
INSERT INTO `ee_doctors` (`id`,`active`,`address`,`code`,`email`,`fax`,`logChangedBy`,`logChangedOn`,`logCreatedBy`,`logCreatedOn`,`logSaveTimes`,`name`,`partnership`,`phone`,`photo`,`photoName`,`strDate`,`strNumber`,`totalRater`,`totalRating`,`website`,`zipCode`,`province_id`,`regency_id`,`specialist_id`) VALUES 
 (20,'','','D020','','','admin','2015-04-16 16:19:48','',NULL,1,'dr. Winda Fibrita','\0','',NULL,'',NULL,'D020',0,0,'','',13,193,29);
/*!40000 ALTER TABLE `ee_doctors` ENABLE KEYS */;


--
-- Table structure for table `e_emergency`.`ee_hospital_galleries`
--

DROP TABLE IF EXISTS `ee_hospital_galleries`;
CREATE TABLE `ee_hospital_galleries` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `label` varchar(32) DEFAULT NULL,
  `logChangedBy` varchar(32) DEFAULT NULL,
  `logChangedOn` datetime DEFAULT NULL,
  `logCreatedBy` varchar(32) DEFAULT NULL,
  `logCreatedOn` datetime DEFAULT NULL,
  `logSaveTimes` int(11) DEFAULT NULL,
  `photo` longblob,
  `photoName` varchar(255) DEFAULT NULL,
  `hospital_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK534B69AA9A072C93` (`hospital_id`),
  CONSTRAINT `FK534B69AA9A072C93` FOREIGN KEY (`hospital_id`) REFERENCES `ee_hospitals` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `e_emergency`.`ee_hospital_galleries`
--

/*!40000 ALTER TABLE `ee_hospital_galleries` DISABLE KEYS */;
/*!40000 ALTER TABLE `ee_hospital_galleries` ENABLE KEYS */;


--
-- Table structure for table `e_emergency`.`ee_hospital_partnerships`
--

DROP TABLE IF EXISTS `ee_hospital_partnerships`;
CREATE TABLE `ee_hospital_partnerships` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `logChangedBy` varchar(32) DEFAULT NULL,
  `logChangedOn` datetime DEFAULT NULL,
  `logCreatedBy` varchar(32) DEFAULT NULL,
  `logCreatedOn` datetime DEFAULT NULL,
  `logSaveTimes` int(11) DEFAULT NULL,
  `name` varchar(64) DEFAULT NULL,
  `hospital_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKBD4F3D759A072C93` (`hospital_id`),
  CONSTRAINT `FKBD4F3D759A072C93` FOREIGN KEY (`hospital_id`) REFERENCES `ee_hospitals` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `e_emergency`.`ee_hospital_partnerships`
--

/*!40000 ALTER TABLE `ee_hospital_partnerships` DISABLE KEYS */;
/*!40000 ALTER TABLE `ee_hospital_partnerships` ENABLE KEYS */;


--
-- Table structure for table `e_emergency`.`ee_hospital_ratings`
--

DROP TABLE IF EXISTS `ee_hospital_ratings`;
CREATE TABLE `ee_hospital_ratings` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `logChangedBy` varchar(32) DEFAULT NULL,
  `logChangedOn` datetime DEFAULT NULL,
  `logCreatedBy` varchar(32) DEFAULT NULL,
  `logCreatedOn` datetime DEFAULT NULL,
  `logSaveTimes` int(11) DEFAULT NULL,
  `raterId` varchar(40) DEFAULT NULL,
  `rating` int(11) DEFAULT NULL,
  `remarks` varchar(255) DEFAULT NULL,
  `hospital_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK8D8A77309A072C93` (`hospital_id`),
  CONSTRAINT `FK8D8A77309A072C93` FOREIGN KEY (`hospital_id`) REFERENCES `ee_hospitals` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `e_emergency`.`ee_hospital_ratings`
--

/*!40000 ALTER TABLE `ee_hospital_ratings` DISABLE KEYS */;
/*!40000 ALTER TABLE `ee_hospital_ratings` ENABLE KEYS */;


--
-- Table structure for table `e_emergency`.`ee_hospital_room_classes`
--

DROP TABLE IF EXISTS `ee_hospital_room_classes`;
CREATE TABLE `ee_hospital_room_classes` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `logChangedBy` varchar(32) DEFAULT NULL,
  `logChangedOn` datetime DEFAULT NULL,
  `logCreatedBy` varchar(32) DEFAULT NULL,
  `logCreatedOn` datetime DEFAULT NULL,
  `logSaveTimes` int(11) DEFAULT NULL,
  `photo` longblob,
  `photoName` varchar(255) DEFAULT NULL,
  `price` double DEFAULT NULL,
  `hospital_id` bigint(20) DEFAULT NULL,
  `roomClass_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK7B4544483FA83A81` (`roomClass_id`),
  KEY `FK7B4544489A072C93` (`hospital_id`),
  CONSTRAINT `FK7B4544489A072C93` FOREIGN KEY (`hospital_id`) REFERENCES `ee_hospitals` (`id`),
  CONSTRAINT `FK7B4544483FA83A81` FOREIGN KEY (`roomClass_id`) REFERENCES `ee_room_classes` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `e_emergency`.`ee_hospital_room_classes`
--

/*!40000 ALTER TABLE `ee_hospital_room_classes` DISABLE KEYS */;
/*!40000 ALTER TABLE `ee_hospital_room_classes` ENABLE KEYS */;


--
-- Table structure for table `e_emergency`.`ee_hospital_types`
--

DROP TABLE IF EXISTS `ee_hospital_types`;
CREATE TABLE `ee_hospital_types` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `code` varchar(8) DEFAULT NULL,
  `logChangedBy` varchar(32) DEFAULT NULL,
  `logChangedOn` datetime DEFAULT NULL,
  `logCreatedBy` varchar(32) DEFAULT NULL,
  `logCreatedOn` datetime DEFAULT NULL,
  `logSaveTimes` int(11) DEFAULT NULL,
  `name` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `e_emergency`.`ee_hospital_types`
--

/*!40000 ALTER TABLE `ee_hospital_types` DISABLE KEYS */;
INSERT INTO `ee_hospital_types` (`id`,`code`,`logChangedBy`,`logChangedOn`,`logCreatedBy`,`logCreatedOn`,`logSaveTimes`,`name`) VALUES 
 (1,'HT01','admin','2015-04-16 16:19:48','',NULL,1,'Anak & Bunda'),
 (2,'HT02','admin','2015-04-16 16:19:48','',NULL,1,'Bersalin'),
 (3,'HT03','admin','2015-04-16 16:19:48','',NULL,1,'Ibu & Anak'),
 (4,'HT04','admin','2015-04-16 16:19:48','',NULL,1,'Jantung'),
 (5,'HT05','admin','2015-04-16 16:19:48','',NULL,1,'Jiwa'),
 (6,'HT06','admin','2015-04-16 16:19:48','',NULL,1,'Ketergantungan Obat'),
 (7,'HT07','admin','2015-04-16 16:19:48','',NULL,1,'Kanker'),
 (8,'HT08','admin','2015-04-16 16:19:48','',NULL,1,'Khusus Anak'),
 (9,'HT09','admin','2015-04-16 16:19:48','',NULL,1,'Khusus Bedah'),
 (10,'HT10','admin','2015-04-16 16:19:48','',NULL,1,'Khusus Gigi & Mulut'),
 (11,'HT11','admin','2015-04-16 16:19:48','',NULL,1,'Khusus Ginjal'),
 (12,'HT12','admin','2015-04-16 16:19:48','',NULL,1,'Khusus Otak'),
 (13,'HT13','admin','2015-04-16 16:19:48','',NULL,1,'Khusus Penyakit Dalam'),
 (14,'HT14','admin','2015-04-16 16:19:48','',NULL,1,'Khusus THT');
INSERT INTO `ee_hospital_types` (`id`,`code`,`logChangedBy`,`logChangedOn`,`logCreatedBy`,`logCreatedOn`,`logSaveTimes`,`name`) VALUES 
 (15,'HT15','admin','2015-04-16 16:19:48','',NULL,1,'Kusta'),
 (16,'HT16','admin','2015-04-16 16:19:48','',NULL,1,'Mata'),
 (17,'HT17','admin','2015-04-16 16:19:48','',NULL,1,'Orthopedi'),
 (18,'HT18','admin','2015-04-16 16:19:48','',NULL,1,'Penyakit Infeksi'),
 (19,'HT19','admin','2015-04-16 16:19:48','',NULL,1,'Stroke'),
 (20,'HT20','admin','2015-04-16 16:19:48','',NULL,1,'Tuberkulosa Paru'),
 (21,'HT21','admin','2015-04-16 16:19:48','',NULL,1,'Umum');
/*!40000 ALTER TABLE `ee_hospital_types` ENABLE KEYS */;


--
-- Table structure for table `e_emergency`.`ee_hospitals`
--

DROP TABLE IF EXISTS `ee_hospitals`;
CREATE TABLE `ee_hospitals` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `address` varchar(255) DEFAULT NULL,
  `code` varchar(8) DEFAULT NULL,
  `email` varchar(32) DEFAULT NULL,
  `fax` varchar(16) DEFAULT NULL,
  `logChangedBy` varchar(32) DEFAULT NULL,
  `logChangedOn` datetime DEFAULT NULL,
  `logCreatedBy` varchar(32) DEFAULT NULL,
  `logCreatedOn` datetime DEFAULT NULL,
  `logSaveTimes` int(11) DEFAULT NULL,
  `name` varchar(64) DEFAULT NULL,
  `ownerType` varchar(12) DEFAULT NULL,
  `ownerTypeIndex` int(11) DEFAULT NULL,
  `partnership` bit(1) DEFAULT NULL,
  `phone` varchar(16) DEFAULT NULL,
  `photo` longblob,
  `photoName` varchar(255) DEFAULT NULL,
  `totalDoctor` int(11) DEFAULT NULL,
  `totalRater` int(11) DEFAULT NULL,
  `totalRating` int(11) DEFAULT NULL,
  `totalRoom` int(11) DEFAULT NULL,
  `website` varchar(48) DEFAULT NULL,
  `zipCode` varchar(5) DEFAULT NULL,
  `hospitalType_id` bigint(20) DEFAULT NULL,
  `province_id` bigint(20) DEFAULT NULL,
  `regency_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK3D8A889A43C44CF7` (`province_id`),
  KEY `FK3D8A889A25D989F3` (`hospitalType_id`),
  KEY `FK3D8A889A676C93FD` (`regency_id`),
  CONSTRAINT `FK3D8A889A676C93FD` FOREIGN KEY (`regency_id`) REFERENCES `util_regencies` (`id`),
  CONSTRAINT `FK3D8A889A25D989F3` FOREIGN KEY (`hospitalType_id`) REFERENCES `ee_hospital_types` (`id`),
  CONSTRAINT `FK3D8A889A43C44CF7` FOREIGN KEY (`province_id`) REFERENCES `util_provinces` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `e_emergency`.`ee_hospitals`
--

/*!40000 ALTER TABLE `ee_hospitals` DISABLE KEYS */;
INSERT INTO `ee_hospitals` (`id`,`address`,`code`,`email`,`fax`,`logChangedBy`,`logChangedOn`,`logCreatedBy`,`logCreatedOn`,`logSaveTimes`,`name`,`ownerType`,`ownerTypeIndex`,`partnership`,`phone`,`photo`,`photoName`,`totalDoctor`,`totalRater`,`totalRating`,`totalRoom`,`website`,`zipCode`,`hospitalType_id`,`province_id`,`regency_id`) VALUES 
 (1,'Jl. Raden Saleh 40','H001','','02138997778','admin','2015-04-16 16:19:48','',NULL,1,'Rumah Sakit PGI Cikini','Private',1,'\0','02138997777',NULL,'',0,0,0,0,'www.rscikini.com','',21,13,192),
 (2,'Jalan Diponegoro Nomor 71, Salemba, Jakarta Pusat','H002','','0213148991','admin','2015-04-16 16:19:48','',NULL,1,'Rumah Sakit Cipto Mangunkusumo (RSCM Jakarta)','Government',0,'\0','021500135',NULL,'',0,0,0,0,'rscm.co.id','',21,13,192),
 (3,'Jalan Letjen. S. Parman Kav. 84-86, Slipi, Jakarta Barat\n(depan halte Busway RS Jantung Harapan Kita)','H003','','0215681579','admin','2015-04-16 16:19:48','',NULL,1,'Rumah Sakit Dharmais','Government',0,'\0','0215681570',NULL,'',0,0,0,0,'www.dharmais.co.id','',7,13,191),
 (4,'Jl. RS.Fatmawati, Cilandak, Jakarta Selatan,DKI Jakarta Indonesia','H004','iph.rsf@gmail.com','','admin','2015-04-16 16:19:48','',NULL,1,'Rumah Sakit Fatmawati','Government',0,'\0','02133777',NULL,'',0,0,0,0,'www.fatmawatihospital.com','12430',21,13,193);
INSERT INTO `ee_hospitals` (`id`,`address`,`code`,`email`,`fax`,`logChangedBy`,`logChangedOn`,`logCreatedBy`,`logCreatedOn`,`logSaveTimes`,`name`,`ownerType`,`ownerTypeIndex`,`partnership`,`phone`,`photo`,`photoName`,`totalDoctor`,`totalRater`,`totalRating`,`totalRoom`,`website`,`zipCode`,`hospitalType_id`,`province_id`,`regency_id`) VALUES 
 (5,'','H005','','','admin','2015-04-16 16:19:48','',NULL,1,'RS Siloam Kebon Jeruk','',NULL,'\0','',NULL,'',0,0,0,0,'','',NULL,NULL,NULL),
 (6,'Jl. Raya Mangga Besar No. 137-139, Jakarta Pusat','H006','','62216497494','admin','2015-04-16 16:19:48','',NULL,1,'Rumah Sakit Husada','Private',1,'\0','62216260108',NULL,'',0,0,0,0,'www.husada.co.id','10730',21,13,192),
 (7,'Jl. Mahoni 1, Cijantung II, Pasar Rebo, Jakarta Timur','H007','kesdamjaya_cijantung@yahoo.com','2187793332','admin','2015-04-16 16:19:48','',NULL,1,'Rumah Sakit Kesdam Jaya Cijantung','',NULL,'\0','0218407886',NULL,'',0,0,0,0,'','',21,13,194),
 (8,'Jl. Jend. Gatot Subroto Kav. 59, Jakarta Selatan','H008','customercare@medistra.com','62215210184','admin','2015-04-16 16:19:48','',NULL,1,'Rumah Sakit Medistra','Private',1,'\0','62215210200',NULL,'',0,0,0,0,'www.medistra.com','12950',21,13,193),
 (9,'Jl. Letjend. Tb. Simatupang No.30, Gedong, Jakarta Timur','H009','admin@rsudpasarrebo.com','','admin','2015-04-16 16:19:48','',NULL,1,'Rumah Sakit Pasar Rebo','Government',0,'\0','62218401127',NULL,'',0,0,0,0,'rsudpasarrebo.com','13760',21,13,194);
INSERT INTO `ee_hospitals` (`id`,`address`,`code`,`email`,`fax`,`logChangedBy`,`logChangedOn`,`logCreatedBy`,`logCreatedOn`,`logSaveTimes`,`name`,`ownerType`,`ownerTypeIndex`,`partnership`,`phone`,`photo`,`photoName`,`totalDoctor`,`totalRater`,`totalRating`,`totalRoom`,`website`,`zipCode`,`hospitalType_id`,`province_id`,`regency_id`) VALUES 
 (10,'Jl. Baru Sunter Permai Raya, Jakarta Utara','H010','','','admin','2015-04-16 16:19:48','',NULL,1,'Rumah Sakit Penyakit Infeksi Prof. Dr. Sulianti Saroso','Government',0,'\0','',NULL,'',0,0,0,0,'rspi-suliantisaroso.co.id','14340',21,13,195),
 (11,'','H011','','','admin','2015-04-16 16:19:48','',NULL,1,'Rumah Sakit Umum Mary Cileungsi','',NULL,'\0','',NULL,'',0,0,0,0,'','',NULL,NULL,NULL),
 (12,'','H012','','','admin','2015-04-16 16:19:48','',NULL,1,'Rumah Sakit MH Thamrin Cileungsi','',NULL,'\0','',NULL,'',0,0,0,0,'','',NULL,NULL,NULL),
 (13,'','H013','','','admin','2015-04-16 16:19:48','',NULL,1,'Rumah Sakit Ibu dan Anak Graha Permata Ibu','',NULL,'\0','',NULL,'',0,0,0,0,'','',NULL,NULL,NULL),
 (14,'','H014','','','admin','2015-04-16 16:19:48','',NULL,1,'Rumah Sakit Eka Hospital','',NULL,'\0','',NULL,'',0,0,0,0,'','',NULL,NULL,NULL);
INSERT INTO `ee_hospitals` (`id`,`address`,`code`,`email`,`fax`,`logChangedBy`,`logChangedOn`,`logCreatedBy`,`logCreatedOn`,`logSaveTimes`,`name`,`ownerType`,`ownerTypeIndex`,`partnership`,`phone`,`photo`,`photoName`,`totalDoctor`,`totalRater`,`totalRating`,`totalRoom`,`website`,`zipCode`,`hospitalType_id`,`province_id`,`regency_id`) VALUES 
 (15,'','H015','','','admin','2015-04-16 16:19:48','',NULL,1,'Rumah Sakit Internasional Omni','',NULL,'\0','',NULL,'',0,0,0,0,'','',NULL,NULL,NULL),
 (16,'','H016','','','admin','2015-04-16 16:19:48','',NULL,1,'Rumah Sakit Pondok Indah-Bintaro Jaya','',NULL,'\0','',NULL,'',0,0,0,0,'','',NULL,NULL,NULL),
 (17,'','H017','','','admin','2015-04-16 16:19:48','',NULL,1,'Rumah Sakit Umum Daerah Adjidarmo','',NULL,'\0','',NULL,'',0,0,0,0,'','',NULL,NULL,NULL),
 (18,'','H018','','','admin','2015-04-16 16:19:48','',NULL,1,'Rumah Sakit Advent Bandung','',NULL,'\0','',NULL,'',0,0,0,0,'','',NULL,NULL,NULL),
 (19,'','H019','','','admin','2015-04-16 16:19:48','',NULL,1,'Rumah Sakit Umum Pusat Hasan Sadikin','',NULL,'\0','',NULL,'',0,0,0,0,'','',NULL,NULL,NULL),
 (20,'','H020','','','admin','2015-04-16 16:19:48','',NULL,1,'Rumah Sakit Jiwa Riau 11','',NULL,'\0','',NULL,'',0,0,0,0,'','',NULL,NULL,NULL);
INSERT INTO `ee_hospitals` (`id`,`address`,`code`,`email`,`fax`,`logChangedBy`,`logChangedOn`,`logCreatedBy`,`logCreatedOn`,`logSaveTimes`,`name`,`ownerType`,`ownerTypeIndex`,`partnership`,`phone`,`photo`,`photoName`,`totalDoctor`,`totalRater`,`totalRating`,`totalRoom`,`website`,`zipCode`,`hospitalType_id`,`province_id`,`regency_id`) VALUES 
 (21,'','H021','','','admin','2015-04-16 16:19:48','',NULL,1,'Rumah Sakit Umum Daerah Kota Bandung','',NULL,'\0','',NULL,'',0,0,0,0,'','',NULL,NULL,NULL),
 (22,'','H022','','','admin','2015-04-16 16:19:48','',NULL,1,'Rumah Bersalin FATIMAH Ujungberung','',NULL,'\0','',NULL,'',0,0,0,0,'','',NULL,NULL,NULL),
 (23,'','H023','','','admin','2015-04-16 16:19:48','',NULL,1,'Rumah Sakit Khusus Bedah Halmahera Siaga','',NULL,'\0','',NULL,'',0,0,0,0,'','',NULL,NULL,NULL),
 (24,'','H024','','','admin','2015-04-16 16:19:48','',NULL,1,'Rumah Sakit Santosa','',NULL,'\0','',NULL,'',0,0,0,0,'','',NULL,NULL,NULL),
 (25,'','H025','','','admin','2015-04-16 16:19:48','',NULL,1,'Rumah Sakit Borromeus','',NULL,'\0','',NULL,'',0,0,0,0,'','',NULL,NULL,NULL),
 (26,'','H026','','','admin','2015-04-16 16:19:48','',NULL,1,'Rumah Sakit Umum Cibabat','',NULL,'\0','',NULL,'',0,0,0,0,'','',NULL,NULL,NULL);
INSERT INTO `ee_hospitals` (`id`,`address`,`code`,`email`,`fax`,`logChangedBy`,`logChangedOn`,`logCreatedBy`,`logCreatedOn`,`logSaveTimes`,`name`,`ownerType`,`ownerTypeIndex`,`partnership`,`phone`,`photo`,`photoName`,`totalDoctor`,`totalRater`,`totalRating`,`totalRoom`,`website`,`zipCode`,`hospitalType_id`,`province_id`,`regency_id`) VALUES 
 (27,'','H027','','','admin','2015-04-16 16:19:48','',NULL,1,'Rumah Sakit Tk. II Dustira','',NULL,'\0','',NULL,'',0,0,0,0,'','',NULL,NULL,NULL),
 (28,'','H028','','','admin','2015-04-16 16:19:48','',NULL,1,'Rumah Sakit Umum Daerah Soreang','',NULL,'\0','',NULL,'',0,0,0,0,'','',NULL,NULL,NULL);
/*!40000 ALTER TABLE `ee_hospitals` ENABLE KEYS */;


--
-- Table structure for table `e_emergency`.`ee_room_classes`
--

DROP TABLE IF EXISTS `ee_room_classes`;
CREATE TABLE `ee_room_classes` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `code` varchar(8) DEFAULT NULL,
  `logChangedBy` varchar(32) DEFAULT NULL,
  `logChangedOn` datetime DEFAULT NULL,
  `logCreatedBy` varchar(32) DEFAULT NULL,
  `logCreatedOn` datetime DEFAULT NULL,
  `logSaveTimes` int(11) DEFAULT NULL,
  `name` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `e_emergency`.`ee_room_classes`
--

/*!40000 ALTER TABLE `ee_room_classes` DISABLE KEYS */;
INSERT INTO `ee_room_classes` (`id`,`code`,`logChangedBy`,`logChangedOn`,`logCreatedBy`,`logCreatedOn`,`logSaveTimes`,`name`) VALUES 
 (1,'RC01','admin','2015-04-16 16:19:48','',NULL,1,'VVIP'),
 (2,'RC02','admin','2015-04-16 16:19:48','',NULL,1,'VIP'),
 (3,'RC03','admin','2015-04-16 16:19:48','',NULL,1,'I'),
 (4,'RC04','admin','2015-04-16 16:19:48','',NULL,1,'II'),
 (5,'RC05','admin','2015-04-16 16:19:48','',NULL,1,'III');
/*!40000 ALTER TABLE `ee_room_classes` ENABLE KEYS */;


--
-- Table structure for table `e_emergency`.`ee_schedules`
--

DROP TABLE IF EXISTS `ee_schedules`;
CREATE TABLE `ee_schedules` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `day_` varchar(12) DEFAULT NULL,
  `dayIndex` int(11) DEFAULT NULL,
  `logChangedBy` varchar(32) DEFAULT NULL,
  `logChangedOn` datetime DEFAULT NULL,
  `logCreatedBy` varchar(32) DEFAULT NULL,
  `logCreatedOn` datetime DEFAULT NULL,
  `logSaveTimes` int(11) DEFAULT NULL,
  `timeBegin` time DEFAULT NULL,
  `timeEnd` time DEFAULT NULL,
  `doctor_id` bigint(20) DEFAULT NULL,
  `hospital_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK64FD7FDD9A072C93` (`hospital_id`),
  KEY `FK64FD7FDD5C3478B3` (`doctor_id`),
  CONSTRAINT `FK64FD7FDD5C3478B3` FOREIGN KEY (`doctor_id`) REFERENCES `ee_doctors` (`id`),
  CONSTRAINT `FK64FD7FDD9A072C93` FOREIGN KEY (`hospital_id`) REFERENCES `ee_hospitals` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `e_emergency`.`ee_schedules`
--

/*!40000 ALTER TABLE `ee_schedules` DISABLE KEYS */;
/*!40000 ALTER TABLE `ee_schedules` ENABLE KEYS */;


--
-- Table structure for table `e_emergency`.`ee_specialists`
--

DROP TABLE IF EXISTS `ee_specialists`;
CREATE TABLE `ee_specialists` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `code` varchar(8) DEFAULT NULL,
  `logChangedBy` varchar(32) DEFAULT NULL,
  `logChangedOn` datetime DEFAULT NULL,
  `logCreatedBy` varchar(32) DEFAULT NULL,
  `logCreatedOn` datetime DEFAULT NULL,
  `logSaveTimes` int(11) DEFAULT NULL,
  `name` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `e_emergency`.`ee_specialists`
--

/*!40000 ALTER TABLE `ee_specialists` DISABLE KEYS */;
INSERT INTO `ee_specialists` (`id`,`code`,`logChangedBy`,`logChangedOn`,`logCreatedBy`,`logCreatedOn`,`logSaveTimes`,`name`) VALUES 
 (1,'S001','admin','2015-04-16 16:19:47','',NULL,1,'Umum'),
 (2,'S002','admin','2015-04-16 16:19:47','',NULL,1,'Ahli Bedah Anak'),
 (3,'S003','admin','2015-04-16 16:19:47','',NULL,1,'Ahli Bedah Orthopedi'),
 (4,'S004','admin','2015-04-16 16:19:47','',NULL,1,'Ahli Bedah Plastik'),
 (5,'S005','admin','2015-04-16 16:19:47','',NULL,1,'Ahli Bedah Syaraf'),
 (6,'S006','admin','2015-04-16 16:19:47','',NULL,1,'Ahli Bedah Thorax'),
 (7,'S007','admin','2015-04-16 16:19:47','',NULL,1,'Anak'),
 (8,'S008','admin','2015-04-16 16:19:47','',NULL,1,'Bedah'),
 (9,'S009','admin','2015-04-16 16:19:47','',NULL,1,'Forensik'),
 (10,'S010','admin','2015-04-16 16:19:47','',NULL,1,'Gigi'),
 (11,'S011','admin','2015-04-16 16:19:47','',NULL,1,'Jantung & Pembuluh Darah'),
 (12,'S012','admin','2015-04-16 16:19:47','',NULL,1,'Kesehatan Jiwa'),
 (13,'S013','admin','2015-04-16 16:19:47','',NULL,1,'Kulit & Kelamin'),
 (14,'S014','admin','2015-04-16 16:19:47','',NULL,1,'Mata');
INSERT INTO `ee_specialists` (`id`,`code`,`logChangedBy`,`logChangedOn`,`logCreatedBy`,`logCreatedOn`,`logSaveTimes`,`name`) VALUES 
 (15,'S015','admin','2015-04-16 16:19:47','',NULL,1,'Obsgin'),
 (16,'S016','admin','2015-04-16 16:19:47','',NULL,1,'Ofthalmologi'),
 (17,'S017','admin','2015-04-16 16:19:47','',NULL,1,'Okupasi'),
 (18,'S018','admin','2015-04-16 16:19:47','',NULL,1,'Orthopedi'),
 (19,'S019','admin','2015-04-16 16:19:47','',NULL,1,'Paru'),
 (20,'S020','admin','2015-04-16 16:19:47','',NULL,1,'Patologi Anatomi'),
 (21,'S021','admin','2015-04-16 16:19:47','',NULL,1,'Penyakit Dalam'),
 (22,'S022','admin','2015-04-16 16:19:47','',NULL,1,'Psikiatri'),
 (23,'S023','admin','2015-04-16 16:19:48','',NULL,1,'Radiologi'),
 (24,'S024','admin','2015-04-16 16:19:48','',NULL,1,'Rehabilitasi Medik'),
 (25,'S025','admin','2015-04-16 16:19:48','',NULL,1,'Syaraf'),
 (26,'S026','admin','2015-04-16 16:19:48','',NULL,1,'THT'),
 (27,'S027','admin','2015-04-16 16:19:48','',NULL,1,'Urologi'),
 (28,'S028','admin','2015-04-16 16:19:48','',NULL,1,'Andrologi'),
 (29,'S029','admin','2015-04-16 16:19:48','',NULL,1,'Akupuntur');
/*!40000 ALTER TABLE `ee_specialists` ENABLE KEYS */;


--
-- Table structure for table `e_emergency`.`util_areas`
--

DROP TABLE IF EXISTS `util_areas`;
CREATE TABLE `util_areas` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `code` varchar(8) DEFAULT NULL,
  `logChangedBy` varchar(32) DEFAULT NULL,
  `logChangedOn` datetime DEFAULT NULL,
  `logCreatedBy` varchar(32) DEFAULT NULL,
  `logCreatedOn` datetime DEFAULT NULL,
  `logSaveTimes` int(11) DEFAULT NULL,
  `name` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `e_emergency`.`util_areas`
--

/*!40000 ALTER TABLE `util_areas` DISABLE KEYS */;
/*!40000 ALTER TABLE `util_areas` ENABLE KEYS */;


--
-- Table structure for table `e_emergency`.`util_configs`
--

DROP TABLE IF EXISTS `util_configs`;
CREATE TABLE `util_configs` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `appl` varchar(32) DEFAULT NULL,
  `file_` longblob,
  `key_` varchar(255) DEFAULT NULL,
  `logChangedBy` varchar(32) DEFAULT NULL,
  `logChangedOn` datetime DEFAULT NULL,
  `logCreatedBy` varchar(32) DEFAULT NULL,
  `logCreatedOn` datetime DEFAULT NULL,
  `logSaveTimes` int(11) DEFAULT NULL,
  `value_` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `e_emergency`.`util_configs`
--

/*!40000 ALTER TABLE `util_configs` DISABLE KEYS */;
INSERT INTO `util_configs` (`id`,`appl`,`file_`,`key_`,`logChangedBy`,`logChangedOn`,`logCreatedBy`,`logCreatedOn`,`logSaveTimes`,`value_`) VALUES 
 (1,'e-emergency-zk',NULL,'UtilConfAppl.applLanguage',NULL,NULL,NULL,'2015-04-16 16:19:11',0,'0'),
 (2,'e-emergency-zk',NULL,'UtilConfAppl.applLookAndFeel',NULL,NULL,NULL,'2015-04-16 16:19:11',0,'0'),
 (3,'e-emergency-zk',NULL,'UtilConfAppl.applMenuPosition',NULL,NULL,NULL,'2015-04-16 16:19:11',0,'0'),
 (4,'e-emergency-zk',NULL,'UtilConfAppl.applWallpaper',NULL,NULL,NULL,'2015-04-16 16:19:11',0,NULL),
 (5,'e-emergency-zk',NULL,'UtilConfAppl.applWallpaperName',NULL,NULL,NULL,'2015-04-16 16:19:11',0,NULL),
 (6,'e-emergency-zk',NULL,'UtilConfAppl.companyLogo',NULL,NULL,NULL,'2015-04-16 16:19:11',0,NULL),
 (7,'e-emergency-zk',NULL,'UtilConfAppl.companyLogoName',NULL,NULL,NULL,'2015-04-16 16:19:11',0,NULL),
 (8,'e-emergency-zk',NULL,'UtilConfAppl.companyName',NULL,NULL,NULL,'2015-04-16 16:19:11',0,NULL),
 (9,'e-emergency-zk',NULL,'UtilConfAppl.companyAddress',NULL,NULL,NULL,'2015-04-16 16:19:11',0,NULL),
 (10,'e-emergency-zk',NULL,'UtilConfAppl.companyCity',NULL,NULL,NULL,'2015-04-16 16:19:11',0,'147');
INSERT INTO `util_configs` (`id`,`appl`,`file_`,`key_`,`logChangedBy`,`logChangedOn`,`logCreatedBy`,`logCreatedOn`,`logSaveTimes`,`value_`) VALUES 
 (11,'e-emergency-zk',NULL,'UtilConfAppl.companyProvince',NULL,NULL,NULL,'2015-04-16 16:19:11',0,'10'),
 (12,'e-emergency-zk',NULL,'UtilConfAppl.companyState',NULL,NULL,NULL,'2015-04-16 16:19:11',0,'Indonesia'),
 (13,'e-emergency-zk',NULL,'UtilConfAppl.companyPhone',NULL,NULL,NULL,'2015-04-16 16:19:11',0,NULL),
 (14,'e-emergency-zk',NULL,'UtilConfAppl.companyFax',NULL,NULL,NULL,'2015-04-16 16:19:11',0,NULL),
 (15,'e-emergency-zk',NULL,'UtilConfAppl.companyEmail',NULL,NULL,NULL,'2015-04-16 16:19:11',0,NULL),
 (16,'e-emergency-zk',NULL,'UtilConfAppl.companyWeb',NULL,NULL,NULL,'2015-04-16 16:19:11',0,NULL),
 (17,'e-emergency-zk',NULL,'UtilConfAppl.ownerName',NULL,NULL,NULL,'2015-04-16 16:19:11',0,NULL),
 (18,'e-emergency-zk',NULL,'UtilConfAppl.ownerPosition',NULL,NULL,NULL,'2015-04-16 16:19:11',0,NULL),
 (19,'e-emergency-zk',NULL,'UtilConfAppl.ownerKtp',NULL,NULL,NULL,'2015-04-16 16:19:11',0,NULL),
 (20,'e-emergency-zk',NULL,'UtilConfAppl.ownerNpwp',NULL,NULL,NULL,'2015-04-16 16:19:11',0,NULL);
INSERT INTO `util_configs` (`id`,`appl`,`file_`,`key_`,`logChangedBy`,`logChangedOn`,`logCreatedBy`,`logCreatedOn`,`logSaveTimes`,`value_`) VALUES 
 (21,'e-emergency-zk',NULL,'UtilConfAppl.utilAdditionalTabClass',NULL,NULL,NULL,'2015-04-16 16:19:11',0,NULL),
 (22,'e-emergency-zk',NULL,'UtilConfAppl.utilAdditionalSettingClass',NULL,NULL,NULL,'2015-04-16 16:19:11',0,'model.obj.emergency.EeConfig');
/*!40000 ALTER TABLE `util_configs` ENABLE KEYS */;


--
-- Table structure for table `e_emergency`.`util_loggers`
--

DROP TABLE IF EXISTS `util_loggers`;
CREATE TABLE `util_loggers` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `action_` varchar(32) DEFAULT NULL,
  `appl` varchar(32) DEFAULT NULL,
  `ip` varchar(40) DEFAULT NULL,
  `logChangedBy` varchar(32) DEFAULT NULL,
  `logChangedOn` datetime DEFAULT NULL,
  `logCreatedBy` varchar(32) DEFAULT NULL,
  `logCreatedOn` datetime DEFAULT NULL,
  `logSaveTimes` int(11) DEFAULT NULL,
  `note` varchar(255) DEFAULT NULL,
  `screen` varchar(32) DEFAULT NULL,
  `userGroupName` varchar(32) DEFAULT NULL,
  `username` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `e_emergency`.`util_loggers`
--

/*!40000 ALTER TABLE `util_loggers` DISABLE KEYS */;
INSERT INTO `util_loggers` (`id`,`action_`,`appl`,`ip`,`logChangedBy`,`logChangedOn`,`logCreatedBy`,`logCreatedOn`,`logSaveTimes`,`note`,`screen`,`userGroupName`,`username`) VALUES 
 (1,'Save','e-emergency-zk','127.0.0.1',NULL,NULL,NULL,'2015-04-16 16:19:12',0,'CCDatabaseConfig : domain:localhost;database:e_emergency;','ZulDatabaseSetting',NULL,NULL),
 (2,'Login','e-emergency-zk','172.19.186.54',NULL,NULL,'admin','2015-04-16 16:19:23',0,'UtilUser : username:admin;active:true;','ZulLogin','admin','admin'),
 (3,'Login','e-emergency-zk','172.19.186.54',NULL,NULL,'admin','2015-04-16 16:22:49',0,'UtilUser : username:admin;active:true;','ZulLogin','admin','admin'),
 (4,'Login','e-emergency-zk','172.19.186.54',NULL,NULL,'admin','2015-04-16 16:25:15',0,'UtilUser : username:admin;active:true;','ZulLogin','admin','admin');
/*!40000 ALTER TABLE `util_loggers` ENABLE KEYS */;


--
-- Table structure for table `e_emergency`.`util_persons`
--

DROP TABLE IF EXISTS `util_persons`;
CREATE TABLE `util_persons` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `address` varchar(255) DEFAULT NULL,
  `birthDate` date DEFAULT NULL,
  `birthPlace` varchar(48) DEFAULT NULL,
  `bloodType` varchar(4) DEFAULT NULL,
  `bloodTypeIndex` int(11) DEFAULT NULL,
  `code` varchar(12) DEFAULT NULL,
  `email` varchar(32) DEFAULT NULL,
  `fax` varchar(16) DEFAULT NULL,
  `fullName` varchar(32) DEFAULT NULL,
  `gender` varchar(8) DEFAULT NULL,
  `genderIndex` int(11) DEFAULT NULL,
  `handPhone` varchar(16) DEFAULT NULL,
  `ktp` varchar(16) DEFAULT NULL,
  `logChangedBy` varchar(32) DEFAULT NULL,
  `logChangedOn` datetime DEFAULT NULL,
  `logCreatedBy` varchar(32) DEFAULT NULL,
  `logCreatedOn` datetime DEFAULT NULL,
  `logSaveTimes` int(11) DEFAULT NULL,
  `nationality` varchar(4) DEFAULT NULL,
  `nationalityIndex` int(11) DEFAULT NULL,
  `neighborhood` varchar(16) DEFAULT NULL,
  `nickName` varchar(16) DEFAULT NULL,
  `npwp` varchar(20) DEFAULT NULL,
  `passport` varchar(12) DEFAULT NULL,
  `phone` varchar(16) DEFAULT NULL,
  `photo` longblob,
  `photoName` varchar(255) DEFAULT NULL,
  `province` varchar(48) DEFAULT NULL,
  `regency` varchar(48) DEFAULT NULL,
  `religion` varchar(16) DEFAULT NULL,
  `religionIndex` int(11) DEFAULT NULL,
  `subDistrict` varchar(48) DEFAULT NULL,
  `village` varchar(48) DEFAULT NULL,
  `website` varchar(48) DEFAULT NULL,
  `work_` varchar(16) DEFAULT NULL,
  `zipCode` varchar(5) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `e_emergency`.`util_persons`
--

/*!40000 ALTER TABLE `util_persons` DISABLE KEYS */;
/*!40000 ALTER TABLE `util_persons` ENABLE KEYS */;


--
-- Table structure for table `e_emergency`.`util_privileges`
--

DROP TABLE IF EXISTS `util_privileges`;
CREATE TABLE `util_privileges` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `appl` varchar(32) DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  `comp` varchar(255) DEFAULT NULL,
  `logChangedBy` varchar(32) DEFAULT NULL,
  `logChangedOn` datetime DEFAULT NULL,
  `logCreatedBy` varchar(32) DEFAULT NULL,
  `logCreatedOn` datetime DEFAULT NULL,
  `logSaveTimes` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `parent_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK5AB8903F7EDDE5E4` (`parent_id`),
  CONSTRAINT `FK5AB8903F7EDDE5E4` FOREIGN KEY (`parent_id`) REFERENCES `util_privileges` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `e_emergency`.`util_privileges`
--

/*!40000 ALTER TABLE `util_privileges` DISABLE KEYS */;
INSERT INTO `util_privileges` (`id`,`appl`,`code`,`comp`,`logChangedBy`,`logChangedOn`,`logCreatedBy`,`logCreatedOn`,`logSaveTimes`,`name`,`parent_id`) VALUES 
 (1,'e-emergency-zk','Ee.01.00','controller.zul.emergency.ZulHospitalListCtrl',NULL,NULL,NULL,'2015-04-16 16:19:11',0,'Hospital',NULL),
 (2,'e-emergency-zk','Ee.01.01','controller.zul.emergency.ZulHospitalListCtrl.btnAdd',NULL,NULL,NULL,'2015-04-16 16:19:11',0,'Add',1),
 (3,'e-emergency-zk','Ee.01.02','controller.zul.emergency.ZulHospitalListCtrl.btnDelete',NULL,NULL,NULL,'2015-04-16 16:19:11',0,'Delete',1),
 (4,'e-emergency-zk','Ee.01.03','controller.zul.emergency.ZulHospitalListCtrl.btnEdit',NULL,NULL,NULL,'2015-04-16 16:19:11',0,'Edit',1),
 (5,'e-emergency-zk','Ee.02.00','controller.zul.emergency.ZulDoctorListCtrl',NULL,NULL,NULL,'2015-04-16 16:19:11',0,'Doctor',NULL),
 (6,'e-emergency-zk','Ee.02.04','controller.zul.emergency.ZulDoctorListCtrl.btnAdd',NULL,NULL,NULL,'2015-04-16 16:19:11',0,'Add',5),
 (7,'e-emergency-zk','Ee.02.05','controller.zul.emergency.ZulDoctorListCtrl.btnDelete',NULL,NULL,NULL,'2015-04-16 16:19:11',0,'Delete',5),
 (8,'e-emergency-zk','Ee.02.06','controller.zul.emergency.ZulDoctorListCtrl.btnEdit',NULL,NULL,NULL,'2015-04-16 16:19:11',0,'Edit',5);
INSERT INTO `util_privileges` (`id`,`appl`,`code`,`comp`,`logChangedBy`,`logChangedOn`,`logCreatedBy`,`logCreatedOn`,`logSaveTimes`,`name`,`parent_id`) VALUES 
 (9,'e-emergency-zk','Utility.03.00','cococare.framework.zk.controller.zul.util.ZulUserGroupListCtrl',NULL,NULL,NULL,'2015-04-16 16:19:11',0,'User Group',NULL),
 (10,'e-emergency-zk','Utility.03.07','cococare.framework.zk.controller.zul.util.ZulUserGroupListCtrl.btnAdd',NULL,NULL,NULL,'2015-04-16 16:19:11',0,'Add',9),
 (11,'e-emergency-zk','Utility.03.08','cococare.framework.zk.controller.zul.util.ZulUserGroupListCtrl.btnDelete',NULL,NULL,NULL,'2015-04-16 16:19:11',0,'Delete',9),
 (12,'e-emergency-zk','Utility.03.09','cococare.framework.zk.controller.zul.util.ZulUserGroupListCtrl.btnEdit',NULL,NULL,NULL,'2015-04-16 16:19:11',0,'Edit',9),
 (13,'e-emergency-zk','Utility.04.00','cococare.framework.zk.controller.zul.util.ZulUserListCtrl',NULL,NULL,NULL,'2015-04-16 16:19:11',0,'User',NULL),
 (14,'e-emergency-zk','Utility.04.10','cococare.framework.zk.controller.zul.util.ZulUserListCtrl.btnAdd',NULL,NULL,NULL,'2015-04-16 16:19:11',0,'Add',13),
 (15,'e-emergency-zk','Utility.04.11','cococare.framework.zk.controller.zul.util.ZulUserListCtrl.btnDelete',NULL,NULL,NULL,'2015-04-16 16:19:11',0,'Delete',13);
INSERT INTO `util_privileges` (`id`,`appl`,`code`,`comp`,`logChangedBy`,`logChangedOn`,`logCreatedBy`,`logCreatedOn`,`logSaveTimes`,`name`,`parent_id`) VALUES 
 (16,'e-emergency-zk','Utility.04.12','cococare.framework.zk.controller.zul.util.ZulUserListCtrl.btnChangePassword',NULL,NULL,NULL,'2015-04-16 16:19:11',0,'Change Password',13),
 (17,'e-emergency-zk','Utility.04.13','cococare.framework.zk.controller.zul.util.ZulUserListCtrl.btnEdit',NULL,NULL,NULL,'2015-04-16 16:19:11',0,'Edit',13),
 (18,'e-emergency-zk','Utility.05.00','cococare.framework.zk.controller.zul.util.ZulChangePasswordCtrl',NULL,NULL,NULL,'2015-04-16 16:19:11',0,'Change Password',NULL),
 (19,'e-emergency-zk','Utility.05.14','cococare.framework.zk.controller.zul.util.ZulChangePasswordCtrl.btnSave',NULL,NULL,NULL,'2015-04-16 16:19:11',0,'Save',18),
 (20,'e-emergency-zk','Utility.05.15','cococare.framework.zk.controller.zul.util.ZulChangePasswordCtrl.btnClose',NULL,NULL,NULL,'2015-04-16 16:19:11',0,'Close',18),
 (21,'e-emergency-zk','Utility.06.00','cococare.framework.zk.controller.zul.util.ZulParameterListCtrl',NULL,NULL,NULL,'2015-04-16 16:19:11',0,'Parameter',NULL),
 (22,'e-emergency-zk','Utility.06.16','cococare.framework.zk.controller.zul.util.ZulParameterListCtrl.btnAdd',NULL,NULL,NULL,'2015-04-16 16:19:11',0,'Add',21);
INSERT INTO `util_privileges` (`id`,`appl`,`code`,`comp`,`logChangedBy`,`logChangedOn`,`logCreatedBy`,`logCreatedOn`,`logSaveTimes`,`name`,`parent_id`) VALUES 
 (23,'e-emergency-zk','Utility.06.17','cococare.framework.zk.controller.zul.util.ZulParameterListCtrl.btnDelete',NULL,NULL,NULL,'2015-04-16 16:19:11',0,'Delete',21),
 (24,'e-emergency-zk','Utility.06.18','cococare.framework.zk.controller.zul.util.ZulParameterListCtrl.btnEdit',NULL,NULL,NULL,'2015-04-16 16:19:11',0,'Edit',21),
 (25,'e-emergency-zk','Utility.07.00','cococare.framework.zk.controller.zul.util.ZulExportImportCtrl',NULL,NULL,NULL,'2015-04-16 16:19:11',0,'Export Import',NULL),
 (26,'e-emergency-zk','Utility.07.19','cococare.framework.zk.controller.zul.util.ZulExportImportCtrl.btnTemplate',NULL,NULL,NULL,'2015-04-16 16:19:11',0,'Template',25),
 (27,'e-emergency-zk','Utility.07.20','cococare.framework.zk.controller.zul.util.ZulExportImportCtrl.btnExport',NULL,NULL,NULL,'2015-04-16 16:19:11',0,'Export',25),
 (28,'e-emergency-zk','Utility.07.21','cococare.framework.zk.controller.zul.util.ZulExportImportCtrl.btnClose',NULL,NULL,NULL,'2015-04-16 16:19:11',0,'Close',25),
 (29,'e-emergency-zk','Utility.07.22','cococare.framework.zk.controller.zul.util.ZulExportImportCtrl.btnImport',NULL,NULL,NULL,'2015-04-16 16:19:11',0,'Import',25);
INSERT INTO `util_privileges` (`id`,`appl`,`code`,`comp`,`logChangedBy`,`logChangedOn`,`logCreatedBy`,`logCreatedOn`,`logSaveTimes`,`name`,`parent_id`) VALUES 
 (30,'e-emergency-zk','Utility.08.00','cococare.framework.zk.controller.zul.util.ZulLoggerListCtrl',NULL,NULL,NULL,'2015-04-16 16:19:11',0,'Logger History',NULL),
 (31,'e-emergency-zk','Utility.09.00','cococare.framework.zk.controller.zul.util.ZulApplicationSettingListCtrl',NULL,NULL,NULL,'2015-04-16 16:19:11',0,'Application Setting',NULL),
 (32,'e-emergency-zk','Utility.10.00','cococare.framework.zk.controller.zul.util.ZulDatabaseSettingCtrl',NULL,NULL,NULL,'2015-04-16 16:19:11',0,'Database Setting',NULL),
 (33,'e-emergency-zk','Utility.10.23','cococare.framework.zk.controller.zul.util.ZulDatabaseSettingCtrl.btnSave',NULL,NULL,NULL,'2015-04-16 16:19:11',0,'Save',32),
 (34,'e-emergency-zk','Utility.10.24','cococare.framework.zk.controller.zul.util.ZulDatabaseSettingCtrl.btnClose',NULL,NULL,NULL,'2015-04-16 16:19:11',0,'Close',32);
/*!40000 ALTER TABLE `util_privileges` ENABLE KEYS */;


--
-- Table structure for table `e_emergency`.`util_provinces`
--

DROP TABLE IF EXISTS `util_provinces`;
CREATE TABLE `util_provinces` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `code` varchar(8) DEFAULT NULL,
  `logChangedBy` varchar(32) DEFAULT NULL,
  `logChangedOn` datetime DEFAULT NULL,
  `logCreatedBy` varchar(32) DEFAULT NULL,
  `logCreatedOn` datetime DEFAULT NULL,
  `logSaveTimes` int(11) DEFAULT NULL,
  `name` varchar(48) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `e_emergency`.`util_provinces`
--

/*!40000 ALTER TABLE `util_provinces` DISABLE KEYS */;
INSERT INTO `util_provinces` (`id`,`code`,`logChangedBy`,`logChangedOn`,`logCreatedBy`,`logCreatedOn`,`logSaveTimes`,`name`) VALUES 
 (1,'P001',NULL,'2015-04-16 16:19:07','',NULL,1,'Aceh'),
 (2,'P002',NULL,'2015-04-16 16:19:07','',NULL,1,'Sumatera Utara'),
 (3,'P003',NULL,'2015-04-16 16:19:07','',NULL,1,'Sumatera Barat'),
 (4,'P004',NULL,'2015-04-16 16:19:07','',NULL,1,'Riau'),
 (5,'P005',NULL,'2015-04-16 16:19:07','',NULL,1,'Kepulauan Riau'),
 (6,'P006',NULL,'2015-04-16 16:19:07','',NULL,1,'Jambi'),
 (7,'P007',NULL,'2015-04-16 16:19:07','',NULL,1,'Bengkulu'),
 (8,'P008',NULL,'2015-04-16 16:19:07','',NULL,1,'Sumatera Selatan'),
 (9,'P009',NULL,'2015-04-16 16:19:07','',NULL,1,'Kepulauan Bangka Belitung'),
 (10,'P010',NULL,'2015-04-16 16:19:07','',NULL,1,'Lampung'),
 (11,'P011',NULL,'2015-04-16 16:19:07','',NULL,1,'Banten'),
 (12,'P012',NULL,'2015-04-16 16:19:07','',NULL,1,'Jawa Barat'),
 (13,'P013',NULL,'2015-04-16 16:19:07','',NULL,1,'DKI Jakarta'),
 (14,'P014',NULL,'2015-04-16 16:19:07','',NULL,1,'Jawa Tengah'),
 (15,'P015',NULL,'2015-04-16 16:19:07','',NULL,1,'Jawa Timur');
INSERT INTO `util_provinces` (`id`,`code`,`logChangedBy`,`logChangedOn`,`logCreatedBy`,`logCreatedOn`,`logSaveTimes`,`name`) VALUES 
 (16,'P016',NULL,'2015-04-16 16:19:07','',NULL,1,'DI Yogyakarta'),
 (17,'P017',NULL,'2015-04-16 16:19:07','',NULL,1,'Bali'),
 (18,'P018',NULL,'2015-04-16 16:19:07','',NULL,1,'Nusa Tenggara Barat'),
 (19,'P019',NULL,'2015-04-16 16:19:07','',NULL,1,'Nusa Tenggara Timur'),
 (20,'P020',NULL,'2015-04-16 16:19:07','',NULL,1,'Kalimantan Barat'),
 (21,'P021',NULL,'2015-04-16 16:19:07','',NULL,1,'Kalimantan Selatan'),
 (22,'P022',NULL,'2015-04-16 16:19:07','',NULL,1,'Kalimantan Tengah'),
 (23,'P023',NULL,'2015-04-16 16:19:07','',NULL,1,'Kalimantan Timur'),
 (24,'P024',NULL,'2015-04-16 16:19:07','',NULL,1,'Kalimantan Utara'),
 (25,'P025',NULL,'2015-04-16 16:19:07','',NULL,1,'Gorontalo'),
 (26,'P026',NULL,'2015-04-16 16:19:07','',NULL,1,'Sulawesi Selatan'),
 (27,'P027',NULL,'2015-04-16 16:19:07','',NULL,1,'Sulawesi Tenggara'),
 (28,'P028',NULL,'2015-04-16 16:19:07','',NULL,1,'Sulawesi Tengah'),
 (29,'P029',NULL,'2015-04-16 16:19:07','',NULL,1,'Sulawesi Utara');
INSERT INTO `util_provinces` (`id`,`code`,`logChangedBy`,`logChangedOn`,`logCreatedBy`,`logCreatedOn`,`logSaveTimes`,`name`) VALUES 
 (30,'P030',NULL,'2015-04-16 16:19:07','',NULL,1,'Sulawesi Barat'),
 (31,'P031',NULL,'2015-04-16 16:19:07','',NULL,1,'Maluku'),
 (32,'P032',NULL,'2015-04-16 16:19:07','',NULL,1,'Maluku Utara'),
 (33,'P033',NULL,'2015-04-16 16:19:07','',NULL,1,'Papua'),
 (34,'P034',NULL,'2015-04-16 16:19:07','',NULL,1,'Papua Barat');
/*!40000 ALTER TABLE `util_provinces` ENABLE KEYS */;


--
-- Table structure for table `e_emergency`.`util_regencies`
--

DROP TABLE IF EXISTS `util_regencies`;
CREATE TABLE `util_regencies` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `code` varchar(8) DEFAULT NULL,
  `logChangedBy` varchar(32) DEFAULT NULL,
  `logChangedOn` datetime DEFAULT NULL,
  `logCreatedBy` varchar(32) DEFAULT NULL,
  `logCreatedOn` datetime DEFAULT NULL,
  `logSaveTimes` int(11) DEFAULT NULL,
  `name` varchar(48) DEFAULT NULL,
  `province_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKEADD715443C44CF7` (`province_id`),
  CONSTRAINT `FKEADD715443C44CF7` FOREIGN KEY (`province_id`) REFERENCES `util_provinces` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=515 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `e_emergency`.`util_regencies`
--

/*!40000 ALTER TABLE `util_regencies` DISABLE KEYS */;
INSERT INTO `util_regencies` (`id`,`code`,`logChangedBy`,`logChangedOn`,`logCreatedBy`,`logCreatedOn`,`logSaveTimes`,`name`,`province_id`) VALUES 
 (1,'R001',NULL,'2015-04-16 16:19:10','',NULL,1,'Aceh Barat',1),
 (2,'R002',NULL,'2015-04-16 16:19:10','',NULL,1,'Aceh Barat Daya',1),
 (3,'R003',NULL,'2015-04-16 16:19:10','',NULL,1,'Aceh Besar',1),
 (4,'R004',NULL,'2015-04-16 16:19:10','',NULL,1,'Aceh Jaya',1),
 (5,'R005',NULL,'2015-04-16 16:19:10','',NULL,1,'Aceh Selatan',1),
 (6,'R006',NULL,'2015-04-16 16:19:10','',NULL,1,'Aceh Singkil',1),
 (7,'R007',NULL,'2015-04-16 16:19:10','',NULL,1,'Aceh Tamiang',1),
 (8,'R008',NULL,'2015-04-16 16:19:10','',NULL,1,'Aceh Tengah',1),
 (9,'R009',NULL,'2015-04-16 16:19:10','',NULL,1,'Aceh Tenggara',1),
 (10,'R010',NULL,'2015-04-16 16:19:10','',NULL,1,'Aceh Timur',1),
 (11,'R011',NULL,'2015-04-16 16:19:10','',NULL,1,'Aceh Utara',1),
 (12,'R012',NULL,'2015-04-16 16:19:10','',NULL,1,'Bener Meriah',1),
 (13,'R013',NULL,'2015-04-16 16:19:10','',NULL,1,'Bireuen',1),
 (14,'R014',NULL,'2015-04-16 16:19:10','',NULL,1,'Gayo Lues',1);
INSERT INTO `util_regencies` (`id`,`code`,`logChangedBy`,`logChangedOn`,`logCreatedBy`,`logCreatedOn`,`logSaveTimes`,`name`,`province_id`) VALUES 
 (15,'R015',NULL,'2015-04-16 16:19:10','',NULL,1,'Nagan Raya',1),
 (16,'R016',NULL,'2015-04-16 16:19:10','',NULL,1,'Pidie',1),
 (17,'R017',NULL,'2015-04-16 16:19:10','',NULL,1,'Pidie Jaya',1),
 (18,'R018',NULL,'2015-04-16 16:19:10','',NULL,1,'Simeulue',1),
 (19,'R019',NULL,'2015-04-16 16:19:10','',NULL,1,'Banda Aceh',1),
 (20,'R020',NULL,'2015-04-16 16:19:10','',NULL,1,'Langsa',1),
 (21,'R021',NULL,'2015-04-16 16:19:10','',NULL,1,'Lhokseumawe',1),
 (22,'R022',NULL,'2015-04-16 16:19:10','',NULL,1,'Sabang',1),
 (23,'R023',NULL,'2015-04-16 16:19:10','',NULL,1,'Subulussalam',1),
 (24,'R024',NULL,'2015-04-16 16:19:10','',NULL,1,'Asahan',2),
 (25,'R025',NULL,'2015-04-16 16:19:10','',NULL,1,'Batubara',2),
 (26,'R026',NULL,'2015-04-16 16:19:10','',NULL,1,'Dairi',2),
 (27,'R027',NULL,'2015-04-16 16:19:10','',NULL,1,'Deli Serdang',2),
 (28,'R028',NULL,'2015-04-16 16:19:10','',NULL,1,'Humbang Hasundutan',2),
 (29,'R029',NULL,'2015-04-16 16:19:10','',NULL,1,'Karo',2);
INSERT INTO `util_regencies` (`id`,`code`,`logChangedBy`,`logChangedOn`,`logCreatedBy`,`logCreatedOn`,`logSaveTimes`,`name`,`province_id`) VALUES 
 (30,'R030',NULL,'2015-04-16 16:19:10','',NULL,1,'Labuhanbatu',2),
 (31,'R031',NULL,'2015-04-16 16:19:10','',NULL,1,'Labuhanbatu Selatan',2),
 (32,'R032',NULL,'2015-04-16 16:19:10','',NULL,1,'Labuhanbatu Utara',2),
 (33,'R033',NULL,'2015-04-16 16:19:10','',NULL,1,'Langkat',2),
 (34,'R034',NULL,'2015-04-16 16:19:10','',NULL,1,'Mandailing Natal',2),
 (35,'R035',NULL,'2015-04-16 16:19:10','',NULL,1,'Nias',2),
 (36,'R036',NULL,'2015-04-16 16:19:10','',NULL,1,'Nias Barat',2),
 (37,'R037',NULL,'2015-04-16 16:19:10','',NULL,1,'Nias Selatan',2),
 (38,'R038',NULL,'2015-04-16 16:19:10','',NULL,1,'Nias Utara',2),
 (39,'R039',NULL,'2015-04-16 16:19:10','',NULL,1,'Padang Lawas',2),
 (40,'R040',NULL,'2015-04-16 16:19:10','',NULL,1,'Padang Lawas Utara',2),
 (41,'R041',NULL,'2015-04-16 16:19:10','',NULL,1,'Pakpak Bharat',2),
 (42,'R042',NULL,'2015-04-16 16:19:10','',NULL,1,'Samosir',2),
 (43,'R043',NULL,'2015-04-16 16:19:10','',NULL,1,'Serdang Bedagai',2);
INSERT INTO `util_regencies` (`id`,`code`,`logChangedBy`,`logChangedOn`,`logCreatedBy`,`logCreatedOn`,`logSaveTimes`,`name`,`province_id`) VALUES 
 (44,'R044',NULL,'2015-04-16 16:19:10','',NULL,1,'Simalungun',2),
 (45,'R045',NULL,'2015-04-16 16:19:10','',NULL,1,'Tapanuli Selatan',2),
 (46,'R046',NULL,'2015-04-16 16:19:10','',NULL,1,'Tapanuli Tengah',2),
 (47,'R047',NULL,'2015-04-16 16:19:10','',NULL,1,'Tapanuli Utara',2),
 (48,'R048',NULL,'2015-04-16 16:19:10','',NULL,1,'Toba Samosir',2),
 (49,'R049',NULL,'2015-04-16 16:19:10','',NULL,1,'Binjai',2),
 (50,'R050',NULL,'2015-04-16 16:19:10','',NULL,1,'Gunungsitoli',2),
 (51,'R051',NULL,'2015-04-16 16:19:10','',NULL,1,'Medan',2),
 (52,'R052',NULL,'2015-04-16 16:19:10','',NULL,1,'Padangsidempuan',2),
 (53,'R053',NULL,'2015-04-16 16:19:10','',NULL,1,'Pematangsiantar',2),
 (54,'R054',NULL,'2015-04-16 16:19:10','',NULL,1,'Sibolga',2),
 (55,'R055',NULL,'2015-04-16 16:19:10','',NULL,1,'Tanjungbalai',2),
 (56,'R056',NULL,'2015-04-16 16:19:10','',NULL,1,'Tebing Tinggi',2),
 (57,'R057',NULL,'2015-04-16 16:19:10','',NULL,1,'Agam',3);
INSERT INTO `util_regencies` (`id`,`code`,`logChangedBy`,`logChangedOn`,`logCreatedBy`,`logCreatedOn`,`logSaveTimes`,`name`,`province_id`) VALUES 
 (58,'R058',NULL,'2015-04-16 16:19:10','',NULL,1,'Dharmasraya',3),
 (59,'R059',NULL,'2015-04-16 16:19:10','',NULL,1,'Kepulauan Mentawai',3),
 (60,'R060',NULL,'2015-04-16 16:19:10','',NULL,1,'Lima Puluh Kota',3),
 (61,'R061',NULL,'2015-04-16 16:19:10','',NULL,1,'Padang Pariaman',3),
 (62,'R062',NULL,'2015-04-16 16:19:10','',NULL,1,'Pasaman',3),
 (63,'R063',NULL,'2015-04-16 16:19:10','',NULL,1,'Pasaman Barat',3),
 (64,'R064',NULL,'2015-04-16 16:19:10','',NULL,1,'Pesisir Selatan',3),
 (65,'R065',NULL,'2015-04-16 16:19:10','',NULL,1,'Sijunjung',3),
 (66,'R066',NULL,'2015-04-16 16:19:10','',NULL,1,'Solok',3),
 (67,'R067',NULL,'2015-04-16 16:19:10','',NULL,1,'Solok Selatan',3),
 (68,'R068',NULL,'2015-04-16 16:19:10','',NULL,1,'Tanah Datar',3),
 (69,'R069',NULL,'2015-04-16 16:19:10','',NULL,1,'Bukittinggi',3),
 (70,'R070',NULL,'2015-04-16 16:19:10','',NULL,1,'Padang',3),
 (71,'R071',NULL,'2015-04-16 16:19:10','',NULL,1,'Padangpanjang',3);
INSERT INTO `util_regencies` (`id`,`code`,`logChangedBy`,`logChangedOn`,`logCreatedBy`,`logCreatedOn`,`logSaveTimes`,`name`,`province_id`) VALUES 
 (72,'R072',NULL,'2015-04-16 16:19:10','',NULL,1,'Pariaman',3),
 (73,'R073',NULL,'2015-04-16 16:19:10','',NULL,1,'Payakumbuh',3),
 (74,'R074',NULL,'2015-04-16 16:19:10','',NULL,1,'Sawahlunto',3),
 (75,'R075',NULL,'2015-04-16 16:19:10','',NULL,1,'Solok',3),
 (76,'R076',NULL,'2015-04-16 16:19:10','',NULL,1,'Bengkalis',4),
 (77,'R077',NULL,'2015-04-16 16:19:10','',NULL,1,'Indragiri Hilir',4),
 (78,'R078',NULL,'2015-04-16 16:19:10','',NULL,1,'Indragiri Hulu',4),
 (79,'R079',NULL,'2015-04-16 16:19:10','',NULL,1,'Kampar',4),
 (80,'R080',NULL,'2015-04-16 16:19:10','',NULL,1,'Kepulauan Meranti',4),
 (81,'R081',NULL,'2015-04-16 16:19:10','',NULL,1,'Kuantan Singingi',4),
 (82,'R082',NULL,'2015-04-16 16:19:10','',NULL,1,'Pelalawan',4),
 (83,'R083',NULL,'2015-04-16 16:19:10','',NULL,1,'Rokan Hilir',4),
 (84,'R084',NULL,'2015-04-16 16:19:10','',NULL,1,'Rokan Hulu',4),
 (85,'R085',NULL,'2015-04-16 16:19:10','',NULL,1,'Siak',4);
INSERT INTO `util_regencies` (`id`,`code`,`logChangedBy`,`logChangedOn`,`logCreatedBy`,`logCreatedOn`,`logSaveTimes`,`name`,`province_id`) VALUES 
 (86,'R086',NULL,'2015-04-16 16:19:10','',NULL,1,'Dumai',4),
 (87,'R087',NULL,'2015-04-16 16:19:10','',NULL,1,'Pekanbaru',4),
 (88,'R088',NULL,'2015-04-16 16:19:10','',NULL,1,'Bintan',5),
 (89,'R089',NULL,'2015-04-16 16:19:10','',NULL,1,'Karimun',5),
 (90,'R090',NULL,'2015-04-16 16:19:10','',NULL,1,'Kepulauan Anambas',5),
 (91,'R091',NULL,'2015-04-16 16:19:10','',NULL,1,'Lingga',5),
 (92,'R092',NULL,'2015-04-16 16:19:10','',NULL,1,'Natuna',5),
 (93,'R093',NULL,'2015-04-16 16:19:10','',NULL,1,'Batam',5),
 (94,'R094',NULL,'2015-04-16 16:19:10','',NULL,1,'Tanjung Pinang',5),
 (95,'R095',NULL,'2015-04-16 16:19:10','',NULL,1,'Batanghari',6),
 (96,'R096',NULL,'2015-04-16 16:19:10','',NULL,1,'Bungo',6),
 (97,'R097',NULL,'2015-04-16 16:19:10','',NULL,1,'Kerinci',6),
 (98,'R098',NULL,'2015-04-16 16:19:10','',NULL,1,'Merangin',6),
 (99,'R099',NULL,'2015-04-16 16:19:10','',NULL,1,'Muaro Jambi',6),
 (100,'R100',NULL,'2015-04-16 16:19:10','',NULL,1,'Sarolangun',6);
INSERT INTO `util_regencies` (`id`,`code`,`logChangedBy`,`logChangedOn`,`logCreatedBy`,`logCreatedOn`,`logSaveTimes`,`name`,`province_id`) VALUES 
 (101,'R101',NULL,'2015-04-16 16:19:10','',NULL,1,'Tanjung Jabung Barat',6),
 (102,'R102',NULL,'2015-04-16 16:19:10','',NULL,1,'Tanjung Jabung Timur',6),
 (103,'R103',NULL,'2015-04-16 16:19:10','',NULL,1,'Tebo',6),
 (104,'R104',NULL,'2015-04-16 16:19:10','',NULL,1,'Jambi',6),
 (105,'R105',NULL,'2015-04-16 16:19:10','',NULL,1,'Sungai Penuh',6),
 (106,'R106',NULL,'2015-04-16 16:19:10','',NULL,1,'Bengkulu Selatan',7),
 (107,'R107',NULL,'2015-04-16 16:19:10','',NULL,1,'Bengkulu Tengah',7),
 (108,'R108',NULL,'2015-04-16 16:19:10','',NULL,1,'Bengkulu Utara',7),
 (109,'R109',NULL,'2015-04-16 16:19:10','',NULL,1,'Kaur',7),
 (110,'R110',NULL,'2015-04-16 16:19:10','',NULL,1,'Kepahiang',7),
 (111,'R111',NULL,'2015-04-16 16:19:10','',NULL,1,'Lebong',7),
 (112,'R112',NULL,'2015-04-16 16:19:10','',NULL,1,'Mukomuko',7),
 (113,'R113',NULL,'2015-04-16 16:19:10','',NULL,1,'Rejang Lebong',7),
 (114,'R114',NULL,'2015-04-16 16:19:10','',NULL,1,'Seluma',7);
INSERT INTO `util_regencies` (`id`,`code`,`logChangedBy`,`logChangedOn`,`logCreatedBy`,`logCreatedOn`,`logSaveTimes`,`name`,`province_id`) VALUES 
 (115,'R115',NULL,'2015-04-16 16:19:10','',NULL,1,'Bengkulu',7),
 (116,'R116',NULL,'2015-04-16 16:19:10','',NULL,1,'Banyuasin',8),
 (117,'R117',NULL,'2015-04-16 16:19:10','',NULL,1,'Empat Lawang',8),
 (118,'R118',NULL,'2015-04-16 16:19:10','',NULL,1,'Lahat',8),
 (119,'R119',NULL,'2015-04-16 16:19:10','',NULL,1,'Muara Enim',8),
 (120,'R120',NULL,'2015-04-16 16:19:10','',NULL,1,'Musi Banyuasin',8),
 (121,'R121',NULL,'2015-04-16 16:19:10','',NULL,1,'Musi Rawas',8),
 (122,'R122',NULL,'2015-04-16 16:19:10','',NULL,1,'Musi Rawas Utara',8),
 (123,'R123',NULL,'2015-04-16 16:19:10','',NULL,1,'Ogan Ilir',8),
 (124,'R124',NULL,'2015-04-16 16:19:10','',NULL,1,'Ogan Komering Ilir',8),
 (125,'R125',NULL,'2015-04-16 16:19:10','',NULL,1,'Ogan Komering Ulu',8),
 (126,'R126',NULL,'2015-04-16 16:19:10','',NULL,1,'Ogan Komering Ulu Selatan',8),
 (127,'R127',NULL,'2015-04-16 16:19:10','',NULL,1,'Ogan Komering Ulu Timur',8),
 (128,'R128',NULL,'2015-04-16 16:19:10','',NULL,1,'Penukal Abab Lematang Ilir',8);
INSERT INTO `util_regencies` (`id`,`code`,`logChangedBy`,`logChangedOn`,`logCreatedBy`,`logCreatedOn`,`logSaveTimes`,`name`,`province_id`) VALUES 
 (129,'R129',NULL,'2015-04-16 16:19:10','',NULL,1,'Lubuklinggau',8),
 (130,'R130',NULL,'2015-04-16 16:19:10','',NULL,1,'Pagar Alam',8),
 (131,'R131',NULL,'2015-04-16 16:19:10','',NULL,1,'Palembang',8),
 (132,'R132',NULL,'2015-04-16 16:19:10','',NULL,1,'Prabumulih',8),
 (133,'R133',NULL,'2015-04-16 16:19:10','',NULL,1,'Bangka',9),
 (134,'R134',NULL,'2015-04-16 16:19:10','',NULL,1,'Bangka Barat',9),
 (135,'R135',NULL,'2015-04-16 16:19:10','',NULL,1,'Bangka Selatan',9),
 (136,'R136',NULL,'2015-04-16 16:19:10','',NULL,1,'Bangka Tengah',9),
 (137,'R137',NULL,'2015-04-16 16:19:10','',NULL,1,'Belitung',9),
 (138,'R138',NULL,'2015-04-16 16:19:10','',NULL,1,'Belitung Timur',9),
 (139,'R139',NULL,'2015-04-16 16:19:10','',NULL,1,'Pangkal Pinang',9),
 (140,'R140',NULL,'2015-04-16 16:19:10','',NULL,1,'Lampung Tengah',10),
 (141,'R141',NULL,'2015-04-16 16:19:10','',NULL,1,'Lampung Utara',10),
 (142,'R142',NULL,'2015-04-16 16:19:10','',NULL,1,'Lampung Selatan',10);
INSERT INTO `util_regencies` (`id`,`code`,`logChangedBy`,`logChangedOn`,`logCreatedBy`,`logCreatedOn`,`logSaveTimes`,`name`,`province_id`) VALUES 
 (143,'R143',NULL,'2015-04-16 16:19:10','',NULL,1,'Lampung Barat',10),
 (144,'R144',NULL,'2015-04-16 16:19:10','',NULL,1,'Lampung Timur',10),
 (145,'R145',NULL,'2015-04-16 16:19:10','',NULL,1,'Mesuji',10),
 (146,'R146',NULL,'2015-04-16 16:19:10','',NULL,1,'Pesawaran',10),
 (147,'R147',NULL,'2015-04-16 16:19:10','',NULL,1,'Pesisir Barat',10),
 (148,'R148',NULL,'2015-04-16 16:19:10','',NULL,1,'Pringsewu',10),
 (149,'R149',NULL,'2015-04-16 16:19:10','',NULL,1,'Tulang Bawang',10),
 (150,'R150',NULL,'2015-04-16 16:19:10','',NULL,1,'Tulang Bawang Barat',10),
 (151,'R151',NULL,'2015-04-16 16:19:10','',NULL,1,'Tanggamus',10),
 (152,'R152',NULL,'2015-04-16 16:19:10','',NULL,1,'Way Kanan',10),
 (153,'R153',NULL,'2015-04-16 16:19:10','',NULL,1,'Bandar Lampung',10),
 (154,'R154',NULL,'2015-04-16 16:19:10','',NULL,1,'Metro',10),
 (155,'R155',NULL,'2015-04-16 16:19:10','',NULL,1,'Lebak',11),
 (156,'R156',NULL,'2015-04-16 16:19:10','',NULL,1,'Pandeglang',11);
INSERT INTO `util_regencies` (`id`,`code`,`logChangedBy`,`logChangedOn`,`logCreatedBy`,`logCreatedOn`,`logSaveTimes`,`name`,`province_id`) VALUES 
 (157,'R157',NULL,'2015-04-16 16:19:10','',NULL,1,'Serang',11),
 (158,'R158',NULL,'2015-04-16 16:19:10','',NULL,1,'Tangerang',11),
 (159,'R159',NULL,'2015-04-16 16:19:10','',NULL,1,'Cilegon',11),
 (160,'R160',NULL,'2015-04-16 16:19:10','',NULL,1,'Serang',11),
 (161,'R161',NULL,'2015-04-16 16:19:10','',NULL,1,'Tangerang',11),
 (162,'R162',NULL,'2015-04-16 16:19:10','',NULL,1,'Tangerang Selatan',11),
 (163,'R163',NULL,'2015-04-16 16:19:10','',NULL,1,'Bandung',12),
 (164,'R164',NULL,'2015-04-16 16:19:10','',NULL,1,'Bandung Barat',12),
 (165,'R165',NULL,'2015-04-16 16:19:10','',NULL,1,'Bekasi',12),
 (166,'R166',NULL,'2015-04-16 16:19:10','',NULL,1,'Bogor',12),
 (167,'R167',NULL,'2015-04-16 16:19:10','',NULL,1,'Ciamis',12),
 (168,'R168',NULL,'2015-04-16 16:19:10','',NULL,1,'Cianjur',12),
 (169,'R169',NULL,'2015-04-16 16:19:10','',NULL,1,'Cirebon',12),
 (170,'R170',NULL,'2015-04-16 16:19:10','',NULL,1,'Garut',12);
INSERT INTO `util_regencies` (`id`,`code`,`logChangedBy`,`logChangedOn`,`logCreatedBy`,`logCreatedOn`,`logSaveTimes`,`name`,`province_id`) VALUES 
 (171,'R171',NULL,'2015-04-16 16:19:10','',NULL,1,'Indramayu',12),
 (172,'R172',NULL,'2015-04-16 16:19:10','',NULL,1,'Karawang',12),
 (173,'R173',NULL,'2015-04-16 16:19:10','',NULL,1,'Kuningan',12),
 (174,'R174',NULL,'2015-04-16 16:19:10','',NULL,1,'Majalengka',12),
 (175,'R175',NULL,'2015-04-16 16:19:10','',NULL,1,'Pangandaran',12),
 (176,'R176',NULL,'2015-04-16 16:19:10','',NULL,1,'Purwakarta',12),
 (177,'R177',NULL,'2015-04-16 16:19:10','',NULL,1,'Subang',12),
 (178,'R178',NULL,'2015-04-16 16:19:10','',NULL,1,'Sukabumi',12),
 (179,'R179',NULL,'2015-04-16 16:19:10','',NULL,1,'Sumedang',12),
 (180,'R180',NULL,'2015-04-16 16:19:10','',NULL,1,'Tasikmalaya',12),
 (181,'R181',NULL,'2015-04-16 16:19:10','',NULL,1,'Bandung',12),
 (182,'R182',NULL,'2015-04-16 16:19:10','',NULL,1,'Banjar',12),
 (183,'R183',NULL,'2015-04-16 16:19:10','',NULL,1,'Bekasi',12),
 (184,'R184',NULL,'2015-04-16 16:19:10','',NULL,1,'Bogor',12);
INSERT INTO `util_regencies` (`id`,`code`,`logChangedBy`,`logChangedOn`,`logCreatedBy`,`logCreatedOn`,`logSaveTimes`,`name`,`province_id`) VALUES 
 (185,'R185',NULL,'2015-04-16 16:19:10','',NULL,1,'Cimahi',12),
 (186,'R186',NULL,'2015-04-16 16:19:10','',NULL,1,'Cirebon',12),
 (187,'R187',NULL,'2015-04-16 16:19:10','',NULL,1,'Depok',12),
 (188,'R188',NULL,'2015-04-16 16:19:10','',NULL,1,'Sukabumi',12),
 (189,'R189',NULL,'2015-04-16 16:19:10','',NULL,1,'Tasikmalaya',12),
 (190,'R190',NULL,'2015-04-16 16:19:10','',NULL,1,'Kepulauan Seribu',13),
 (191,'R191',NULL,'2015-04-16 16:19:10','',NULL,1,'Jakarta Barat',13),
 (192,'R192',NULL,'2015-04-16 16:19:10','',NULL,1,'Jakarta Pusat',13),
 (193,'R193',NULL,'2015-04-16 16:19:10','',NULL,1,'Jakarta Selatan',13),
 (194,'R194',NULL,'2015-04-16 16:19:10','',NULL,1,'Jakarta Timur',13),
 (195,'R195',NULL,'2015-04-16 16:19:10','',NULL,1,'Jakarta Utara',13),
 (196,'R196',NULL,'2015-04-16 16:19:10','',NULL,1,'Banjarnegara',14),
 (197,'R197',NULL,'2015-04-16 16:19:10','',NULL,1,'Banyumas',14),
 (198,'R198',NULL,'2015-04-16 16:19:10','',NULL,1,'Batang',14);
INSERT INTO `util_regencies` (`id`,`code`,`logChangedBy`,`logChangedOn`,`logCreatedBy`,`logCreatedOn`,`logSaveTimes`,`name`,`province_id`) VALUES 
 (199,'R199',NULL,'2015-04-16 16:19:10','',NULL,1,'Blora',14),
 (200,'R200',NULL,'2015-04-16 16:19:10','',NULL,1,'Boyolali',14),
 (201,'R201',NULL,'2015-04-16 16:19:10','',NULL,1,'Brebes',14),
 (202,'R202',NULL,'2015-04-16 16:19:10','',NULL,1,'Cilacap',14),
 (203,'R203',NULL,'2015-04-16 16:19:10','',NULL,1,'Demak',14),
 (204,'R204',NULL,'2015-04-16 16:19:10','',NULL,1,'Grobogan',14),
 (205,'R205',NULL,'2015-04-16 16:19:10','',NULL,1,'Jepara',14),
 (206,'R206',NULL,'2015-04-16 16:19:10','',NULL,1,'Karanganyar',14),
 (207,'R207',NULL,'2015-04-16 16:19:10','',NULL,1,'Kebumen',14),
 (208,'R208',NULL,'2015-04-16 16:19:10','',NULL,1,'Kendal',14),
 (209,'R209',NULL,'2015-04-16 16:19:10','',NULL,1,'Klaten',14),
 (210,'R210',NULL,'2015-04-16 16:19:10','',NULL,1,'Kudus',14),
 (211,'R211',NULL,'2015-04-16 16:19:10','',NULL,1,'Magelang',14),
 (212,'R212',NULL,'2015-04-16 16:19:10','',NULL,1,'Pati',14),
 (213,'R213',NULL,'2015-04-16 16:19:10','',NULL,1,'Pekalongan',14);
INSERT INTO `util_regencies` (`id`,`code`,`logChangedBy`,`logChangedOn`,`logCreatedBy`,`logCreatedOn`,`logSaveTimes`,`name`,`province_id`) VALUES 
 (214,'R214',NULL,'2015-04-16 16:19:10','',NULL,1,'Pemalang',14),
 (215,'R215',NULL,'2015-04-16 16:19:10','',NULL,1,'Purbalingga',14),
 (216,'R216',NULL,'2015-04-16 16:19:10','',NULL,1,'Purworejo',14),
 (217,'R217',NULL,'2015-04-16 16:19:10','',NULL,1,'Rembang',14),
 (218,'R218',NULL,'2015-04-16 16:19:10','',NULL,1,'Semarang',14),
 (219,'R219',NULL,'2015-04-16 16:19:10','',NULL,1,'Sragen',14),
 (220,'R220',NULL,'2015-04-16 16:19:10','',NULL,1,'Sukoharjo',14),
 (221,'R221',NULL,'2015-04-16 16:19:10','',NULL,1,'Tegal',14),
 (222,'R222',NULL,'2015-04-16 16:19:10','',NULL,1,'Temanggung',14),
 (223,'R223',NULL,'2015-04-16 16:19:10','',NULL,1,'Wonogiri',14),
 (224,'R224',NULL,'2015-04-16 16:19:10','',NULL,1,'Wonosobo',14),
 (225,'R225',NULL,'2015-04-16 16:19:10','',NULL,1,'Magelang',14),
 (226,'R226',NULL,'2015-04-16 16:19:10','',NULL,1,'Pekalongan',14),
 (227,'R227',NULL,'2015-04-16 16:19:10','',NULL,1,'Salatiga',14);
INSERT INTO `util_regencies` (`id`,`code`,`logChangedBy`,`logChangedOn`,`logCreatedBy`,`logCreatedOn`,`logSaveTimes`,`name`,`province_id`) VALUES 
 (228,'R228',NULL,'2015-04-16 16:19:10','',NULL,1,'Semarang',14),
 (229,'R229',NULL,'2015-04-16 16:19:10','',NULL,1,'Surakarta',14),
 (230,'R230',NULL,'2015-04-16 16:19:10','',NULL,1,'Tegal',14),
 (231,'R231',NULL,'2015-04-16 16:19:10','',NULL,1,'Bangkalan',15),
 (232,'R232',NULL,'2015-04-16 16:19:10','',NULL,1,'Banyuwangi',15),
 (233,'R233',NULL,'2015-04-16 16:19:10','',NULL,1,'Blitar',15),
 (234,'R234',NULL,'2015-04-16 16:19:10','',NULL,1,'Bojonegoro',15),
 (235,'R235',NULL,'2015-04-16 16:19:10','',NULL,1,'Bondowoso',15),
 (236,'R236',NULL,'2015-04-16 16:19:10','',NULL,1,'Gresik',15),
 (237,'R237',NULL,'2015-04-16 16:19:10','',NULL,1,'Jember',15),
 (238,'R238',NULL,'2015-04-16 16:19:10','',NULL,1,'Jombang',15),
 (239,'R239',NULL,'2015-04-16 16:19:10','',NULL,1,'Kediri',15),
 (240,'R240',NULL,'2015-04-16 16:19:10','',NULL,1,'Lamongan',15),
 (241,'R241',NULL,'2015-04-16 16:19:10','',NULL,1,'Lumajang',15);
INSERT INTO `util_regencies` (`id`,`code`,`logChangedBy`,`logChangedOn`,`logCreatedBy`,`logCreatedOn`,`logSaveTimes`,`name`,`province_id`) VALUES 
 (242,'R242',NULL,'2015-04-16 16:19:10','',NULL,1,'Madiun',15),
 (243,'R243',NULL,'2015-04-16 16:19:10','',NULL,1,'Magetan',15),
 (244,'R244',NULL,'2015-04-16 16:19:10','',NULL,1,'Malang',15),
 (245,'R245',NULL,'2015-04-16 16:19:10','',NULL,1,'Mojokerto',15),
 (246,'R246',NULL,'2015-04-16 16:19:10','',NULL,1,'Nganjuk',15),
 (247,'R247',NULL,'2015-04-16 16:19:10','',NULL,1,'Ngawi',15),
 (248,'R248',NULL,'2015-04-16 16:19:10','',NULL,1,'Pacitan',15),
 (249,'R249',NULL,'2015-04-16 16:19:10','',NULL,1,'Pamekasan',15),
 (250,'R250',NULL,'2015-04-16 16:19:10','',NULL,1,'Pasuruan',15),
 (251,'R251',NULL,'2015-04-16 16:19:10','',NULL,1,'Ponorogo',15),
 (252,'R252',NULL,'2015-04-16 16:19:10','',NULL,1,'Probolinggo',15),
 (253,'R253',NULL,'2015-04-16 16:19:10','',NULL,1,'Sampang',15),
 (254,'R254',NULL,'2015-04-16 16:19:10','',NULL,1,'Sidoarjo',15),
 (255,'R255',NULL,'2015-04-16 16:19:10','',NULL,1,'Situbondo',15),
 (256,'R256',NULL,'2015-04-16 16:19:10','',NULL,1,'Sumenep',15);
INSERT INTO `util_regencies` (`id`,`code`,`logChangedBy`,`logChangedOn`,`logCreatedBy`,`logCreatedOn`,`logSaveTimes`,`name`,`province_id`) VALUES 
 (257,'R257',NULL,'2015-04-16 16:19:10','',NULL,1,'Trenggalek',15),
 (258,'R258',NULL,'2015-04-16 16:19:10','',NULL,1,'Tuban',15),
 (259,'R259',NULL,'2015-04-16 16:19:10','',NULL,1,'Tulungagung',15),
 (260,'R260',NULL,'2015-04-16 16:19:10','',NULL,1,'Batu[12]',15),
 (261,'R261',NULL,'2015-04-16 16:19:10','',NULL,1,'Blitar',15),
 (262,'R262',NULL,'2015-04-16 16:19:10','',NULL,1,'Kediri',15),
 (263,'R263',NULL,'2015-04-16 16:19:10','',NULL,1,'Madiun',15),
 (264,'R264',NULL,'2015-04-16 16:19:10','',NULL,1,'Malang',15),
 (265,'R265',NULL,'2015-04-16 16:19:10','',NULL,1,'Mojokerto',15),
 (266,'R266',NULL,'2015-04-16 16:19:10','',NULL,1,'Pasuruan',15),
 (267,'R267',NULL,'2015-04-16 16:19:10','',NULL,1,'Probolinggo',15),
 (268,'R268',NULL,'2015-04-16 16:19:10','',NULL,1,'Surabaya',15),
 (269,'R269',NULL,'2015-04-16 16:19:10','',NULL,1,'Bantul',16),
 (270,'R270',NULL,'2015-04-16 16:19:10','',NULL,1,'Gunungkidul',16);
INSERT INTO `util_regencies` (`id`,`code`,`logChangedBy`,`logChangedOn`,`logCreatedBy`,`logCreatedOn`,`logSaveTimes`,`name`,`province_id`) VALUES 
 (271,'R271',NULL,'2015-04-16 16:19:10','',NULL,1,'Kulon Progo',16),
 (272,'R272',NULL,'2015-04-16 16:19:10','',NULL,1,'Sleman',16),
 (273,'R273',NULL,'2015-04-16 16:19:10','',NULL,1,'Yogyakarta',16),
 (274,'R274',NULL,'2015-04-16 16:19:10','',NULL,1,'Badung',17),
 (275,'R275',NULL,'2015-04-16 16:19:10','',NULL,1,'Bangli',17),
 (276,'R276',NULL,'2015-04-16 16:19:10','',NULL,1,'Buleleng',17),
 (277,'R277',NULL,'2015-04-16 16:19:10','',NULL,1,'Gianyar',17),
 (278,'R278',NULL,'2015-04-16 16:19:10','',NULL,1,'Jembrana',17),
 (279,'R279',NULL,'2015-04-16 16:19:10','',NULL,1,'Karangasem',17),
 (280,'R280',NULL,'2015-04-16 16:19:10','',NULL,1,'Klungkung',17),
 (281,'R281',NULL,'2015-04-16 16:19:10','',NULL,1,'Tabanan',17),
 (282,'R282',NULL,'2015-04-16 16:19:10','',NULL,1,'Denpasar',17),
 (283,'R283',NULL,'2015-04-16 16:19:10','',NULL,1,'Bima',18),
 (284,'R284',NULL,'2015-04-16 16:19:10','',NULL,1,'Dompu',18);
INSERT INTO `util_regencies` (`id`,`code`,`logChangedBy`,`logChangedOn`,`logCreatedBy`,`logCreatedOn`,`logSaveTimes`,`name`,`province_id`) VALUES 
 (285,'R285',NULL,'2015-04-16 16:19:10','',NULL,1,'Lombok Barat',18),
 (286,'R286',NULL,'2015-04-16 16:19:10','',NULL,1,'Lombok Tengah',18),
 (287,'R287',NULL,'2015-04-16 16:19:10','',NULL,1,'Lombok Timur',18),
 (288,'R288',NULL,'2015-04-16 16:19:10','',NULL,1,'Lombok Utara',18),
 (289,'R289',NULL,'2015-04-16 16:19:10','',NULL,1,'Sumbawa',18),
 (290,'R290',NULL,'2015-04-16 16:19:10','',NULL,1,'Sumbawa Barat',18),
 (291,'R291',NULL,'2015-04-16 16:19:10','',NULL,1,'Bima',18),
 (292,'R292',NULL,'2015-04-16 16:19:10','',NULL,1,'Mataram',18),
 (293,'R293',NULL,'2015-04-16 16:19:10','',NULL,1,'Alor',19),
 (294,'R294',NULL,'2015-04-16 16:19:10','',NULL,1,'Belu',19),
 (295,'R295',NULL,'2015-04-16 16:19:10','',NULL,1,'Ende',19),
 (296,'R296',NULL,'2015-04-16 16:19:10','',NULL,1,'Flores Timur',19),
 (297,'R297',NULL,'2015-04-16 16:19:10','',NULL,1,'Kupang',19),
 (298,'R298',NULL,'2015-04-16 16:19:10','',NULL,1,'Lembata',19);
INSERT INTO `util_regencies` (`id`,`code`,`logChangedBy`,`logChangedOn`,`logCreatedBy`,`logCreatedOn`,`logSaveTimes`,`name`,`province_id`) VALUES 
 (299,'R299',NULL,'2015-04-16 16:19:10','',NULL,1,'Malaka',19),
 (300,'R300',NULL,'2015-04-16 16:19:10','',NULL,1,'Manggarai',19),
 (301,'R301',NULL,'2015-04-16 16:19:10','',NULL,1,'Manggarai Barat',19),
 (302,'R302',NULL,'2015-04-16 16:19:10','',NULL,1,'Manggarai Timur',19),
 (303,'R303',NULL,'2015-04-16 16:19:10','',NULL,1,'Ngada',19),
 (304,'R304',NULL,'2015-04-16 16:19:10','',NULL,1,'Nagekeo',19),
 (305,'R305',NULL,'2015-04-16 16:19:10','',NULL,1,'Rote Ndao',19),
 (306,'R306',NULL,'2015-04-16 16:19:10','',NULL,1,'Sabu Raijua',19),
 (307,'R307',NULL,'2015-04-16 16:19:10','',NULL,1,'Sikka',19),
 (308,'R308',NULL,'2015-04-16 16:19:10','',NULL,1,'Sumba Barat',19),
 (309,'R309',NULL,'2015-04-16 16:19:10','',NULL,1,'Sumba Barat Daya',19),
 (310,'R310',NULL,'2015-04-16 16:19:10','',NULL,1,'Sumba Tengah',19),
 (311,'R311',NULL,'2015-04-16 16:19:10','',NULL,1,'Sumba Timur',19),
 (312,'R312',NULL,'2015-04-16 16:19:10','',NULL,1,'Timor Tengah Selatan',19);
INSERT INTO `util_regencies` (`id`,`code`,`logChangedBy`,`logChangedOn`,`logCreatedBy`,`logCreatedOn`,`logSaveTimes`,`name`,`province_id`) VALUES 
 (313,'R313',NULL,'2015-04-16 16:19:10','',NULL,1,'Timor Tengah Utara',19),
 (314,'R314',NULL,'2015-04-16 16:19:10','',NULL,1,'Kupang',19),
 (315,'R315',NULL,'2015-04-16 16:19:10','',NULL,1,'Bengkayang',20),
 (316,'R316',NULL,'2015-04-16 16:19:10','',NULL,1,'Kapuas Hulu',20),
 (317,'R317',NULL,'2015-04-16 16:19:10','',NULL,1,'Kayong Utara',20),
 (318,'R318',NULL,'2015-04-16 16:19:10','',NULL,1,'Ketapang',20),
 (319,'R319',NULL,'2015-04-16 16:19:10','',NULL,1,'Kubu Raya',20),
 (320,'R320',NULL,'2015-04-16 16:19:10','',NULL,1,'Landak',20),
 (321,'R321',NULL,'2015-04-16 16:19:10','',NULL,1,'Melawi',20),
 (322,'R322',NULL,'2015-04-16 16:19:10','',NULL,1,'Mempawah',20),
 (323,'R323',NULL,'2015-04-16 16:19:10','',NULL,1,'Sambas',20),
 (324,'R324',NULL,'2015-04-16 16:19:10','',NULL,1,'Sanggau',20),
 (325,'R325',NULL,'2015-04-16 16:19:10','',NULL,1,'Sekadau',20),
 (326,'R326',NULL,'2015-04-16 16:19:10','',NULL,1,'Sintang',20);
INSERT INTO `util_regencies` (`id`,`code`,`logChangedBy`,`logChangedOn`,`logCreatedBy`,`logCreatedOn`,`logSaveTimes`,`name`,`province_id`) VALUES 
 (327,'R327',NULL,'2015-04-16 16:19:10','',NULL,1,'Pontianak',20),
 (328,'R328',NULL,'2015-04-16 16:19:10','',NULL,1,'Singkawang',20),
 (329,'R329',NULL,'2015-04-16 16:19:10','',NULL,1,'Balangan',21),
 (330,'R330',NULL,'2015-04-16 16:19:10','',NULL,1,'Banjar',21),
 (331,'R331',NULL,'2015-04-16 16:19:10','',NULL,1,'Barito Kuala',21),
 (332,'R332',NULL,'2015-04-16 16:19:10','',NULL,1,'Hulu Sungai Selatan',21),
 (333,'R333',NULL,'2015-04-16 16:19:10','',NULL,1,'Hulu Sungai Tengah',21),
 (334,'R334',NULL,'2015-04-16 16:19:10','',NULL,1,'Hulu Sungai Utara',21),
 (335,'R335',NULL,'2015-04-16 16:19:10','',NULL,1,'Kotabaru',21),
 (336,'R336',NULL,'2015-04-16 16:19:10','',NULL,1,'Tabalong',21),
 (337,'R337',NULL,'2015-04-16 16:19:10','',NULL,1,'Tanah Bumbu',21),
 (338,'R338',NULL,'2015-04-16 16:19:10','',NULL,1,'Tanah Laut',21),
 (339,'R339',NULL,'2015-04-16 16:19:10','',NULL,1,'Tapin',21),
 (340,'R340',NULL,'2015-04-16 16:19:10','',NULL,1,'Banjarbaru',21);
INSERT INTO `util_regencies` (`id`,`code`,`logChangedBy`,`logChangedOn`,`logCreatedBy`,`logCreatedOn`,`logSaveTimes`,`name`,`province_id`) VALUES 
 (341,'R341',NULL,'2015-04-16 16:19:10','',NULL,1,'Banjarmasin',21),
 (342,'R342',NULL,'2015-04-16 16:19:10','',NULL,1,'Barito Selatan',22),
 (343,'R343',NULL,'2015-04-16 16:19:10','',NULL,1,'Barito Timur',22),
 (344,'R344',NULL,'2015-04-16 16:19:10','',NULL,1,'Barito Utara',22),
 (345,'R345',NULL,'2015-04-16 16:19:10','',NULL,1,'Gunung Mas',22),
 (346,'R346',NULL,'2015-04-16 16:19:10','',NULL,1,'Kapuas',22),
 (347,'R347',NULL,'2015-04-16 16:19:10','',NULL,1,'Katingan',22),
 (348,'R348',NULL,'2015-04-16 16:19:10','',NULL,1,'Kotawaringin Barat',22),
 (349,'R349',NULL,'2015-04-16 16:19:10','',NULL,1,'Kotawaringin Timur',22),
 (350,'R350',NULL,'2015-04-16 16:19:10','',NULL,1,'Lamandau',22),
 (351,'R351',NULL,'2015-04-16 16:19:10','',NULL,1,'Murung Raya',22),
 (352,'R352',NULL,'2015-04-16 16:19:10','',NULL,1,'Pulang Pisau',22),
 (353,'R353',NULL,'2015-04-16 16:19:10','',NULL,1,'Sukamara',22),
 (354,'R354',NULL,'2015-04-16 16:19:10','',NULL,1,'Seruyan',22);
INSERT INTO `util_regencies` (`id`,`code`,`logChangedBy`,`logChangedOn`,`logCreatedBy`,`logCreatedOn`,`logSaveTimes`,`name`,`province_id`) VALUES 
 (355,'R355',NULL,'2015-04-16 16:19:10','',NULL,1,'Palangka Raya',22),
 (356,'R356',NULL,'2015-04-16 16:19:10','',NULL,1,'Berau',23),
 (357,'R357',NULL,'2015-04-16 16:19:10','',NULL,1,'Kutai Barat',23),
 (358,'R358',NULL,'2015-04-16 16:19:10','',NULL,1,'Kutai Kartanegara',23),
 (359,'R359',NULL,'2015-04-16 16:19:10','',NULL,1,'Kutai Timur',23),
 (360,'R360',NULL,'2015-04-16 16:19:10','',NULL,1,'Mahakam Ulu',23),
 (361,'R361',NULL,'2015-04-16 16:19:10','',NULL,1,'Paser',23),
 (362,'R362',NULL,'2015-04-16 16:19:10','',NULL,1,'Penajam Paser Utara',23),
 (363,'R363',NULL,'2015-04-16 16:19:10','',NULL,1,'Balikpapan',23),
 (364,'R364',NULL,'2015-04-16 16:19:10','',NULL,1,'Bontang',23),
 (365,'R365',NULL,'2015-04-16 16:19:10','',NULL,1,'Samarinda',23),
 (366,'R366',NULL,'2015-04-16 16:19:10','',NULL,1,'Bulungan',24),
 (367,'R367',NULL,'2015-04-16 16:19:10','',NULL,1,'Malinau',24),
 (368,'R368',NULL,'2015-04-16 16:19:10','',NULL,1,'Nunukan',24);
INSERT INTO `util_regencies` (`id`,`code`,`logChangedBy`,`logChangedOn`,`logCreatedBy`,`logCreatedOn`,`logSaveTimes`,`name`,`province_id`) VALUES 
 (369,'R369',NULL,'2015-04-16 16:19:10','',NULL,1,'Tana Tidung',24),
 (370,'R370',NULL,'2015-04-16 16:19:10','',NULL,1,'Tarakan',24),
 (371,'R371',NULL,'2015-04-16 16:19:10','',NULL,1,'Boalemo',25),
 (372,'R372',NULL,'2015-04-16 16:19:10','',NULL,1,'Bone Bolango',25),
 (373,'R373',NULL,'2015-04-16 16:19:10','',NULL,1,'Gorontalo',25),
 (374,'R374',NULL,'2015-04-16 16:19:10','',NULL,1,'Gorontalo Utara',25),
 (375,'R375',NULL,'2015-04-16 16:19:10','',NULL,1,'Pohuwato',25),
 (376,'R376',NULL,'2015-04-16 16:19:10','',NULL,1,'Gorontalo',25),
 (377,'R377',NULL,'2015-04-16 16:19:10','',NULL,1,'Bantaeng',26),
 (378,'R378',NULL,'2015-04-16 16:19:10','',NULL,1,'Barru',26),
 (379,'R379',NULL,'2015-04-16 16:19:10','',NULL,1,'Bone',26),
 (380,'R380',NULL,'2015-04-16 16:19:10','',NULL,1,'Bulukumba',26),
 (381,'R381',NULL,'2015-04-16 16:19:10','',NULL,1,'Enrekang',26),
 (382,'R382',NULL,'2015-04-16 16:19:10','',NULL,1,'Gowa',26);
INSERT INTO `util_regencies` (`id`,`code`,`logChangedBy`,`logChangedOn`,`logCreatedBy`,`logCreatedOn`,`logSaveTimes`,`name`,`province_id`) VALUES 
 (383,'R383',NULL,'2015-04-16 16:19:10','',NULL,1,'Jeneponto',26),
 (384,'R384',NULL,'2015-04-16 16:19:10','',NULL,1,'Kepulauan Selayar',26),
 (385,'R385',NULL,'2015-04-16 16:19:10','',NULL,1,'Luwu',26),
 (386,'R386',NULL,'2015-04-16 16:19:10','',NULL,1,'Luwu Timur',26),
 (387,'R387',NULL,'2015-04-16 16:19:10','',NULL,1,'Luwu Utara',26),
 (388,'R388',NULL,'2015-04-16 16:19:10','',NULL,1,'Maros',26),
 (389,'R389',NULL,'2015-04-16 16:19:10','',NULL,1,'Pangkajene dan Kepulauan',26),
 (390,'R390',NULL,'2015-04-16 16:19:10','',NULL,1,'Pinrang',26),
 (391,'R391',NULL,'2015-04-16 16:19:10','',NULL,1,'Sidenreng Rappang',26),
 (392,'R392',NULL,'2015-04-16 16:19:10','',NULL,1,'Sinjai',26),
 (393,'R393',NULL,'2015-04-16 16:19:10','',NULL,1,'Soppeng',26),
 (394,'R394',NULL,'2015-04-16 16:19:10','',NULL,1,'Takalar',26),
 (395,'R395',NULL,'2015-04-16 16:19:10','',NULL,1,'Tana Toraja',26),
 (396,'R396',NULL,'2015-04-16 16:19:10','',NULL,1,'Toraja Utara',26);
INSERT INTO `util_regencies` (`id`,`code`,`logChangedBy`,`logChangedOn`,`logCreatedBy`,`logCreatedOn`,`logSaveTimes`,`name`,`province_id`) VALUES 
 (397,'R397',NULL,'2015-04-16 16:19:10','',NULL,1,'Wajo',26),
 (398,'R398',NULL,'2015-04-16 16:19:10','',NULL,1,'Makassar',26),
 (399,'R399',NULL,'2015-04-16 16:19:10','',NULL,1,'Palopo',26),
 (400,'R400',NULL,'2015-04-16 16:19:10','',NULL,1,'Parepare',26),
 (401,'R401',NULL,'2015-04-16 16:19:10','',NULL,1,'Bombana',27),
 (402,'R402',NULL,'2015-04-16 16:19:10','',NULL,1,'Buton',27),
 (403,'R403',NULL,'2015-04-16 16:19:10','',NULL,1,'Buton Selatan',27),
 (404,'R404',NULL,'2015-04-16 16:19:10','',NULL,1,'Buton Tengah',27),
 (405,'R405',NULL,'2015-04-16 16:19:10','',NULL,1,'Buton Utara',27),
 (406,'R406',NULL,'2015-04-16 16:19:10','',NULL,1,'Kolaka',27),
 (407,'R407',NULL,'2015-04-16 16:19:10','',NULL,1,'Kolaka Timur',27),
 (408,'R408',NULL,'2015-04-16 16:19:10','',NULL,1,'Kolaka Utara',27),
 (409,'R409',NULL,'2015-04-16 16:19:10','',NULL,1,'Konawe',27),
 (410,'R410',NULL,'2015-04-16 16:19:10','',NULL,1,'Konawe Kepulauan',27);
INSERT INTO `util_regencies` (`id`,`code`,`logChangedBy`,`logChangedOn`,`logCreatedBy`,`logCreatedOn`,`logSaveTimes`,`name`,`province_id`) VALUES 
 (411,'R411',NULL,'2015-04-16 16:19:10','',NULL,1,'Konawe Selatan',27),
 (412,'R412',NULL,'2015-04-16 16:19:10','',NULL,1,'Konawe Utara',27),
 (413,'R413',NULL,'2015-04-16 16:19:10','',NULL,1,'Muna',27),
 (414,'R414',NULL,'2015-04-16 16:19:10','',NULL,1,'Muna Barat',27),
 (415,'R415',NULL,'2015-04-16 16:19:10','',NULL,1,'Wakatobi',27),
 (416,'R416',NULL,'2015-04-16 16:19:10','',NULL,1,'Bau-Bau',27),
 (417,'R417',NULL,'2015-04-16 16:19:10','',NULL,1,'Kendari',27),
 (418,'R418',NULL,'2015-04-16 16:19:10','',NULL,1,'Banggai',28),
 (419,'R419',NULL,'2015-04-16 16:19:10','',NULL,1,'Banggai Kepulauan',28),
 (420,'R420',NULL,'2015-04-16 16:19:10','',NULL,1,'Banggai Laut',28),
 (421,'R421',NULL,'2015-04-16 16:19:10','',NULL,1,'Buol',28),
 (422,'R422',NULL,'2015-04-16 16:19:10','',NULL,1,'Donggala',28),
 (423,'R423',NULL,'2015-04-16 16:19:10','',NULL,1,'Morowali',28),
 (424,'R424',NULL,'2015-04-16 16:19:10','',NULL,1,'Morowali Utara',28);
INSERT INTO `util_regencies` (`id`,`code`,`logChangedBy`,`logChangedOn`,`logCreatedBy`,`logCreatedOn`,`logSaveTimes`,`name`,`province_id`) VALUES 
 (425,'R425',NULL,'2015-04-16 16:19:10','',NULL,1,'Parigi Moutong',28),
 (426,'R426',NULL,'2015-04-16 16:19:10','',NULL,1,'Poso',28),
 (427,'R427',NULL,'2015-04-16 16:19:10','',NULL,1,'Sigi',28),
 (428,'R428',NULL,'2015-04-16 16:19:10','',NULL,1,'Tojo Una-Una',28),
 (429,'R429',NULL,'2015-04-16 16:19:10','',NULL,1,'Toli-Toli',28),
 (430,'R430',NULL,'2015-04-16 16:19:10','',NULL,1,'Palu',28),
 (431,'R431',NULL,'2015-04-16 16:19:10','',NULL,1,'Bolaang Mongondow',29),
 (432,'R432',NULL,'2015-04-16 16:19:10','',NULL,1,'Bolaang Mongondow Selatan',29),
 (433,'R433',NULL,'2015-04-16 16:19:10','',NULL,1,'Bolaang Mongondow Timur',29),
 (434,'R434',NULL,'2015-04-16 16:19:10','',NULL,1,'Bolaang Mongondow Utara',29),
 (435,'R435',NULL,'2015-04-16 16:19:10','',NULL,1,'Kepulauan Sangihe',29),
 (436,'R436',NULL,'2015-04-16 16:19:10','',NULL,1,'Kepulauan Siau Tagulandang Biaro',29),
 (437,'R437',NULL,'2015-04-16 16:19:10','',NULL,1,'Kepulauan Talaud',29);
INSERT INTO `util_regencies` (`id`,`code`,`logChangedBy`,`logChangedOn`,`logCreatedBy`,`logCreatedOn`,`logSaveTimes`,`name`,`province_id`) VALUES 
 (438,'R438',NULL,'2015-04-16 16:19:10','',NULL,1,'Minahasa',29),
 (439,'R439',NULL,'2015-04-16 16:19:10','',NULL,1,'Minahasa Selatan',29),
 (440,'R440',NULL,'2015-04-16 16:19:10','',NULL,1,'Minahasa Tenggara',29),
 (441,'R441',NULL,'2015-04-16 16:19:10','',NULL,1,'Minahasa Utara',29),
 (442,'R442',NULL,'2015-04-16 16:19:10','',NULL,1,'Bitung',29),
 (443,'R443',NULL,'2015-04-16 16:19:10','',NULL,1,'Kotamobagu',29),
 (444,'R444',NULL,'2015-04-16 16:19:10','',NULL,1,'Manado',29),
 (445,'R445',NULL,'2015-04-16 16:19:10','',NULL,1,'Tomohon',29),
 (446,'R446',NULL,'2015-04-16 16:19:10','',NULL,1,'Majene',30),
 (447,'R447',NULL,'2015-04-16 16:19:10','',NULL,1,'Mamasa',30),
 (448,'R448',NULL,'2015-04-16 16:19:10','',NULL,1,'Mamuju',30),
 (449,'R449',NULL,'2015-04-16 16:19:10','',NULL,1,'Mamuju Tengah',30),
 (450,'R450',NULL,'2015-04-16 16:19:10','',NULL,1,'Mamuju Utara',30),
 (451,'R451',NULL,'2015-04-16 16:19:10','',NULL,1,'Polewali Mandar',30);
INSERT INTO `util_regencies` (`id`,`code`,`logChangedBy`,`logChangedOn`,`logCreatedBy`,`logCreatedOn`,`logSaveTimes`,`name`,`province_id`) VALUES 
 (452,'R452',NULL,'2015-04-16 16:19:10','',NULL,1,'Buru',31),
 (453,'R453',NULL,'2015-04-16 16:19:10','',NULL,1,'Buru Selatan',31),
 (454,'R454',NULL,'2015-04-16 16:19:10','',NULL,1,'Kepulauan Aru',31),
 (455,'R455',NULL,'2015-04-16 16:19:10','',NULL,1,'Maluku Barat Daya',31),
 (456,'R456',NULL,'2015-04-16 16:19:10','',NULL,1,'Maluku Tengah',31),
 (457,'R457',NULL,'2015-04-16 16:19:10','',NULL,1,'Maluku Tenggara',31),
 (458,'R458',NULL,'2015-04-16 16:19:10','',NULL,1,'Maluku Tenggara Barat',31),
 (459,'R459',NULL,'2015-04-16 16:19:10','',NULL,1,'Seram Bagian Barat',31),
 (460,'R460',NULL,'2015-04-16 16:19:10','',NULL,1,'Seram Bagian Timur',31),
 (461,'R461',NULL,'2015-04-16 16:19:10','',NULL,1,'Ambon',31),
 (462,'R462',NULL,'2015-04-16 16:19:10','',NULL,1,'Tual',31),
 (463,'R463',NULL,'2015-04-16 16:19:10','',NULL,1,'Halmahera Barat',32),
 (464,'R464',NULL,'2015-04-16 16:19:10','',NULL,1,'Halmahera Tengah',32),
 (465,'R465',NULL,'2015-04-16 16:19:10','',NULL,1,'Halmahera Utara',32);
INSERT INTO `util_regencies` (`id`,`code`,`logChangedBy`,`logChangedOn`,`logCreatedBy`,`logCreatedOn`,`logSaveTimes`,`name`,`province_id`) VALUES 
 (466,'R466',NULL,'2015-04-16 16:19:10','',NULL,1,'Halmahera Selatan',32),
 (467,'R467',NULL,'2015-04-16 16:19:10','',NULL,1,'Kepulauan Sula',32),
 (468,'R468',NULL,'2015-04-16 16:19:10','',NULL,1,'Halmahera Timur',32),
 (469,'R469',NULL,'2015-04-16 16:19:10','',NULL,1,'Pulau Morotai',32),
 (470,'R470',NULL,'2015-04-16 16:19:10','',NULL,1,'Pulau Taliabu',32),
 (471,'R471',NULL,'2015-04-16 16:19:10','',NULL,1,'Ternate',32),
 (472,'R472',NULL,'2015-04-16 16:19:10','',NULL,1,'Tidore Kepulauan',32),
 (473,'R473',NULL,'2015-04-16 16:19:10','',NULL,1,'Asmat',33),
 (474,'R474',NULL,'2015-04-16 16:19:10','',NULL,1,'Biak Numfor',33),
 (475,'R475',NULL,'2015-04-16 16:19:10','',NULL,1,'Boven Digoel',33),
 (476,'R476',NULL,'2015-04-16 16:19:10','',NULL,1,'Deiyai',33),
 (477,'R477',NULL,'2015-04-16 16:19:10','',NULL,1,'Dogiyai',33),
 (478,'R478',NULL,'2015-04-16 16:19:10','',NULL,1,'Intan Jaya',33),
 (479,'R479',NULL,'2015-04-16 16:19:10','',NULL,1,'Jayapura',33);
INSERT INTO `util_regencies` (`id`,`code`,`logChangedBy`,`logChangedOn`,`logCreatedBy`,`logCreatedOn`,`logSaveTimes`,`name`,`province_id`) VALUES 
 (480,'R480',NULL,'2015-04-16 16:19:10','',NULL,1,'Jayawijaya',33),
 (481,'R481',NULL,'2015-04-16 16:19:10','',NULL,1,'Keerom',33),
 (482,'R482',NULL,'2015-04-16 16:19:10','',NULL,1,'Kepulauan Yapen',33),
 (483,'R483',NULL,'2015-04-16 16:19:10','',NULL,1,'Lanny Jaya',33),
 (484,'R484',NULL,'2015-04-16 16:19:10','',NULL,1,'Mamberamo Raya',33),
 (485,'R485',NULL,'2015-04-16 16:19:10','',NULL,1,'Mamberamo Tengah',33),
 (486,'R486',NULL,'2015-04-16 16:19:10','',NULL,1,'Mappi',33),
 (487,'R487',NULL,'2015-04-16 16:19:10','',NULL,1,'Merauke',33),
 (488,'R488',NULL,'2015-04-16 16:19:10','',NULL,1,'Mimika',33),
 (489,'R489',NULL,'2015-04-16 16:19:10','',NULL,1,'Nabire',33),
 (490,'R490',NULL,'2015-04-16 16:19:10','',NULL,1,'Nduga',33),
 (491,'R491',NULL,'2015-04-16 16:19:10','',NULL,1,'Paniai',33),
 (492,'R492',NULL,'2015-04-16 16:19:10','',NULL,1,'Pegunungan Bintang',33),
 (493,'R493',NULL,'2015-04-16 16:19:10','',NULL,1,'Puncak',33);
INSERT INTO `util_regencies` (`id`,`code`,`logChangedBy`,`logChangedOn`,`logCreatedBy`,`logCreatedOn`,`logSaveTimes`,`name`,`province_id`) VALUES 
 (494,'R494',NULL,'2015-04-16 16:19:10','',NULL,1,'Puncak Jaya',33),
 (495,'R495',NULL,'2015-04-16 16:19:10','',NULL,1,'Sarmi',33),
 (496,'R496',NULL,'2015-04-16 16:19:10','',NULL,1,'Supiori',33),
 (497,'R497',NULL,'2015-04-16 16:19:10','',NULL,1,'Tolikara',33),
 (498,'R498',NULL,'2015-04-16 16:19:10','',NULL,1,'Waropen',33),
 (499,'R499',NULL,'2015-04-16 16:19:10','',NULL,1,'Yahukimo',33),
 (500,'R500',NULL,'2015-04-16 16:19:10','',NULL,1,'Yalimo',33),
 (501,'R501',NULL,'2015-04-16 16:19:10','',NULL,1,'Jayapura',33),
 (502,'R502',NULL,'2015-04-16 16:19:10','',NULL,1,'Fakfak',34),
 (503,'R503',NULL,'2015-04-16 16:19:10','',NULL,1,'Kaimana',34),
 (504,'R504',NULL,'2015-04-16 16:19:10','',NULL,1,'Manokwari',34),
 (505,'R505',NULL,'2015-04-16 16:19:10','',NULL,1,'Manokwari Selatan',34),
 (506,'R506',NULL,'2015-04-16 16:19:10','',NULL,1,'Maybrat',34),
 (507,'R507',NULL,'2015-04-16 16:19:10','',NULL,1,'Pegunungan Arfak',34);
INSERT INTO `util_regencies` (`id`,`code`,`logChangedBy`,`logChangedOn`,`logCreatedBy`,`logCreatedOn`,`logSaveTimes`,`name`,`province_id`) VALUES 
 (508,'R508',NULL,'2015-04-16 16:19:10','',NULL,1,'Raja Ampat',34),
 (509,'R509',NULL,'2015-04-16 16:19:10','',NULL,1,'Sorong',34),
 (510,'R510',NULL,'2015-04-16 16:19:10','',NULL,1,'Sorong Selatan',34),
 (511,'R511',NULL,'2015-04-16 16:19:10','',NULL,1,'Tambrauw',34),
 (512,'R512',NULL,'2015-04-16 16:19:10','',NULL,1,'Teluk Bintuni',34),
 (513,'R513',NULL,'2015-04-16 16:19:10','',NULL,1,'Teluk Wondama',34),
 (514,'R514',NULL,'2015-04-16 16:19:10','',NULL,1,'Sorong',34);
/*!40000 ALTER TABLE `util_regencies` ENABLE KEYS */;


--
-- Table structure for table `e_emergency`.`util_schedulers`
--

DROP TABLE IF EXISTS `util_schedulers`;
CREATE TABLE `util_schedulers` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `active` bit(1) DEFAULT NULL,
  `appl` varchar(32) DEFAULT NULL,
  `code` varchar(8) DEFAULT NULL,
  `cronSchedule` varchar(32) DEFAULT NULL,
  `jobClassName` varchar(255) DEFAULT NULL,
  `lastCheckpointStatus` varchar(32) DEFAULT NULL,
  `lastCheckpointTime` datetime DEFAULT NULL,
  `logChangedBy` varchar(32) DEFAULT NULL,
  `logChangedOn` datetime DEFAULT NULL,
  `logCreatedBy` varchar(32) DEFAULT NULL,
  `logCreatedOn` datetime DEFAULT NULL,
  `logSaveTimes` int(11) DEFAULT NULL,
  `name` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `e_emergency`.`util_schedulers`
--

/*!40000 ALTER TABLE `util_schedulers` DISABLE KEYS */;
/*!40000 ALTER TABLE `util_schedulers` ENABLE KEYS */;


--
-- Table structure for table `e_emergency`.`util_user_childs`
--

DROP TABLE IF EXISTS `util_user_childs`;
CREATE TABLE `util_user_childs` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `appl` varchar(32) DEFAULT NULL,
  `associativeArray` varchar(255) DEFAULT NULL,
  `className` varchar(255) DEFAULT NULL,
  `logChangedBy` varchar(32) DEFAULT NULL,
  `logChangedOn` datetime DEFAULT NULL,
  `logCreatedBy` varchar(32) DEFAULT NULL,
  `logCreatedOn` datetime DEFAULT NULL,
  `logSaveTimes` int(11) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKC855510E60CAE997` (`user_id`),
  CONSTRAINT `FKC855510E60CAE997` FOREIGN KEY (`user_id`) REFERENCES `util_users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `e_emergency`.`util_user_childs`
--

/*!40000 ALTER TABLE `util_user_childs` DISABLE KEYS */;
/*!40000 ALTER TABLE `util_user_childs` ENABLE KEYS */;


--
-- Table structure for table `e_emergency`.`util_user_group_childs`
--

DROP TABLE IF EXISTS `util_user_group_childs`;
CREATE TABLE `util_user_group_childs` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `appl` varchar(32) DEFAULT NULL,
  `associativeArray` varchar(255) DEFAULT NULL,
  `className` varchar(255) DEFAULT NULL,
  `logChangedBy` varchar(32) DEFAULT NULL,
  `logChangedOn` datetime DEFAULT NULL,
  `logCreatedBy` varchar(32) DEFAULT NULL,
  `logCreatedOn` datetime DEFAULT NULL,
  `logSaveTimes` int(11) DEFAULT NULL,
  `userGroup_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK7D2E158E60F4677D` (`userGroup_id`),
  CONSTRAINT `FK7D2E158E60F4677D` FOREIGN KEY (`userGroup_id`) REFERENCES `util_user_groups` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `e_emergency`.`util_user_group_childs`
--

/*!40000 ALTER TABLE `util_user_group_childs` DISABLE KEYS */;
/*!40000 ALTER TABLE `util_user_group_childs` ENABLE KEYS */;


--
-- Table structure for table `e_emergency`.`util_user_group_ips`
--

DROP TABLE IF EXISTS `util_user_group_ips`;
CREATE TABLE `util_user_group_ips` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `appl` varchar(32) DEFAULT NULL,
  `ip` varchar(40) DEFAULT NULL,
  `logChangedBy` varchar(32) DEFAULT NULL,
  `logChangedOn` datetime DEFAULT NULL,
  `logCreatedBy` varchar(32) DEFAULT NULL,
  `logCreatedOn` datetime DEFAULT NULL,
  `logSaveTimes` int(11) DEFAULT NULL,
  `userGroup_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKADA384D560F4677D` (`userGroup_id`),
  CONSTRAINT `FKADA384D560F4677D` FOREIGN KEY (`userGroup_id`) REFERENCES `util_user_groups` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `e_emergency`.`util_user_group_ips`
--

/*!40000 ALTER TABLE `util_user_group_ips` DISABLE KEYS */;
/*!40000 ALTER TABLE `util_user_group_ips` ENABLE KEYS */;


--
-- Table structure for table `e_emergency`.`util_user_group_privileges`
--

DROP TABLE IF EXISTS `util_user_group_privileges`;
CREATE TABLE `util_user_group_privileges` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `appl` varchar(32) DEFAULT NULL,
  `logChangedBy` varchar(32) DEFAULT NULL,
  `logChangedOn` datetime DEFAULT NULL,
  `logCreatedBy` varchar(32) DEFAULT NULL,
  `logCreatedOn` datetime DEFAULT NULL,
  `logSaveTimes` int(11) DEFAULT NULL,
  `privilege_id` bigint(20) DEFAULT NULL,
  `userGroup_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKB1A71319D762667D` (`privilege_id`),
  KEY `FKB1A7131960F4677D` (`userGroup_id`),
  CONSTRAINT `FKB1A7131960F4677D` FOREIGN KEY (`userGroup_id`) REFERENCES `util_user_groups` (`id`),
  CONSTRAINT `FKB1A71319D762667D` FOREIGN KEY (`privilege_id`) REFERENCES `util_privileges` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=69 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `e_emergency`.`util_user_group_privileges`
--

/*!40000 ALTER TABLE `util_user_group_privileges` DISABLE KEYS */;
INSERT INTO `util_user_group_privileges` (`id`,`appl`,`logChangedBy`,`logChangedOn`,`logCreatedBy`,`logCreatedOn`,`logSaveTimes`,`privilege_id`,`userGroup_id`) VALUES 
 (1,'e-emergency-zk',NULL,NULL,NULL,'2015-04-16 16:19:11',0,1,1),
 (2,'e-emergency-zk',NULL,NULL,NULL,'2015-04-16 16:19:11',0,2,1),
 (3,'e-emergency-zk',NULL,NULL,NULL,'2015-04-16 16:19:11',0,3,1),
 (4,'e-emergency-zk',NULL,NULL,NULL,'2015-04-16 16:19:11',0,4,1),
 (5,'e-emergency-zk',NULL,NULL,NULL,'2015-04-16 16:19:11',0,5,1),
 (6,'e-emergency-zk',NULL,NULL,NULL,'2015-04-16 16:19:11',0,6,1),
 (7,'e-emergency-zk',NULL,NULL,NULL,'2015-04-16 16:19:11',0,7,1),
 (8,'e-emergency-zk',NULL,NULL,NULL,'2015-04-16 16:19:11',0,8,1),
 (9,'e-emergency-zk',NULL,NULL,NULL,'2015-04-16 16:19:11',0,9,1),
 (10,'e-emergency-zk',NULL,NULL,NULL,'2015-04-16 16:19:11',0,10,1),
 (11,'e-emergency-zk',NULL,NULL,NULL,'2015-04-16 16:19:11',0,11,1),
 (12,'e-emergency-zk',NULL,NULL,NULL,'2015-04-16 16:19:11',0,12,1),
 (13,'e-emergency-zk',NULL,NULL,NULL,'2015-04-16 16:19:11',0,13,1),
 (14,'e-emergency-zk',NULL,NULL,NULL,'2015-04-16 16:19:11',0,14,1);
INSERT INTO `util_user_group_privileges` (`id`,`appl`,`logChangedBy`,`logChangedOn`,`logCreatedBy`,`logCreatedOn`,`logSaveTimes`,`privilege_id`,`userGroup_id`) VALUES 
 (15,'e-emergency-zk',NULL,NULL,NULL,'2015-04-16 16:19:11',0,15,1),
 (16,'e-emergency-zk',NULL,NULL,NULL,'2015-04-16 16:19:11',0,16,1),
 (17,'e-emergency-zk',NULL,NULL,NULL,'2015-04-16 16:19:11',0,17,1),
 (18,'e-emergency-zk',NULL,NULL,NULL,'2015-04-16 16:19:11',0,18,1),
 (19,'e-emergency-zk',NULL,NULL,NULL,'2015-04-16 16:19:11',0,19,1),
 (20,'e-emergency-zk',NULL,NULL,NULL,'2015-04-16 16:19:11',0,20,1),
 (21,'e-emergency-zk',NULL,NULL,NULL,'2015-04-16 16:19:11',0,21,1),
 (22,'e-emergency-zk',NULL,NULL,NULL,'2015-04-16 16:19:11',0,22,1),
 (23,'e-emergency-zk',NULL,NULL,NULL,'2015-04-16 16:19:11',0,23,1),
 (24,'e-emergency-zk',NULL,NULL,NULL,'2015-04-16 16:19:11',0,24,1),
 (25,'e-emergency-zk',NULL,NULL,NULL,'2015-04-16 16:19:11',0,25,1),
 (26,'e-emergency-zk',NULL,NULL,NULL,'2015-04-16 16:19:11',0,26,1),
 (27,'e-emergency-zk',NULL,NULL,NULL,'2015-04-16 16:19:11',0,27,1),
 (28,'e-emergency-zk',NULL,NULL,NULL,'2015-04-16 16:19:11',0,28,1);
INSERT INTO `util_user_group_privileges` (`id`,`appl`,`logChangedBy`,`logChangedOn`,`logCreatedBy`,`logCreatedOn`,`logSaveTimes`,`privilege_id`,`userGroup_id`) VALUES 
 (29,'e-emergency-zk',NULL,NULL,NULL,'2015-04-16 16:19:11',0,29,1),
 (30,'e-emergency-zk',NULL,NULL,NULL,'2015-04-16 16:19:11',0,30,1),
 (31,'e-emergency-zk',NULL,NULL,NULL,'2015-04-16 16:19:11',0,31,1),
 (32,'e-emergency-zk',NULL,NULL,NULL,'2015-04-16 16:19:11',0,32,1),
 (33,'e-emergency-zk',NULL,NULL,NULL,'2015-04-16 16:19:11',0,33,1),
 (34,'e-emergency-zk',NULL,NULL,NULL,'2015-04-16 16:19:11',0,34,1),
 (35,'e-emergency-zk',NULL,NULL,NULL,'2015-04-16 16:19:12',0,1,2),
 (36,'e-emergency-zk',NULL,NULL,NULL,'2015-04-16 16:19:12',0,2,2),
 (37,'e-emergency-zk',NULL,NULL,NULL,'2015-04-16 16:19:12',0,3,2),
 (38,'e-emergency-zk',NULL,NULL,NULL,'2015-04-16 16:19:12',0,4,2),
 (39,'e-emergency-zk',NULL,NULL,NULL,'2015-04-16 16:19:12',0,5,2),
 (40,'e-emergency-zk',NULL,NULL,NULL,'2015-04-16 16:19:12',0,6,2),
 (41,'e-emergency-zk',NULL,NULL,NULL,'2015-04-16 16:19:12',0,7,2),
 (42,'e-emergency-zk',NULL,NULL,NULL,'2015-04-16 16:19:12',0,8,2);
INSERT INTO `util_user_group_privileges` (`id`,`appl`,`logChangedBy`,`logChangedOn`,`logCreatedBy`,`logCreatedOn`,`logSaveTimes`,`privilege_id`,`userGroup_id`) VALUES 
 (43,'e-emergency-zk',NULL,NULL,NULL,'2015-04-16 16:19:12',0,9,2),
 (44,'e-emergency-zk',NULL,NULL,NULL,'2015-04-16 16:19:12',0,10,2),
 (45,'e-emergency-zk',NULL,NULL,NULL,'2015-04-16 16:19:12',0,11,2),
 (46,'e-emergency-zk',NULL,NULL,NULL,'2015-04-16 16:19:12',0,12,2),
 (47,'e-emergency-zk',NULL,NULL,NULL,'2015-04-16 16:19:12',0,13,2),
 (48,'e-emergency-zk',NULL,NULL,NULL,'2015-04-16 16:19:12',0,14,2),
 (49,'e-emergency-zk',NULL,NULL,NULL,'2015-04-16 16:19:12',0,15,2),
 (50,'e-emergency-zk',NULL,NULL,NULL,'2015-04-16 16:19:12',0,16,2),
 (51,'e-emergency-zk',NULL,NULL,NULL,'2015-04-16 16:19:12',0,17,2),
 (52,'e-emergency-zk',NULL,NULL,NULL,'2015-04-16 16:19:12',0,18,2),
 (53,'e-emergency-zk',NULL,NULL,NULL,'2015-04-16 16:19:12',0,19,2),
 (54,'e-emergency-zk',NULL,NULL,NULL,'2015-04-16 16:19:12',0,20,2),
 (55,'e-emergency-zk',NULL,NULL,NULL,'2015-04-16 16:19:12',0,21,2),
 (56,'e-emergency-zk',NULL,NULL,NULL,'2015-04-16 16:19:12',0,22,2);
INSERT INTO `util_user_group_privileges` (`id`,`appl`,`logChangedBy`,`logChangedOn`,`logCreatedBy`,`logCreatedOn`,`logSaveTimes`,`privilege_id`,`userGroup_id`) VALUES 
 (57,'e-emergency-zk',NULL,NULL,NULL,'2015-04-16 16:19:12',0,23,2),
 (58,'e-emergency-zk',NULL,NULL,NULL,'2015-04-16 16:19:12',0,24,2),
 (59,'e-emergency-zk',NULL,NULL,NULL,'2015-04-16 16:19:12',0,25,2),
 (60,'e-emergency-zk',NULL,NULL,NULL,'2015-04-16 16:19:12',0,26,2),
 (61,'e-emergency-zk',NULL,NULL,NULL,'2015-04-16 16:19:12',0,27,2),
 (62,'e-emergency-zk',NULL,NULL,NULL,'2015-04-16 16:19:12',0,28,2),
 (63,'e-emergency-zk',NULL,NULL,NULL,'2015-04-16 16:19:12',0,29,2),
 (64,'e-emergency-zk',NULL,NULL,NULL,'2015-04-16 16:19:12',0,30,2),
 (65,'e-emergency-zk',NULL,NULL,NULL,'2015-04-16 16:19:12',0,31,2),
 (66,'e-emergency-zk',NULL,NULL,NULL,'2015-04-16 16:19:12',0,32,2),
 (67,'e-emergency-zk',NULL,NULL,NULL,'2015-04-16 16:19:12',0,33,2),
 (68,'e-emergency-zk',NULL,NULL,NULL,'2015-04-16 16:19:12',0,34,2);
/*!40000 ALTER TABLE `util_user_group_privileges` ENABLE KEYS */;


--
-- Table structure for table `e_emergency`.`util_user_groups`
--

DROP TABLE IF EXISTS `util_user_groups`;
CREATE TABLE `util_user_groups` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `appl` varchar(32) DEFAULT NULL,
  `code` varchar(8) DEFAULT NULL,
  `logChangedBy` varchar(32) DEFAULT NULL,
  `logChangedOn` datetime DEFAULT NULL,
  `logCreatedBy` varchar(32) DEFAULT NULL,
  `logCreatedOn` datetime DEFAULT NULL,
  `logSaveTimes` int(11) DEFAULT NULL,
  `name` varchar(32) DEFAULT NULL,
  `root` bit(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `e_emergency`.`util_user_groups`
--

/*!40000 ALTER TABLE `util_user_groups` DISABLE KEYS */;
INSERT INTO `util_user_groups` (`id`,`appl`,`code`,`logChangedBy`,`logChangedOn`,`logCreatedBy`,`logCreatedOn`,`logSaveTimes`,`name`,`root`) VALUES 
 (1,'e-emergency-zk','root',NULL,NULL,NULL,'2015-04-16 16:19:11',0,'root',''),
 (2,'e-emergency-zk','admin',NULL,NULL,NULL,'2015-04-16 16:19:12',0,'admin','\0');
/*!40000 ALTER TABLE `util_user_groups` ENABLE KEYS */;


--
-- Table structure for table `e_emergency`.`util_user_ips`
--

DROP TABLE IF EXISTS `util_user_ips`;
CREATE TABLE `util_user_ips` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `appl` varchar(32) DEFAULT NULL,
  `ip` varchar(40) DEFAULT NULL,
  `logChangedBy` varchar(32) DEFAULT NULL,
  `logChangedOn` datetime DEFAULT NULL,
  `logCreatedBy` varchar(32) DEFAULT NULL,
  `logCreatedOn` datetime DEFAULT NULL,
  `logSaveTimes` int(11) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK94B1F95560CAE997` (`user_id`),
  CONSTRAINT `FK94B1F95560CAE997` FOREIGN KEY (`user_id`) REFERENCES `util_users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `e_emergency`.`util_user_ips`
--

/*!40000 ALTER TABLE `util_user_ips` DISABLE KEYS */;
/*!40000 ALTER TABLE `util_user_ips` ENABLE KEYS */;


--
-- Table structure for table `e_emergency`.`util_user_privileges`
--

DROP TABLE IF EXISTS `util_user_privileges`;
CREATE TABLE `util_user_privileges` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `appl` varchar(32) DEFAULT NULL,
  `logChangedBy` varchar(32) DEFAULT NULL,
  `logChangedOn` datetime DEFAULT NULL,
  `logCreatedBy` varchar(32) DEFAULT NULL,
  `logCreatedOn` datetime DEFAULT NULL,
  `logSaveTimes` int(11) DEFAULT NULL,
  `privilege_id` bigint(20) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK57C48E99D762667D` (`privilege_id`),
  KEY `FK57C48E9960CAE997` (`user_id`),
  CONSTRAINT `FK57C48E9960CAE997` FOREIGN KEY (`user_id`) REFERENCES `util_users` (`id`),
  CONSTRAINT `FK57C48E99D762667D` FOREIGN KEY (`privilege_id`) REFERENCES `util_privileges` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=69 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `e_emergency`.`util_user_privileges`
--

/*!40000 ALTER TABLE `util_user_privileges` DISABLE KEYS */;
INSERT INTO `util_user_privileges` (`id`,`appl`,`logChangedBy`,`logChangedOn`,`logCreatedBy`,`logCreatedOn`,`logSaveTimes`,`privilege_id`,`user_id`) VALUES 
 (1,'e-emergency-zk',NULL,NULL,NULL,'2015-04-16 16:19:11',0,1,1),
 (2,'e-emergency-zk',NULL,NULL,NULL,'2015-04-16 16:19:11',0,2,1),
 (3,'e-emergency-zk',NULL,NULL,NULL,'2015-04-16 16:19:11',0,3,1),
 (4,'e-emergency-zk',NULL,NULL,NULL,'2015-04-16 16:19:11',0,4,1),
 (5,'e-emergency-zk',NULL,NULL,NULL,'2015-04-16 16:19:11',0,5,1),
 (6,'e-emergency-zk',NULL,NULL,NULL,'2015-04-16 16:19:11',0,6,1),
 (7,'e-emergency-zk',NULL,NULL,NULL,'2015-04-16 16:19:11',0,7,1),
 (8,'e-emergency-zk',NULL,NULL,NULL,'2015-04-16 16:19:11',0,8,1),
 (9,'e-emergency-zk',NULL,NULL,NULL,'2015-04-16 16:19:11',0,9,1),
 (10,'e-emergency-zk',NULL,NULL,NULL,'2015-04-16 16:19:11',0,10,1),
 (11,'e-emergency-zk',NULL,NULL,NULL,'2015-04-16 16:19:11',0,11,1),
 (12,'e-emergency-zk',NULL,NULL,NULL,'2015-04-16 16:19:11',0,12,1),
 (13,'e-emergency-zk',NULL,NULL,NULL,'2015-04-16 16:19:11',0,13,1),
 (14,'e-emergency-zk',NULL,NULL,NULL,'2015-04-16 16:19:11',0,14,1);
INSERT INTO `util_user_privileges` (`id`,`appl`,`logChangedBy`,`logChangedOn`,`logCreatedBy`,`logCreatedOn`,`logSaveTimes`,`privilege_id`,`user_id`) VALUES 
 (15,'e-emergency-zk',NULL,NULL,NULL,'2015-04-16 16:19:11',0,15,1),
 (16,'e-emergency-zk',NULL,NULL,NULL,'2015-04-16 16:19:11',0,16,1),
 (17,'e-emergency-zk',NULL,NULL,NULL,'2015-04-16 16:19:11',0,17,1),
 (18,'e-emergency-zk',NULL,NULL,NULL,'2015-04-16 16:19:11',0,18,1),
 (19,'e-emergency-zk',NULL,NULL,NULL,'2015-04-16 16:19:11',0,19,1),
 (20,'e-emergency-zk',NULL,NULL,NULL,'2015-04-16 16:19:11',0,20,1),
 (21,'e-emergency-zk',NULL,NULL,NULL,'2015-04-16 16:19:11',0,21,1),
 (22,'e-emergency-zk',NULL,NULL,NULL,'2015-04-16 16:19:11',0,22,1),
 (23,'e-emergency-zk',NULL,NULL,NULL,'2015-04-16 16:19:11',0,23,1),
 (24,'e-emergency-zk',NULL,NULL,NULL,'2015-04-16 16:19:12',0,24,1),
 (25,'e-emergency-zk',NULL,NULL,NULL,'2015-04-16 16:19:12',0,25,1),
 (26,'e-emergency-zk',NULL,NULL,NULL,'2015-04-16 16:19:12',0,26,1),
 (27,'e-emergency-zk',NULL,NULL,NULL,'2015-04-16 16:19:12',0,27,1),
 (28,'e-emergency-zk',NULL,NULL,NULL,'2015-04-16 16:19:12',0,28,1);
INSERT INTO `util_user_privileges` (`id`,`appl`,`logChangedBy`,`logChangedOn`,`logCreatedBy`,`logCreatedOn`,`logSaveTimes`,`privilege_id`,`user_id`) VALUES 
 (29,'e-emergency-zk',NULL,NULL,NULL,'2015-04-16 16:19:12',0,29,1),
 (30,'e-emergency-zk',NULL,NULL,NULL,'2015-04-16 16:19:12',0,30,1),
 (31,'e-emergency-zk',NULL,NULL,NULL,'2015-04-16 16:19:12',0,31,1),
 (32,'e-emergency-zk',NULL,NULL,NULL,'2015-04-16 16:19:12',0,32,1),
 (33,'e-emergency-zk',NULL,NULL,NULL,'2015-04-16 16:19:12',0,33,1),
 (34,'e-emergency-zk',NULL,NULL,NULL,'2015-04-16 16:19:12',0,34,1),
 (35,'e-emergency-zk',NULL,NULL,NULL,'2015-04-16 16:19:12',0,1,2),
 (36,'e-emergency-zk',NULL,NULL,NULL,'2015-04-16 16:19:12',0,2,2),
 (37,'e-emergency-zk',NULL,NULL,NULL,'2015-04-16 16:19:12',0,3,2),
 (38,'e-emergency-zk',NULL,NULL,NULL,'2015-04-16 16:19:12',0,4,2),
 (39,'e-emergency-zk',NULL,NULL,NULL,'2015-04-16 16:19:12',0,5,2),
 (40,'e-emergency-zk',NULL,NULL,NULL,'2015-04-16 16:19:12',0,6,2),
 (41,'e-emergency-zk',NULL,NULL,NULL,'2015-04-16 16:19:12',0,7,2),
 (42,'e-emergency-zk',NULL,NULL,NULL,'2015-04-16 16:19:12',0,8,2);
INSERT INTO `util_user_privileges` (`id`,`appl`,`logChangedBy`,`logChangedOn`,`logCreatedBy`,`logCreatedOn`,`logSaveTimes`,`privilege_id`,`user_id`) VALUES 
 (43,'e-emergency-zk',NULL,NULL,NULL,'2015-04-16 16:19:12',0,9,2),
 (44,'e-emergency-zk',NULL,NULL,NULL,'2015-04-16 16:19:12',0,10,2),
 (45,'e-emergency-zk',NULL,NULL,NULL,'2015-04-16 16:19:12',0,11,2),
 (46,'e-emergency-zk',NULL,NULL,NULL,'2015-04-16 16:19:12',0,12,2),
 (47,'e-emergency-zk',NULL,NULL,NULL,'2015-04-16 16:19:12',0,13,2),
 (48,'e-emergency-zk',NULL,NULL,NULL,'2015-04-16 16:19:12',0,14,2),
 (49,'e-emergency-zk',NULL,NULL,NULL,'2015-04-16 16:19:12',0,15,2),
 (50,'e-emergency-zk',NULL,NULL,NULL,'2015-04-16 16:19:12',0,16,2),
 (51,'e-emergency-zk',NULL,NULL,NULL,'2015-04-16 16:19:12',0,17,2),
 (52,'e-emergency-zk',NULL,NULL,NULL,'2015-04-16 16:19:12',0,18,2),
 (53,'e-emergency-zk',NULL,NULL,NULL,'2015-04-16 16:19:12',0,19,2),
 (54,'e-emergency-zk',NULL,NULL,NULL,'2015-04-16 16:19:12',0,20,2),
 (55,'e-emergency-zk',NULL,NULL,NULL,'2015-04-16 16:19:12',0,21,2),
 (56,'e-emergency-zk',NULL,NULL,NULL,'2015-04-16 16:19:12',0,22,2);
INSERT INTO `util_user_privileges` (`id`,`appl`,`logChangedBy`,`logChangedOn`,`logCreatedBy`,`logCreatedOn`,`logSaveTimes`,`privilege_id`,`user_id`) VALUES 
 (57,'e-emergency-zk',NULL,NULL,NULL,'2015-04-16 16:19:12',0,23,2),
 (58,'e-emergency-zk',NULL,NULL,NULL,'2015-04-16 16:19:12',0,24,2),
 (59,'e-emergency-zk',NULL,NULL,NULL,'2015-04-16 16:19:12',0,25,2),
 (60,'e-emergency-zk',NULL,NULL,NULL,'2015-04-16 16:19:12',0,26,2),
 (61,'e-emergency-zk',NULL,NULL,NULL,'2015-04-16 16:19:12',0,27,2),
 (62,'e-emergency-zk',NULL,NULL,NULL,'2015-04-16 16:19:12',0,28,2),
 (63,'e-emergency-zk',NULL,NULL,NULL,'2015-04-16 16:19:12',0,29,2),
 (64,'e-emergency-zk',NULL,NULL,NULL,'2015-04-16 16:19:12',0,30,2),
 (65,'e-emergency-zk',NULL,NULL,NULL,'2015-04-16 16:19:12',0,31,2),
 (66,'e-emergency-zk',NULL,NULL,NULL,'2015-04-16 16:19:12',0,32,2),
 (67,'e-emergency-zk',NULL,NULL,NULL,'2015-04-16 16:19:12',0,33,2),
 (68,'e-emergency-zk',NULL,NULL,NULL,'2015-04-16 16:19:12',0,34,2);
/*!40000 ALTER TABLE `util_user_privileges` ENABLE KEYS */;


--
-- Table structure for table `e_emergency`.`util_users`
--

DROP TABLE IF EXISTS `util_users`;
CREATE TABLE `util_users` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `active` bit(1) DEFAULT NULL,
  `appl` varchar(32) DEFAULT NULL,
  `fullname` varchar(255) DEFAULT NULL,
  `logChangedBy` varchar(32) DEFAULT NULL,
  `logChangedOn` datetime DEFAULT NULL,
  `logCreatedBy` varchar(32) DEFAULT NULL,
  `logCreatedOn` datetime DEFAULT NULL,
  `logSaveTimes` int(11) DEFAULT NULL,
  `password` varchar(32) DEFAULT NULL,
  `username` varchar(32) DEFAULT NULL,
  `userGroup_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK69D2578B60F4677D` (`userGroup_id`),
  CONSTRAINT `FK69D2578B60F4677D` FOREIGN KEY (`userGroup_id`) REFERENCES `util_user_groups` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `e_emergency`.`util_users`
--

/*!40000 ALTER TABLE `util_users` DISABLE KEYS */;
INSERT INTO `util_users` (`id`,`active`,`appl`,`fullname`,`logChangedBy`,`logChangedOn`,`logCreatedBy`,`logCreatedOn`,`logSaveTimes`,`password`,`username`,`userGroup_id`) VALUES 
 (1,'','e-emergency-zk','root',NULL,NULL,NULL,'2015-04-16 16:19:11',0,'63a9f0ea7bb98050796b649e85481845','root',1),
 (2,'','e-emergency-zk','admin',NULL,NULL,NULL,'2015-04-16 16:19:12',0,'21232f297a57a5a743894a0e4a801fc3','admin',2);
/*!40000 ALTER TABLE `util_users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
