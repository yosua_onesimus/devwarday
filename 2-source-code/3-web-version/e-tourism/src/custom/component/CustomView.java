package custom.component;

import static cococare.common.CCClass.getValue;
import static cococare.common.CCFormat.getString4View;
import static cococare.common.CCFormat.mailto;
import static cococare.common.CCFormat.maxLength;
import static cococare.common.CCFormat.parseInt;
import static cococare.common.CCLogic.coalesce;
import static cococare.common.CCMath.calculate;
import static cococare.zk.CCSession.getAttribute;
import static cococare.zk.CCZk.addListener;
import static cococare.zk.CCZk.setDimension;
import static cococare.zk.CCZk.setHeight;
import static cococare.zk.CCZk.setHflex;
import static cococare.zk.CCZk.setImageContent;
import static cococare.zk.CCZk.setSclass;
import static cococare.zk.CCZk.setStyle;
import static cococare.zk.CCZk.setWidth;
import model.bo.tourism.EtConfigBo;
import model.obj.tourism.EtConfig;
import model.obj.tourism.EtSearch;
import model.obj.tourism.EtTourismSite;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zul.A;
import org.zkoss.zul.Div;
import org.zkoss.zul.Hbox;
import org.zkoss.zul.Image;
import org.zkoss.zul.Label;
import org.zkoss.zul.Space;
import org.zkoss.zul.Vbox;

import cococare.zk.CCRating;
import controller.zul.preLogin.ZulTourismSiteProfileCtrl;

public class CustomView {
	public static String getString(Object object, String fieldName) {
		return coalesce(getString4View(getValue(object, fieldName)), "~");
	}

	public static Space newSpaceH5() {
		Space space = new Space();
		setHeight(space, 5);
		return space;
	}

	public static Space newSpaceW5() {
		Space space = new Space();
		setWidth(space, 5);
		return space;
	}

	public static A newAEmail(String label) {
		A a = new A(label);
		a.setHref(mailto(label, null, null, null, null));
		return a;
	}

	public static A newAWebsite(String label) {
		A a = new A(label);
		a.setHref("http://" + label.replaceFirst("http://", ""));
		a.setTarget("_blank");
		return a;
	}

	/**
	 * Create a new component from Tourism Site object.
	 * 
	 * @param object
	 *            Tourism Site.
	 * @return the new component.
	 */
	public static Component newComponent(final Object object) {
		Hbox hbox = new Hbox();
		setHflex(hbox, 1);
		Vbox vbox = new Vbox();
		{
			setHflex(vbox, 1);
			setStyle(vbox, "background:#EFFFEF;filter:alpha(opacity=80);opacity:0.8;");
			newSpaceH5().setParent(vbox);
			Hbox hbox2 = new Hbox();
			{
				setHflex(hbox2, 1);
				newSpaceW5().setParent(hbox2);
				Image image = new Image();
				setDimension(image, 100, 100);
				EtConfig config = new EtConfigBo().getConfig();
				setImageContent(image, (byte[]) coalesce(//
						getValue(object, "photo"),// 
						config.getPhotoTourism()));
				image.setParent(hbox2);
				newSpaceW5().setParent(hbox2);
				Vbox vbox2 = new Vbox();
				{
					setHeight(vbox2, 100);
					setHflex(vbox2, 1);
					Hbox hbox3 = new Hbox();
					setHflex(hbox3, 1);
					{
						Label lblName = new Label(getString(object, "name"));
						setStyle(lblName, "font-size:24px;");
						lblName.setParent(hbox3);
						Space space = new Space();
						setHflex(space, 1);
						space.setParent(hbox3);
						CCRating rating = new CCRating(new Hbox());
						rating.setRating(parseInt(calculate("totalRating/totalRater", object)));
						setSclass(rating.getBox(), "starRating");
						rating.getBox().setTooltiptext(getString(object, "@totalRating/@totalRater"));
						rating.getBox().setParent(hbox3);
					}
					hbox3.setParent(vbox2);
					Div div = new Div();
					{
						Label lblReview = new Label(maxLength(getString(object, "review"), 300, " "));
						lblReview.setParent(div);
						A a = new A("More..");
						addListener(a, new EventListener() {
							public void onEvent(Event event) throws Exception {
								EtSearch search = getAttribute(EtSearch.class);
								search.setTourismSite((EtTourismSite) object);
								new ZulTourismSiteProfileCtrl().init();
							}
						});
						a.setParent(div);
					}
					div.setParent(vbox2);
				}
				vbox2.setParent(hbox2);
				newSpaceW5().setParent(hbox2);
			}
			hbox2.setParent(vbox);
			newSpaceH5().setParent(vbox);
		}
		vbox.setParent(hbox);
		return hbox;
	}
}