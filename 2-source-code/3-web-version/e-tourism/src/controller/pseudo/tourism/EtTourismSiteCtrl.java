package controller.pseudo.tourism;

import static cococare.common.CCLogic.isNotNull;
import static cococare.zk.CCZk.addListener;

import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;

import cococare.framework.model.obj.util.UtilProvince;
import cococare.framework.model.obj.util.UtilRegency;
import cococare.framework.model.obj.util.UtilFilter.isProvince;
import cococare.framework.zk.controller.zul.util.ZulParameterCtrl;
import cococare.zk.CCBandbox;

public class EtTourismSiteCtrl extends ZulParameterCtrl {
	private CCBandbox txtRegency;
	private CCBandbox txtProvince;

	@Override
	protected void _initEditor() {
		super._initEditor();
		txtRegency.getTable().setHqlFilters(new isProvince() {
			@Override
			public Object getFieldValue() {
				return txtProvince.getObject();
			}
		});
	}

	@Override
	protected void _initListener() {
		super._initListener();
		addListener(txtRegency.getBandbox(), new EventListener() {
			public void onEvent(Event event) throws Exception {
				_doRegency();
			}
		});
		addListener(txtProvince.getBandbox(), new EventListener() {
			public void onEvent(Event event) throws Exception {
				_doProvince();
			}
		});
	}

	private void _doRegency() {
		UtilRegency regency = txtRegency.getObject();
		if (isNotNull(regency)) {
			txtProvince.setObject(regency.getProvince());
		}
	}

	private void _doProvince() {
		UtilRegency regency = txtRegency.getObject();
		UtilProvince province = txtProvince.getObject();
		if (isNotNull(regency) //
				&& isNotNull(province)// 
				&& !regency.getProvince().getId().equals(province.getId())) {
			txtRegency.setObject(null);
		}
		txtRegency.getTable().search();
	}
}