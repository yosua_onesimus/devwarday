package controller.pseudo.tourism;

import static cococare.framework.model.obj.util.UtilFilter.isUserGroupNotRoot;
import cococare.framework.zk.controller.zul.util.ZulApplicationSettingCtrl;
import cococare.zk.CCBandbox;

public class EtConfigCtrl extends ZulApplicationSettingCtrl {
	private CCBandbox txtGroupTourism;

	@Override
	protected void _initEditor() {
		super._initEditor();
		txtGroupTourism.getTable().setHqlFilters(isUserGroupNotRoot);
	}
}