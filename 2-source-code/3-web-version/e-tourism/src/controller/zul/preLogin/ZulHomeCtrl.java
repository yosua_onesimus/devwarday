package controller.zul.preLogin;

import static cococare.zk.CCSession.getAttribute;
import static cococare.zk.CCZk.addListener;
import static cococare.zk.CCZk.getBandbox;
import model.obj.tourism.EtSearch;
import model.obj.tourism.EtTourismSite;

import org.zkoss.zul.Button;

import cococare.framework.model.obj.util.UtilProvince;
import cococare.framework.model.obj.util.UtilRegency;
import cococare.framework.zk.CFZkCtrl;
import cococare.zk.CCAutoComplete;
import cococare.zk.CCZk;

public class ZulHomeCtrl extends CFZkCtrl {
	private CCAutoComplete bndAutoComplete;
	private Button btnSearch;

	@Override
	protected Class _getEntity() {
		return null;
	}

	@Override
	protected BaseFunction _getBaseFunction() {
		// to enable backward action / forward action
		CCZk.setCurrentDesktopBookmark();
		return BaseFunction.LIST_FUNCTION;
	}

	@Override
	protected void _initComponent() {
		super._initComponent();

		bndAutoComplete = new CCAutoComplete(getBandbox(getContainer(), "bndAutoComplete"));
		bndAutoComplete.setEntity(EtTourismSite.class, UtilRegency.class, UtilProvince.class);
	}

	@Override
	protected void _initListener() {
		super._initListener();

		addListener(btnSearch, elSearch);
	}

	@Override
	protected void _doSearch() {
		EtSearch search = getAttribute(EtSearch.class);
		if (bndAutoComplete.getObject() instanceof UtilProvince) {
			search.setProvince((UtilProvince) bndAutoComplete.getObject());
		} else if (bndAutoComplete.getObject() instanceof UtilRegency) {
			search.setRegency((UtilRegency) bndAutoComplete.getObject());
		} else {
			search.setName(bndAutoComplete.getText());
		}

		new ZulSearchCtrl().init();
	}
}