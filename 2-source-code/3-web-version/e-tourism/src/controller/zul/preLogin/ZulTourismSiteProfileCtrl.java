package controller.zul.preLogin;

import static cococare.common.CCClass.getValue;
import static cococare.common.CCFormat.getString4View;
import static cococare.common.CCFormat.mailto;
import static cococare.common.CCFormat.parseInt;
import static cococare.common.CCLogic.coalesce;
import static cococare.common.CCLogic.isNotNullAndNotEmpty;
import static cococare.common.CCMath.calculate;
import static cococare.zk.CCSession.getAttribute;
import static cococare.zk.CCZk.addListener;
import static cococare.zk.CCZk.initComponent;
import static cococare.zk.CCZk.setImageContent;
import model.bo.tourism.EtConfigBo;
import model.bo.tourism.EtTourismSiteBo;
import model.obj.tourism.EtSearch;
import model.obj.tourism.EtTourismSite;
import model.obj.tourism.EtTourismType;

import org.zkoss.zul.A;
import org.zkoss.zul.Button;
import org.zkoss.zul.Hbox;
import org.zkoss.zul.Image;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Window;

import cococare.framework.zk.CFZkCtrl;
import cococare.zk.CCEditor;
import cococare.zk.CCRating;
import cococare.zk.CCTable;
import cococare.zk.CCZk;

public class ZulTourismSiteProfileCtrl extends CFZkCtrl {
	private EtTourismSiteBo tourismSiteBo;

	private CCEditor edtSearch;
	private CCTable tblTourismType;

	private CCEditor edtTourismSite;

	private Window winSearch;
	private Listbox lstTourismType;
	private Button btnSearch;

	private Window winTourismSite;
	private Image imgPhoto;
	private Hbox boxRating;
	private Label txtPhone;
	private A txtEmail;
	private A txtWebsite;
	private Label txtReview;
	private Label txtHistory;
	private Label txtAdvice;
	private Label txtTransport;

	@Override
	protected Class _getEntity() {
		return EtTourismSite.class;
	}

	@Override
	protected BaseFunction _getBaseFunction() {
		// to enable backward action / forward action
		CCZk.setCurrentDesktopBookmark();
		return BaseFunction.LIST_FUNCTION;
	}

	@Override
	protected void _initComponent() {
		super._initComponent();

		edtSearch = new CCEditor(winSearch, EtSearch.class);
		initComponent(winSearch, this, null);
		tblTourismType = new CCTable(lstTourismType, EtTourismType.class);
		tblTourismType.setHeadersVisible(false);
		tblTourismType.search();

		edtTourismSite = new CCEditor(winTourismSite, EtTourismSite.class);
		initComponent(winTourismSite, this, null);
	}

	@Override
	protected void _initListener() {
		super._initListener();

		tblTourismType.addListenerOnSelect(elSearch);
		addListener(btnSearch, elSearch);
	}

	@Override
	protected void _doSearch() {
		EtSearch search = getAttribute(EtSearch.class);
		edtSearch.getValueFromEditor();
		search.setTourismType((EtTourismType) tblTourismType.getSelectedItem());
		search.setTourismTypeIndex(tblTourismType.getSelectedRow());

		new ZulSearchCtrl().init();
	}

	@Override
	protected void _doUpdateComponent() {
		EtSearch search = getAttribute(EtSearch.class);
		edtSearch.setValueToEditor(search);
		tblTourismType.setSelectedRow(true, search.getTourismTypeIndex());

		edtTourismSite.setValueToEditor(search.getTourismSite());
		setImageContent(imgPhoto, coalesce(search.getTourismSite().getPhoto(), new EtConfigBo().getConfig().getPhotoTourism()));
		CCRating rating = new CCRating(boxRating);
		rating.setRating(parseInt(calculate("totalRating/totalRater", search.getTourismSite())));
		rating.getBox().setTooltiptext(getString4View(getValue(search.getTourismSite(), "@totalRating/@totalRater")));
		txtPhone.setValue("Phone: " + coalesce(txtPhone.getValue(), "~"));
		if (isNotNullAndNotEmpty(txtEmail.getLabel())) {
			txtEmail.setHref(mailto(txtEmail.getLabel(), null, null, null, null));
		}
		txtEmail.setLabel(coalesce(txtEmail.getLabel(), "Email: ~"));
		if (isNotNullAndNotEmpty(txtWebsite.getLabel())) {
			txtWebsite.setHref("http://" + txtWebsite.getLabel().replaceFirst("http://", ""));
			txtWebsite.setTarget("_blank");
		}
		txtWebsite.setLabel(coalesce(txtWebsite.getLabel(), "Website: ~"));
		if (isNotNullAndNotEmpty(txtReview.getValue())) {
			txtReview.setValue("Review:\n" + txtReview.getValue());
		}

		super._doUpdateComponent();
	}
}