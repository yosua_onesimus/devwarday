package controller.zul.preLogin;

import static cococare.common.CCFormat.parseDouble;
import static cococare.common.CCFormat.parseInt;
import static cococare.zk.CCSession.getAttribute;
import static cococare.zk.CCZk.addListener;
import static cococare.zk.CCZk.execute;
import static cococare.zk.CCZk.getPaging;
import static cococare.zk.CCZk.initSpecialComponent;
import static cococare.zk.CCZk.setHflex;

import java.util.List;

import model.bo.tourism.EtTourismSiteBo;
import model.obj.tourism.EtSearch;
import model.obj.tourism.EtTourismSite;
import model.obj.tourism.EtTourismType;
import model.obj.tourism.EtFilter.isRegency;
import model.obj.tourism.EtFilter.isTourismType;

import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zul.A;
import org.zkoss.zul.Button;
import org.zkoss.zul.Grid;
import org.zkoss.zul.Hbox;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Space;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Vlayout;

import cococare.database.CCHibernateFilter;
import cococare.framework.model.obj.util.UtilFilter.isProvince;
import cococare.framework.zk.CFZkCtrl;
import cococare.zk.CCCombobox;
import cococare.zk.CCEditor;
import cococare.zk.CCPaging;
import cococare.zk.CCTable;
import cococare.zk.CCZk;
import custom.component.CustomView;

public class ZulSearchCtrl extends CFZkCtrl {
	private EtTourismSiteBo tourismSiteBo;

	private CCEditor edtSearch;
	private CCTable tblTourismType;

	private CCPaging pgnResult;

	private Textbox txtName;
	private Grid grdSearch;
	private A aAllTourismType;
	private Listbox lstTourismType;
	private CCCombobox txtRegency;
	private CCCombobox txtProvince;
	private Button btnSearch;

	private Vlayout vlResult;

	@Override
	protected Class _getEntity() {
		return EtTourismSite.class;
	}

	@Override
	protected BaseFunction _getBaseFunction() {
		// to enable backward action / forward action
		CCZk.setCurrentDesktopBookmark();
		return BaseFunction.LIST_FUNCTION;
	}

	@Override
	protected void _initComponent() {
		super._initComponent();

		edtSearch = new CCEditor(grdSearch, EtSearch.class);
		// to init special component coz _getBaseFunction is LIST_FUNCTION
		initSpecialComponent(getContainer(), this);
		tblTourismType = new CCTable(lstTourismType, EtTourismType.class);
		tblTourismType.setHeadersVisible(false);
		tblTourismType.search();
	}

	@Override
	protected void _initTable() {
		pgnResult = new CCPaging(getPaging(getContainer(), "pgnResult"));
		pgnResult.setActivePage(0);
		pgnResult.setPageSize(EtSearch.MAX_RESULTS);
	}

	@Override
	protected void _initListener() {
		super._initListener();

		tblTourismType.addListenerOnSelect(elSearch);
		addListener(aAllTourismType, new EventListener() {
			public void onEvent(Event event) throws Exception {
				tblTourismType.setSelectAll(false);
				execute(elSearch, event);
			}
		});
		addListener(btnSearch, elSearch);
		addListener(pgnResult.getPaging(), elSearch);
	}

	@Override
	protected void _doSearch() {
		EtSearch search = getAttribute(EtSearch.class);
		edtSearch.getValueFromEditor();
		search.setTourismType((EtTourismType) tblTourismType.getSelectedItem());
		search.setTourismTypeIndex(tblTourismType.getSelectedRow());

		super._doSearch();
	}

	@Override
	protected void _doUpdateComponent() {
		EtSearch search = getAttribute(EtSearch.class);
		edtSearch.setValueToEditor(search);
		tblTourismType.setSelectedRow(true, search.getTourismTypeIndex());

		super._doUpdateComponent();
	}

	@Override
	public void doUpdateTable() {
		CCHibernateFilter[] hibernateFilters = new CCHibernateFilter[] { new isTourismType() {
			@Override
			public Object getFieldValue() {
				return tblTourismType.getSelectedItem();
			}
		}, new isRegency() {
			@Override
			public Object getFieldValue() {
				return txtRegency.getSelectedObject();
			}
		}, new isProvince() {
			@Override
			public Object getFieldValue() {
				return txtProvince.getSelectedObject();
			}
		} };
		pgnResult.setTotalSize(tourismSiteBo.count(txtName.getText(), hibernateFilters));
		List<EtTourismSite> tourismSites = tourismSiteBo.getListBy(txtName.getText(), pgnResult.getFirstResult(), hibernateFilters);
		int size = tourismSites.size();
		int columns = 2;
		int rows = parseInt(Math.ceil(parseDouble(size) / parseDouble(columns)));

		vlResult.getChildren().clear();
		CustomView.newSpaceH5().setParent(vlResult);
		for (int row = 0; row < rows; row++) {
			Hbox hbox = new Hbox();
			setHflex(hbox, 1);
			{
				CustomView.newSpaceW5().setParent(hbox);
				for (int column = 0; column < columns; column++) {
					if (size > 0) {
						int index = row * 2 + column;
						CustomView.newComponent(tourismSites.get(index)).setParent(hbox);
						CustomView.newSpaceW5().setParent(hbox);
						size--;
					} else {
						Space space = new Space();
						setHflex(space, 1);
						space.setParent(hbox);
						CustomView.newSpaceW5().setParent(hbox);
					}
				}
			}
			hbox.setParent(vlResult);
			CustomView.newSpaceH5().setParent(vlResult);
		}
	}
}