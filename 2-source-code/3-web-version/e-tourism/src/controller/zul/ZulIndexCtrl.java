package controller.zul;

import static cococare.zk.CCZk.addListener;
import static cococare.zk.CCZk.initComponent;
import model.mdl.tourism.TourismMain;

import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zul.A;
import org.zkoss.zul.Window;

import cococare.framework.zk.CFZkMain;
import cococare.framework.zk.controller.zul.util.ZulLoginCtrl;
import cococare.zk.CCEditor;

public class ZulIndexCtrl extends Window {
	private CCEditor edtLogin;
	private A aLogIn;
	private A aLogOut;

	public void onCreate() {
		CFZkMain.start();
		initComponent(this, this, null);
		EventListener elLogin = new EventListener() {
			public void onEvent(Event event) throws Exception {
				new ZulLoginCtrl().init();
			}
		};
		addListener(aLogIn, elLogin);
		addListener(aLogOut, new EventListener() {
			public void onEvent(Event event) throws Exception {
				TourismMain.INSTANCE.logout();
			}
		});
	}
}