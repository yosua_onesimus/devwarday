package controller.zul.tourism;

import model.obj.tourism.EtTourismAttractionTime;
import cococare.framework.zk.CFZkCtrl;

public class ZulTourismAttractionTimeCtrl extends CFZkCtrl {
	@Override
	protected Class _getEntity() {
		return EtTourismAttractionTime.class;
	}

	@Override
	protected BaseFunction _getBaseFunction() {
		return BaseFunction.FORM_FUNCTION;
	}
}