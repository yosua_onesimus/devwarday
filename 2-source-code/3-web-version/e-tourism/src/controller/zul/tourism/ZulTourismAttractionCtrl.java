package controller.zul.tourism;

import model.obj.tourism.EtTourismAttraction;
import cococare.framework.zk.CFZkCtrl;

public class ZulTourismAttractionCtrl extends CFZkCtrl {
	@Override
	protected Class _getEntity() {
		return EtTourismAttraction.class;
	}

	@Override
	protected BaseFunction _getBaseFunction() {
		return BaseFunction.FORM_FUNCTION;
	}

	@Override
	protected void _initComponent() {
		super._initComponent();
		_addChildScreen("tourismAttraction", new ZulTourismAttractionTicketListCtrl(), "zulTourismAttractionTicket");
		_addChildScreen("tourismAttraction", new ZulTourismAttractionTimeListCtrl(), "zulTourismAttractionTime");
	}
}