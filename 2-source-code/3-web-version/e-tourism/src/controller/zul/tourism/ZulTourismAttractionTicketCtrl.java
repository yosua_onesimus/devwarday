package controller.zul.tourism;

import model.obj.tourism.EtTourismAttractionTicket;
import cococare.framework.zk.CFZkCtrl;

public class ZulTourismAttractionTicketCtrl extends CFZkCtrl {
	@Override
	protected Class _getEntity() {
		return EtTourismAttractionTicket.class;
	}

	@Override
	protected BaseFunction _getBaseFunction() {
		return BaseFunction.FORM_FUNCTION;
	}
}