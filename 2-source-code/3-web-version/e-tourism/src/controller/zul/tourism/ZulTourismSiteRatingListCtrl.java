package controller.zul.tourism;

import model.obj.tourism.EtTourismSiteRating;
import cococare.framework.zk.CFZkCtrl;

public class ZulTourismSiteRatingListCtrl extends CFZkCtrl {
	@Override
	protected Class _getEntity() {
		return EtTourismSiteRating.class;
	}

	@Override
	protected BaseFunction _getBaseFunction() {
		return BaseFunction.LIST_FUNCTION;
	}
}