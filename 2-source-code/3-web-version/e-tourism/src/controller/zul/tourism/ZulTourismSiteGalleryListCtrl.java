package controller.zul.tourism;

import model.obj.tourism.EtTourismSiteGallery;
import cococare.framework.zk.CFZkCtrl;

public class ZulTourismSiteGalleryListCtrl extends CFZkCtrl {
	@Override
	protected Class _getEntity() {
		return EtTourismSiteGallery.class;
	}

	@Override
	protected BaseFunction _getBaseFunction() {
		return BaseFunction.LIST_FUNCTION;
	}
}