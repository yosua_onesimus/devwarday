package controller.zul.tourism;

import model.obj.tourism.EtTourismSite;
import cococare.framework.zk.CFZkCtrl;

public class ZulTourismSiteListCtrl extends CFZkCtrl {
	@Override
	protected Class _getEntity() {
		return EtTourismSite.class;
	}

	@Override
	protected BaseFunction _getBaseFunction() {
		return BaseFunction.LIST_FUNCTION;
	}
}