package controller.zul.tourism;

import model.obj.tourism.EtTourismAttraction;
import cococare.framework.zk.CFZkCtrl;

public class ZulTourismAttractionListCtrl extends CFZkCtrl {
	@Override
	protected Class _getEntity() {
		return EtTourismAttraction.class;
	}

	@Override
	protected BaseFunction _getBaseFunction() {
		return BaseFunction.LIST_FUNCTION;
	}
}