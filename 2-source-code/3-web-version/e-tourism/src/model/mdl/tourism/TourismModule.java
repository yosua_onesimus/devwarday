package model.mdl.tourism;

import java.util.Arrays;
import java.util.List;

import model.obj.tourism.EtTourismAttraction;
import model.obj.tourism.EtTourismAttractionTicket;
import model.obj.tourism.EtTourismAttractionTime;
import model.obj.tourism.EtTourismSite;
import model.obj.tourism.EtTourismSiteGallery;
import model.obj.tourism.EtTourismSiteRating;
import model.obj.tourism.EtTourismType;
import cococare.database.CCHibernateModule;

public class TourismModule extends CCHibernateModule {

	public static TourismModule INSTANCE = new TourismModule();

	@Override
	protected List<Class> _getAnnotatedClasses() {
		return (List) Arrays.asList(EtTourismType.class, EtTourismSite.class, EtTourismAttraction.class, EtTourismAttractionTime.class, EtTourismAttractionTicket.class, EtTourismSiteGallery.class, EtTourismSiteRating.class);
	}
}