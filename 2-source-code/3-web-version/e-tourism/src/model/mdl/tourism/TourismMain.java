package model.mdl.tourism;

import static cococare.common.CCConfig.APPL_UTIL_INCLUDED_PERSON_ENTITIES;
import static cococare.common.CCConfig.TBL_COLUMN_MAX_WIDTH;
import static cococare.common.CCLogic.isNullOrEmpty;
import static cococare.database.CCLoginInfo.INSTANCE_hasLogged;
import static cococare.zk.CCSession.getAttribute;
import static cococare.zk.CCSession.setAttribute;
import static model.mdl.tourism.TourismLanguage.Admin_Archive;
import static model.mdl.tourism.TourismLanguage.Et;
import static model.mdl.tourism.TourismLanguage.Tourism_Site;

import java.util.Arrays;

import model.obj.tourism.EtConfig;
import model.obj.tourism.EtSearch;
import cococare.common.CCLanguage;
import cococare.framework.common.CFApplUae;
import cococare.framework.model.bo.util.UtilConfigBo;
import cococare.framework.zk.CFZkMain;
import cococare.framework.zk.CFZkMap;
import cococare.zk.CCZk;
import controller.zul.preLogin.ZulHomeCtrl;
import controller.zul.tourism.ZulTourismSiteListCtrl;

public class TourismMain extends CFZkMain {
	@Override
	protected void _loadInternalSetting() {
		APPL_CODE = "e-tourism-zk";
		APPL_LOGO = "/resource/img-e-tourism.jpg";
		APPL_NAME = "e-tourist: Tourism Inside Story";
		APPL_UTIL_INCLUDED_PERSON_ENTITIES = true;
		TBL_COLUMN_MAX_WIDTH = 150;
		super._loadInternalSetting();
	}

	@Override
	protected void _loadExternalSetting() {
		CCLanguage.init(false, TourismLanguage.class);
		super._loadExternalSetting();
	}

	@Override
	protected void _initDatabaseEntity() {
		super._initDatabaseEntity();
		TourismModule.INSTANCE.init(HIBERNATE);
	}

	@Override
	protected boolean _initInitialData() {
		UtilConfigBo configBo = new UtilConfigBo();
		confAppl = configBo.loadConfAppl();
		confAppl.setUtilAdditionalSettingClass(Arrays.asList(//
				EtConfig.class.getName()));
		return super._initInitialData()//
				&& configBo.saveConf(confAppl);
	}

	@Override
	protected void _initInitialUaeBody(CFApplUae uae) {
		uae.reg(Et, Tourism_Site, ZulTourismSiteListCtrl.class);
	}

	@Override
	protected void _applyUserConfigUaeBody(CFApplUae uae) {
		uae.addMenuParent(Admin_Archive, "/img/icon-menu-parent.png", null);
		uae.addMenuChild(Tourism_Site, "/img/icon-menu-child.png", ZulTourismSiteListCtrl.class);
	}

	@Override
	public void updateNonContent(Object object) {
		super.updateNonContent(object);
		boolean hasLogged = INSTANCE_hasLogged();
		CCZk.getA(CFZkMap.getMainScreen(), "aLogIn").setVisible(!hasLogged);
		CCZk.getA(CFZkMap.getMainScreen(), "aLogOut").setVisible(hasLogged);
	}

	@Override
	protected boolean _showLoginScreen() {
		// initial search session
		if (isNullOrEmpty(getAttribute(EtSearch.class))) {
			setAttribute(new EtSearch());
		}
		// initial BookmarkEvent to enable backward action / forward action
		CCZk.initBookmarkEvent(CFZkMap.getMainScreen(), ZulHomeCtrl.class, "init");
		return true;
	}
}