package model.mdl.tourism;

import java.util.List;

import cococare.database.CCHibernate;
import cococare.database.CCHibernateDao;
import cococare.database.CCHibernateFilter;

public abstract class TourismDao extends CCHibernateDao {

	@Override
	protected CCHibernate getCCHibernate() {
		return TourismModule.INSTANCE.getCCHibernate();
	}

	@Override
	protected List<CCHibernateFilter> getFilters() {
		return TourismModule.INSTANCE.getFilters();
	}
}