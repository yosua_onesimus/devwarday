package model.dao.tourism;

import model.mdl.tourism.TourismDao;
import model.obj.tourism.EtTourismSite;

public class EtTourismSiteDao extends TourismDao {
	@Override
	protected Class getEntity() {
		return EtTourismSite.class;
	}
}