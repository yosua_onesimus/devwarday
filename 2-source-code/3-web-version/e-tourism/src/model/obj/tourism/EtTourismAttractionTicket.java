package model.obj.tourism;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import cococare.common.CCFieldConfig;
import cococare.common.CCTypeConfig;
import cococare.common.CCFieldConfig.Accessible;
import cococare.common.CCFieldConfig.Type;
import cococare.database.CCEntity;

@Entity
@Table(name = "et_tourism_site_attraction_tickets")
@CCTypeConfig(label = "Attraction Ticket", uniqueKey = "name", parameter = true)
public class EtTourismAttractionTicket extends CCEntity {
	@ManyToOne
	@CCFieldConfig(accessible = Accessible.MANDATORY, maxLength = 64, uniqueKey = "name", visible = false)
	private EtTourismAttraction tourismAttraction;
	@Column(length = 64)
	@CCFieldConfig(accessible = Accessible.MANDATORY, requestFocus = true)
	private String name;
	@CCFieldConfig(accessible = Accessible.MANDATORY, type = Type.NUMBER_FORMAT)
	private Double price;

	public EtTourismAttraction getTourismAttraction() {
		return tourismAttraction;
	}

	public void setTourismAttraction(EtTourismAttraction tourismAttraction) {
		this.tourismAttraction = tourismAttraction;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}
}