package model.obj.tourism;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import cococare.common.CCFieldConfig;
import cococare.common.CCTypeConfig;
import cococare.common.CCFieldConfig.Accessible;
import cococare.common.CCFieldConfig.Type;
import cococare.database.CCEntity;

@Entity
@Table(name = "et_tourism_site_ratings")
@CCTypeConfig(label = "Tourism Rating", uniqueKey = "raterId")
public class EtTourismSiteRating extends CCEntity {
	@ManyToOne
	@CCFieldConfig(accessible = Accessible.MANDATORY, maxLength = 64, uniqueKey = "name", visible = false)
	private EtTourismSite tourismSite;
	@Column(length = 40)
	@CCFieldConfig(accessible = Accessible.MANDATORY_READONLY, visible = false, visible2 = false)
	private String raterId;
	@CCFieldConfig(accessible = Accessible.MANDATORY, type = Type.NUMERIC)
	private Integer rating;
	@Column(length = 255)
	@CCFieldConfig(maxLength = Short.MAX_VALUE)
	private String remarks;

	public EtTourismSite getTourismSite() {
		return tourismSite;
	}

	public void setTourismSite(EtTourismSite tourismSite) {
		this.tourismSite = tourismSite;
	}

	public String getRaterId() {
		return raterId;
	}

	public void setRaterId(String raterId) {
		this.raterId = raterId;
	}

	public Integer getRating() {
		return rating;
	}

	public void setRating(Integer rating) {
		this.rating = rating;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
}