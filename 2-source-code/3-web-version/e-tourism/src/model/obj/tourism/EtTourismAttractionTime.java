package model.obj.tourism;

import static cococare.common.CCFormat.getMaxTime;
import static cococare.common.CCFormat.getMinTime;

import java.sql.Time;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import cococare.common.CCFieldConfig;
import cococare.common.CCTypeConfig;
import cococare.common.CCFieldConfig.Accessible;
import cococare.common.CCFieldConfig.CompareRule;
import cococare.database.CCEntity;

@Entity
@Table(name = "et_tourism_site_attraction_times")
@CCTypeConfig(label = "Attraction Time", uniqueKey = "name", parameter = true)
public class EtTourismAttractionTime extends CCEntity {
	@ManyToOne
	@CCFieldConfig(accessible = Accessible.MANDATORY, maxLength = 64, uniqueKey = "name", visible = false)
	private EtTourismAttraction tourismAttraction;
	@Column(length = 32)
	@CCFieldConfig(accessible = Accessible.MANDATORY, requestFocus = true)
	private String name;
	@CCFieldConfig(accessible = Accessible.MANDATORY, maxLength = 5)
	private Time open = new Time(getMinTime(new Date()).getTime());
	@CCFieldConfig(accessible = Accessible.MANDATORY, maxLength = 5, compareRule = CompareRule.GREATER_OR_EQUAL_THAN, compareWith = "txtCounterClosed")
	private Time close = new Time(getMaxTime(new Date()).getTime());
	@CCFieldConfig(accessible = Accessible.MANDATORY, maxLength = 5, compareRule = CompareRule.GREATER_THAN, compareWith = "txtOpen")
	private Time counterClosed = new Time(getMaxTime(new Date()).getTime());

	public EtTourismAttraction getTourismAttraction() {
		return tourismAttraction;
	}

	public void setTourismAttraction(EtTourismAttraction tourismAttraction) {
		this.tourismAttraction = tourismAttraction;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Time getOpen() {
		return open;
	}

	public void setOpen(Time open) {
		this.open = open;
	}

	public Time getClose() {
		return close;
	}

	public void setClose(Time close) {
		this.close = close;
	}

	public Time getCounterClosed() {
		return counterClosed;
	}

	public void setCounterClosed(Time counterClosed) {
		this.counterClosed = counterClosed;
	}
}