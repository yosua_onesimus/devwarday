package model.obj.tourism;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import cococare.common.CCFieldConfig;
import cococare.common.CCTypeConfig;
import cococare.common.CCFieldConfig.Accessible;
import cococare.database.CCEntity;

@Entity
@Table(name = "et_tourism_site_attractions")
@CCTypeConfig(label = "Tourism Attraction", uniqueKey = "name", parameter = true)
public class EtTourismAttraction extends CCEntity {
	@ManyToOne
	@CCFieldConfig(accessible = Accessible.MANDATORY, maxLength = 64, uniqueKey = "name", visible = false)
	private EtTourismSite tourismSite;
	@Column(length = 64)
	@CCFieldConfig(accessible = Accessible.MANDATORY, requestFocus = true)
	private String name;

	public EtTourismSite getTourismSite() {
		return tourismSite;
	}

	public void setTourismSite(EtTourismSite tourismSite) {
		this.tourismSite = tourismSite;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}