package model.obj.tourism;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import cococare.common.CCFieldConfig;
import cococare.common.CCTypeConfig;
import cococare.common.CCFieldConfig.Accessible;
import cococare.database.CCEntity;

@Entity
@Table(name = "et_tourism_types")
@CCTypeConfig(label = "Tourism Type", uniqueKey = "name", parameter = true)
public class EtTourismType extends CCEntity {
	@Column(length = 8)
	@CCFieldConfig(accessible = Accessible.MANDATORY, sequence = "TT00", unique = true, visible = false)
	private String code;
	@Column(length = 32)
	@CCFieldConfig(accessible = Accessible.MANDATORY, requestFocus = true)
	private String name;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}