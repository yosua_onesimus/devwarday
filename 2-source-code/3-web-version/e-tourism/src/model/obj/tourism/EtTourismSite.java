package model.obj.tourism;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import cococare.common.CCFieldConfig;
import cococare.common.CCTypeConfig;
import cococare.common.CCFieldConfig.Accessible;
import cococare.common.CCFieldConfig.Type;
import cococare.database.CCEntity;
import cococare.framework.model.obj.util.UtilProvince;
import cococare.framework.model.obj.util.UtilRegency;

@Entity
@Table(name = "et_tourism_sites")
@CCTypeConfig(label = "Tourism Site", uniqueKey = "name", parameter = true, controllerClass = "controller.pseudo.tourism.EtTourismSiteCtrl")
public class EtTourismSite extends CCEntity {
	@Column(length = 8)
	@CCFieldConfig(group = "Profile", accessible = Accessible.MANDATORY, sequence = "TS00", unique = true, visible = false)
	private String code;
	@Column(length = 64)
	@CCFieldConfig(group = "Profile", accessible = Accessible.MANDATORY, requestFocus = true)
	private String name;
	@ManyToOne
	@CCFieldConfig(group = "Profile", accessible = Accessible.MANDATORY, maxLength = 32, uniqueKey = "name")
	private EtTourismType tourismType;
	@CCFieldConfig(group = "Profile", label = "Owner Type", accessible = Accessible.MANDATORY, optionSource = "model.obj.tourism.EtEnum$OwnerType", optionReflectKey = "ownerType", visible = false)
	private Integer ownerTypeIndex;
	@Column(length = 12)
	@CCFieldConfig(group = "Profile", maxLength = 12, visible2 = false)
	private String ownerType;
	// ----------------------------------------------------------------------------------------------
	@Lob
	@Column(length = Integer.MAX_VALUE)
	@CCFieldConfig(group = "Profile", type = Type.THUMB_FILE, optionReflectKey = "photoName", visible = false)
	private byte[] photo;
	@Column(length = 255)
	@CCFieldConfig(group = "Profile", visible = false, visible2 = false)
	private String photoName;
	// ----------------------------------------------------------------------------------------------
	@Column(length = 255)
	@CCFieldConfig(group = "Address", maxLength = Short.MAX_VALUE)
	private String address;
	@ManyToOne
	@CCFieldConfig(group = "Address", tooltiptext = "Kabupaten", accessible = Accessible.MANDATORY, maxLength = 48, uniqueKey = "name")
	private UtilRegency regency;
	@ManyToOne
	@CCFieldConfig(group = "Address", tooltiptext = "Propinsi", accessible = Accessible.MANDATORY, maxLength = 48, uniqueKey = "name")
	private UtilProvince province;
	@Column(length = 5)
	@CCFieldConfig(group = "Address", minLength = 5, type = Type.NUMBER_ONLY, visible = false)
	private String zipCode;
	// ----------------------------------------------------------------------------------------------
	@Column(length = 16)
	@CCFieldConfig(group = "Contact", type = Type.PHONE_NUMBER, visible = false)
	private String phone;
	@Column(length = 32)
	@CCFieldConfig(group = "Contact", type = Type.EMAIL, visible = false)
	private String email;
	@Column(length = 48)
	@CCFieldConfig(group = "Contact", visible = false)
	private String website;
	// ----------------------------------------------------------------------------------------------
	@Column(length = Short.MAX_VALUE)
	@CCFieldConfig(group = "Review", accessible = Accessible.MANDATORY, maxLength = Short.MAX_VALUE, visible = false)
	private String review;
	@Column(length = Short.MAX_VALUE)
	@CCFieldConfig(group = "Review", maxLength = Short.MAX_VALUE, visible = false)
	private String history;
	@Column(length = Short.MAX_VALUE)
	@CCFieldConfig(group = "Review", maxLength = Short.MAX_VALUE, visible = false)
	private String advice;
	@Column(length = Short.MAX_VALUE)
	@CCFieldConfig(group = "Review", maxLength = Short.MAX_VALUE, visible = false)
	private String transport;
	// ----------------------------------------------------------------------------------------------
	@Column(length = Short.MAX_VALUE)
	@CCFieldConfig(group = "Operation Time", maxLength = Short.MAX_VALUE, visible = false)
	private String specialPrice;
	@Column(length = Short.MAX_VALUE)
	@CCFieldConfig(group = "Operation Time", maxLength = Short.MAX_VALUE, visible = false)
	private String operationRemarks;
	// ----------------------------------------------------------------------------------------------
	@CCFieldConfig(accessible = Accessible.READONLY, type = Type.NUMERIC, visible = false, visible2 = false)
	private Integer totalRating = 0;
	@CCFieldConfig(accessible = Accessible.READONLY, type = Type.NUMERIC, visible = false, visible2 = false)
	private Integer totalRater = 0;
	// ----------------------------------------------------------------------------------------------
	@CCFieldConfig(maxLength = 4, visible = false)
	private Boolean partnership = false;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public EtTourismType getTourismType() {
		return tourismType;
	}

	public void setTourismType(EtTourismType tourismType) {
		this.tourismType = tourismType;
	}

	public Integer getOwnerTypeIndex() {
		return ownerTypeIndex;
	}

	public void setOwnerTypeIndex(Integer ownerTypeIndex) {
		this.ownerTypeIndex = ownerTypeIndex;
	}

	public String getOwnerType() {
		return ownerType;
	}

	public void setOwnerType(String ownerType) {
		this.ownerType = ownerType;
	}

	public byte[] getPhoto() {
		return photo;
	}

	public void setPhoto(byte[] photo) {
		this.photo = photo;
	}

	public String getPhotoName() {
		return photoName;
	}

	public void setPhotoName(String photoName) {
		this.photoName = photoName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public UtilRegency getRegency() {
		return regency;
	}

	public void setRegency(UtilRegency regency) {
		this.regency = regency;
	}

	public UtilProvince getProvince() {
		return province;
	}

	public void setProvince(UtilProvince province) {
		this.province = province;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getWebsite() {
		return website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	public String getReview() {
		return review;
	}

	public void setReview(String review) {
		this.review = review;
	}

	public String getHistory() {
		return history;
	}

	public void setHistory(String history) {
		this.history = history;
	}

	public String getAdvice() {
		return advice;
	}

	public void setAdvice(String advice) {
		this.advice = advice;
	}

	public void setTransport(String transport) {
		this.transport = transport;
	}

	public String getTransport() {
		return transport;
	}

	public String getSpecialPrice() {
		return specialPrice;
	}

	public void setSpecialPrice(String specialPrice) {
		this.specialPrice = specialPrice;
	}

	public String getOperationRemarks() {
		return operationRemarks;
	}

	public void setOperationRemarks(String operationRemarks) {
		this.operationRemarks = operationRemarks;
	}

	public Integer getTotalRating() {
		return totalRating;
	}

	public void setTotalRating(Integer totalRating) {
		this.totalRating = totalRating;
	}

	public Integer getTotalRater() {
		return totalRater;
	}

	public void setTotalRater(Integer totalRater) {
		this.totalRater = totalRater;
	}

	public Boolean getPartnership() {
		return partnership;
	}

	public void setPartnership(Boolean partnership) {
		this.partnership = partnership;
	}
}