package model.obj.tourism;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import cococare.common.CCFieldConfig;
import cococare.common.CCTypeConfig;
import cococare.common.CCFieldConfig.Accessible;
import cococare.common.CCFieldConfig.Type;
import cococare.database.CCEntity;

@Entity
@Table(name = "et_tourism_site_galleries")
@CCTypeConfig(label = "Tourism Gallery", uniqueKey = "label", parameter = true)
public class EtTourismSiteGallery extends CCEntity {
	@ManyToOne
	@CCFieldConfig(accessible = Accessible.MANDATORY, maxLength = 64, uniqueKey = "name", visible = false)
	private EtTourismSite tourismSite;
	@Column(length = 32)
	@CCFieldConfig(accessible = Accessible.MANDATORY, requestFocus = true)
	private String label;
	@Lob
	@Column(length = Integer.MAX_VALUE)
	@CCFieldConfig(type = Type.THUMB_FILE, optionReflectKey = "photoName", visible = false)
	private byte[] photo;
	@Column(length = 255)
	@CCFieldConfig(visible = false, visible2 = false)
	private String photoName;

	public EtTourismSite getTourismSite() {
		return tourismSite;
	}

	public void setTourismSite(EtTourismSite tourismSite) {
		this.tourismSite = tourismSite;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public byte[] getPhoto() {
		return photo;
	}

	public void setPhoto(byte[] photo) {
		this.photo = photo;
	}

	public String getPhotoName() {
		return photoName;
	}

	public void setPhotoName(String photoName) {
		this.photoName = photoName;
	}
}