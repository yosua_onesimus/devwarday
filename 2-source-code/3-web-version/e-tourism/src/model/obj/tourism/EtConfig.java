package model.obj.tourism;

import javax.persistence.Column;
import javax.persistence.Lob;

import cococare.common.CCFieldConfig;
import cococare.common.CCTypeConfig;
import cococare.common.CCFieldConfig.Accessible;
import cococare.common.CCFieldConfig.Type;
import cococare.database.CCEntity;
import cococare.framework.model.obj.util.UtilUserGroup;

@CCTypeConfig(label = "Tourism Module", tooltiptext = "Default Group, etc", controllerClass = "controller.pseudo.tourism.EtConfigCtrl")
public class EtConfig extends CCEntity {
	@CCFieldConfig(group = "Default Group", label = "Tourism", accessible = Accessible.MANDATORY, maxLength = 32, uniqueKey = "name")
	private UtilUserGroup groupTourism;
	@Lob
	@Column(length = Integer.MAX_VALUE)
	@CCFieldConfig(group = "Default Photo", label = "Tourism", type = Type.THUMB_FILE, optionReflectKey = "photoTourismName")
	private byte[] photoTourism;
	@Column(length = 255)
	@CCFieldConfig(visible = false, visible2 = false)
	private String photoTourismName;

	public UtilUserGroup getGroupTourism() {
		return groupTourism;
	}

	public void setGroupTourism(UtilUserGroup groupTourism) {
		this.groupTourism = groupTourism;
	}

	public byte[] getPhotoTourism() {
		return photoTourism;
	}

	public void setPhotoTourism(byte[] photoTourism) {
		this.photoTourism = photoTourism;
	}

	public String getPhotoTourismName() {
		return photoTourismName;
	}

	public void setPhotoTourismName(String photoTourismName) {
		this.photoTourismName = photoTourismName;
	}
}