package model.obj.tourism;

import cococare.common.CCFieldConfig;
import cococare.framework.model.obj.util.UtilProvince;
import cococare.framework.model.obj.util.UtilRegency;

public class EtSearch {

	public static int MAX_RESULTS = 10;
	@CCFieldConfig(maxLength = 32, requestFocus = true)
	private String name;
	@CCFieldConfig(maxLength = 32, uniqueKey = "name")
	private EtTourismType tourismType;
	@CCFieldConfig(label = "Owner Type", optionSource = "model.obj.tourism.EtEnum$OwnerType")
	private Integer ownerTypeIndex = 0;
	@CCFieldConfig(tooltiptext = "Kabupaten", maxLength = 48, uniqueKey = "name")
	private UtilRegency regency;
	@CCFieldConfig(tooltiptext = "Propinsi", maxLength = 48, uniqueKey = "name")
	private UtilProvince province;
	private EtTourismSite tourismSite;
	private int tourismTypeIndex = -1;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public EtTourismType getTourismType() {
		return tourismType;
	}

	public void setTourismType(EtTourismType tourismType) {
		this.tourismType = tourismType;
	}

	public Integer getOwnerTypeIndex() {
		return ownerTypeIndex;
	}

	public void setOwnerTypeIndex(Integer ownerTypeIndex) {
		this.ownerTypeIndex = ownerTypeIndex;
	}

	public UtilRegency getRegency() {
		return regency;
	}

	public void setRegency(UtilRegency regency) {
		this.regency = regency;
	}

	public UtilProvince getProvince() {
		return province;
	}

	public void setProvince(UtilProvince province) {
		this.province = province;
	}

	public void setTourismSite(EtTourismSite tourismSite) {
		this.tourismSite = tourismSite;
	}

	public EtTourismSite getTourismSite() {
		return tourismSite;
	}

	public int getTourismTypeIndex() {
		return tourismTypeIndex;
	}

	public void setTourismTypeIndex(int tourismTypeIndex) {
		this.tourismTypeIndex = tourismTypeIndex;
	}
}