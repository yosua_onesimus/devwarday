package model.obj.tourism;

import cococare.database.CCHibernateFilter;

public class EtFilter {
	public static abstract class isTourismType extends CCHibernateFilter {
		@Override
		public String getFieldName() {
			return "tourismType";
		}
	};

	public static abstract class isOwnerTypeIndex extends CCHibernateFilter {
		@Override
		public String getFieldName() {
			return "ownerTypeIndex";
		}
	};

	public static abstract class isRegency extends CCHibernateFilter {
		@Override
		public String getFieldName() {
			return "regency";
		}
	};
}