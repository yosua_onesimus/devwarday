package model.bo.tourism;

import java.util.List;

import model.dao.tourism.EtTourismSiteDao;
import model.obj.tourism.EtSearch;
import model.obj.tourism.EtTourismSite;
import cococare.database.CCHibernateBo;
import cococare.database.CCHibernateFilter;

public class EtTourismSiteBo extends CCHibernateBo {
	private EtTourismSiteDao tourismSiteDao;

	public synchronized long count(String keyword, CCHibernateFilter... hibernateFilters) {
		return tourismSiteDao.count(keyword, hibernateFilters);
	}

	public synchronized List<EtTourismSite> getListBy(String keyword, int firstResult, CCHibernateFilter... hibernateFilters) {
		return tourismSiteDao.getListBy(keyword, firstResult, EtSearch.MAX_RESULTS, hibernateFilters);
	}
}