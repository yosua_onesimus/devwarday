package model.bo.tourism;

import static cococare.common.CCLogic.isNotNull;
import static cococare.database.CCLoginInfo.INSTANCE_getUserLogin;
import model.obj.tourism.EtConfig;
import cococare.database.CCHibernateBo;
import cococare.framework.model.dao.util.UtilConfigDao;
import cococare.framework.model.obj.util.UtilUser;
import cococare.framework.model.obj.util.UtilUserGroup;

public class EtConfigBo extends CCHibernateBo {

	private UtilConfigDao configDao;
	private EtConfig config = loadEtConfig();

	public synchronized EtConfig loadEtConfig() {
		return configDao.loadHash(EtConfig.class);
	}

	public synchronized boolean saveConf(Object object) {
		return configDao.saveHash(object);
	}

	public synchronized EtConfig getConfig() {
		return config;
	}

	public synchronized boolean isGroupTourism() {
		UtilUser user;
		UtilUserGroup groupTourism;
		if (isNotNull(user = INSTANCE_getUserLogin())// 
				&& isNotNull(groupTourism = getConfig().getGroupTourism())) {
			return groupTourism.getId().equals(user.getUserGroup().getId());
		}
		return false;
	}
}