package model.dao.emergency;

import model.mdl.emergency.EmergencyDao;
import model.obj.emergency.EeDoctor;
import cococare.framework.model.obj.util.UtilUser;

public class EeDoctorDao extends EmergencyDao {
	@Override
	protected Class getEntity() {
		return EeDoctor.class;
	}

	public EeDoctor getBy(UtilUser user) {
		return getByField("user", user);
	}
}