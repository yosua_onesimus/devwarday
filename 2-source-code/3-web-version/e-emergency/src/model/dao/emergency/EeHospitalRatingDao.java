package model.dao.emergency;

import static cococare.common.CCFormat.parseInt;
import model.mdl.emergency.EmergencyDao;
import model.obj.emergency.EeHospital;
import model.obj.emergency.EeHospitalRating;

public class EeHospitalRatingDao extends EmergencyDao {
	@Override
	protected Class getEntity() {
		return EeHospitalRating.class;
	}

	public boolean hasRated(EeHospital hospital, String raterId) {
		hql.start().//
				select("COUNT(*)").//
				where("hospital = :hospital").//
				where("raterId = :raterId");
		parameters.start().//
				set("hospital", hospital).//
				set("raterId", raterId);
		return parseInt(getBy(hql.value(), parameters.value())) > 0;
	}

	public int getTotalRating(EeHospital hospital) {
		hql.start().//
				select("SUM(rating)").//
				where("hospital = :hospital");
		parameters.start().//
				set("hospital", hospital);
		return parseInt(getBy(hql.value(), parameters.value()));
	}

	public int getTotalRater(EeHospital hospital) {
		hql.start().//
				select("COUNT(rating)").//
				where("hospital = :hospital");
		parameters.start().//
				set("hospital", hospital);
		return parseInt(getBy(hql.value(), parameters.value()));
	}
}