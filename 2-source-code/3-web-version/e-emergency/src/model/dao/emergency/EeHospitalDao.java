package model.dao.emergency;

import model.mdl.emergency.EmergencyDao;
import model.obj.emergency.EeHospital;
import cococare.framework.model.obj.util.UtilUser;

public class EeHospitalDao extends EmergencyDao {
	@Override
	protected Class getEntity() {
		return EeHospital.class;
	}

	public EeHospital getBy(UtilUser user) {
		return getByField("user", user);
	}
}