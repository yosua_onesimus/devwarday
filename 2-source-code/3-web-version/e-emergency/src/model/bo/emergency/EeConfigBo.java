package model.bo.emergency;

import static cococare.common.CCLogic.isNotNull;
import static cococare.database.CCLoginInfo.INSTANCE_getUserLogin;
import model.dao.emergency.EeDoctorDao;
import model.dao.emergency.EeHospitalDao;
import model.obj.emergency.EeConfig;
import model.obj.emergency.EeDoctor;
import model.obj.emergency.EeHospital;
import cococare.database.CCHibernateBo;
import cococare.framework.model.dao.util.UtilConfigDao;
import cococare.framework.model.obj.util.UtilUser;
import cococare.framework.model.obj.util.UtilUserGroup;

public class EeConfigBo extends CCHibernateBo {
	private UtilConfigDao configDao;
	private EeHospitalDao hospitalDao;
	private EeDoctorDao doctorDao;
	private EeConfig config = loadEeConfig();

	public synchronized EeConfig loadEeConfig() {
		return configDao.loadHash(EeConfig.class);
	}

	public synchronized boolean saveConf(Object object) {
		return configDao.saveHash(object);
	}

	public synchronized EeConfig getConfig() {
		return config;
	}

	public synchronized boolean isGroupHospital() {
		UtilUser user;
		UtilUserGroup groupHospital;
		if (isNotNull(user = INSTANCE_getUserLogin())// 
				&& isNotNull(groupHospital = getConfig().getGroupHospital())) {
			return groupHospital.getId().equals(user.getUserGroup().getId());
		}
		return false;
	}

	public synchronized boolean isGroupDoctor() {
		UtilUser user;
		UtilUserGroup groupDoctor;
		if (isNotNull(user = INSTANCE_getUserLogin())// 
				&& isNotNull(groupDoctor = getConfig().getGroupDoctor())) {
			return groupDoctor.getId().equals(user.getUserGroup().getId());
		}
		return false;
	}

	public synchronized EeHospital getHospitalLogin() {
		return hospitalDao.getBy((UtilUser) INSTANCE_getUserLogin());
	}

	public synchronized EeDoctor getDoctorLogin() {
		return doctorDao.getBy((UtilUser) INSTANCE_getUserLogin());
	}
}