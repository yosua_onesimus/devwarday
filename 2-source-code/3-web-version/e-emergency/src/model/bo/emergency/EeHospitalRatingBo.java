package model.bo.emergency;

import model.dao.emergency.EeHospitalDao;
import model.dao.emergency.EeHospitalRatingDao;
import model.obj.emergency.EeHospital;
import model.obj.emergency.EeHospitalRating;
import cococare.database.CCHibernateBo;
import cococare.database.CCHibernate.Transaction;

public class EeHospitalRatingBo extends CCHibernateBo {
	private EeHospitalDao hospitalDao;
	private EeHospitalRatingDao hospitalRatingDao;

	public synchronized boolean hasRated(EeHospital hospital, String raterId) {
		return hospitalRatingDao.hasRated(hospital, raterId);
	}

	public synchronized boolean saveOrUpdate(EeHospitalRating hospitalRating) {
		Transaction transaction = hospitalRatingDao.newTransaction();
		transaction.saveOrUpdate(hospitalRating);

		EeHospital hospital = hospitalDao.reLoad(hospitalRating.getHospital());
		hospital.setTotalRating(hospitalRatingDao.getTotalRating(hospital) + hospitalRating.getRating());
		hospital.setTotalRater(hospitalRatingDao.getTotalRater(hospital) + 1);
		transaction.saveOrUpdate(hospital);

		return transaction.execute();
	}
}