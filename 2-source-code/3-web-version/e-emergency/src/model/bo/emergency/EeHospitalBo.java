package model.bo.emergency;

import static cococare.common.CCLogic.isNotNull;
import static cococare.common.CCLogic.isNull;

import java.util.List;

import model.dao.emergency.EeHospitalDao;
import model.obj.emergency.EeConfig;
import model.obj.emergency.EeHospital;
import cococare.database.CCHibernateBo;
import cococare.database.CCHibernate.Transaction;
import cococare.framework.model.obj.util.UtilUser;

public class EeHospitalBo extends CCHibernateBo {
	private EeConfigBo configBo = new EeConfigBo();
	private EeHospitalDao hospitalDao;

	public synchronized boolean saveOrUpdate(EeHospital hospital, List entityChilds) {
		Transaction transaction = hospitalDao.newTransaction();
		EeConfig config = configBo.getConfig();
		if (isNotNull(config.getGroupHospital())) {
			// if hospital become e-mergency partner,
			// then create user for the hospital login
			if (hospital.isPartnership() && isNull(hospital.getUser())) {
				hospital.setUser(new UtilUser(//
						hospital.getCode(),//
						hospital.getCode(),//
						hospital.getName(),// 
						config.getGroupHospital()));
			}
			if (isNotNull(hospital.getUser())) {
				hospital.getUser().setActive(hospital.isPartnership());
				transaction.saveOrUpdate(hospital.getUser());
			}
		}
		transaction.saveOrUpdate(hospital);
		return transaction.//
				saveOrUpdate(hospital).//
				saveOrUpdate(entityChilds).//
				execute();
	}

	public synchronized boolean deleteById(EeHospital hospital) {
		return hospitalDao.newTransaction().//
				deleteById(hospital).//
				deleteById(hospital.getUser()).//
				execute();
	}
}