package model.bo.emergency;

import static cococare.common.CCLogic.isNotNull;
import static cococare.common.CCLogic.isNull;

import java.util.List;

import model.dao.emergency.EeDoctorDao;
import model.obj.emergency.EeConfig;
import model.obj.emergency.EeDoctor;
import cococare.database.CCHibernateBo;
import cococare.database.CCHibernate.Transaction;
import cococare.framework.model.obj.util.UtilUser;

public class EeDoctorBo extends CCHibernateBo {
	private EeConfigBo configBo = new EeConfigBo();
	private EeDoctorDao doctorDao;

	public synchronized boolean saveOrUpdate(EeDoctor doctor, List entityChilds) {
		Transaction transaction = doctorDao.newTransaction();
		EeConfig config = configBo.getConfig();
		if (isNotNull(config.getGroupDoctor())) {
			// if doctor become e-mergency partner,
			// then create user for the doctor login
			if (doctor.isPartnership() && isNull(doctor.getUser())) {
				doctor.setUser(new UtilUser(//
						doctor.getCode(),//
						doctor.getCode(),//
						doctor.getName(),// 
						config.getGroupDoctor()));
			}
			if (isNotNull(doctor.getUser())) {
				doctor.getUser().setActive(doctor.isPartnership());
				transaction.saveOrUpdate(doctor.getUser());
			}
		}
		transaction.saveOrUpdate(doctor);
		return transaction.//
				saveOrUpdate(doctor).//
				saveOrUpdate(entityChilds).//
				execute();
	}

	public synchronized boolean deleteById(EeDoctor doctor) {
		return doctorDao.newTransaction().//
				deleteById(doctor).//
				deleteById(doctor.getUser()).//
				execute();
	}
}