package model.obj.emergency;

import javax.persistence.Column;
import javax.persistence.Lob;

import cococare.common.CCFieldConfig;
import cococare.common.CCTypeConfig;
import cococare.common.CCFieldConfig.Accessible;
import cococare.common.CCFieldConfig.Type;
import cococare.database.CCEntity;
import cococare.framework.model.obj.util.UtilUserGroup;

@CCTypeConfig(label = "Emergency Module", tooltiptext = "Default Group, Default Photo, etc", controllerClass = "controller.pseudo.emergency.EeConfigCtrl")
public class EeConfig extends CCEntity {
	@CCFieldConfig(group = "Default Group", label = "Doctor", accessible = Accessible.MANDATORY, maxLength = 32, uniqueKey = "name")
	private UtilUserGroup groupDoctor;
	@CCFieldConfig(group = "Default Group", label = "Hospital", accessible = Accessible.MANDATORY, maxLength = 32, uniqueKey = "name")
	private UtilUserGroup groupHospital;
	@Lob
	@Column(length = Integer.MAX_VALUE)
	@CCFieldConfig(group = "Default Photo", label = "Doctor", type = Type.THUMB_FILE, optionReflectKey = "photoDoctorName")
	private byte[] photoDoctor;
	@Column(length = 255)
	@CCFieldConfig(visible = false, visible2 = false)
	private String photoDoctorName;
	@Lob
	@Column(length = Integer.MAX_VALUE)
	@CCFieldConfig(group = "Default Photo", label = "Hospital", type = Type.THUMB_FILE, optionReflectKey = "photoHospitalName")
	private byte[] photoHospital;
	@Column(length = 255)
	@CCFieldConfig(visible = false, visible2 = false)
	private String photoHospitalName;

	public UtilUserGroup getGroupDoctor() {
		return groupDoctor;
	}

	public void setGroupDoctor(UtilUserGroup groupDoctor) {
		this.groupDoctor = groupDoctor;
	}

	public UtilUserGroup getGroupHospital() {
		return groupHospital;
	}

	public void setGroupHospital(UtilUserGroup groupHospital) {
		this.groupHospital = groupHospital;
	}

	public byte[] getPhotoDoctor() {
		return photoDoctor;
	}

	public void setPhotoDoctor(byte[] photoDoctor) {
		this.photoDoctor = photoDoctor;
	}

	public String getPhotoDoctorName() {
		return photoDoctorName;
	}

	public void setPhotoDoctorName(String photoDoctorName) {
		this.photoDoctorName = photoDoctorName;
	}

	public byte[] getPhotoHospital() {
		return photoHospital;
	}

	public void setPhotoHospital(byte[] photoHospital) {
		this.photoHospital = photoHospital;
	}

	public String getPhotoHospitalName() {
		return photoHospitalName;
	}

	public void setPhotoHospitalName(String photoHospitalName) {
		this.photoHospitalName = photoHospitalName;
	}
}