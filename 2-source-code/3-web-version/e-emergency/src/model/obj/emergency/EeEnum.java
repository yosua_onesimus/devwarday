package model.obj.emergency;

public class EeEnum {
	public enum Searching {
		HOSPITAL("Hospital"), DOCTOR("Doctor");
		private String string;

		private Searching(String string) {
			this.string = string;
		}

		@Override
		public String toString() {
			return string;
		}
	}

	public enum OwnerType {
		GOVERNMENT("Government"), PRIVATE("Private");
		private String string;

		private OwnerType(String string) {
			this.string = string;
		}

		@Override
		public String toString() {
			return string;
		}
	}

	public enum Day {

		SUNDAY("Sunday"), MONDAY("Monday"), TUESDAY("Tuesday"), WEDNESDAY("Wednesday"), THURSDAY("Thursday"), FRIDAY("Friday"), SATURDAY("Saturday");
		private String string;

		private Day(String string) {
			this.string = string;
		}

		@Override
		public String toString() {
			return string;
		}
	}
}