package model.obj.emergency;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import cococare.common.CCFieldConfig;
import cococare.common.CCTypeConfig;
import cococare.common.CCFieldConfig.Accessible;
import cococare.database.CCEntity;

@Entity
@Table(name = "ee_hospital_types")
@CCTypeConfig(label = "Hospital Type", uniqueKey = "name", parameter = true)
public class EeHospitalType extends CCEntity {
	@Column(length = 8)
	@CCFieldConfig(accessible = Accessible.MANDATORY, sequence = "HT00", unique = true, visible = false)
	private String code;
	@Column(length = 32)
	@CCFieldConfig(accessible = Accessible.MANDATORY, requestFocus = true)
	private String name;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}