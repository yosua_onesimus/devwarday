package model.obj.emergency;

import static cococare.common.CCFormat.getMaxTime;
import static cococare.common.CCFormat.getMinTime;

import java.sql.Time;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import cococare.common.CCFieldConfig;
import cococare.common.CCTypeConfig;
import cococare.common.CCFieldConfig.Accessible;
import cococare.common.CCFieldConfig.CompareRule;
import cococare.database.CCEntity;

@Entity
@Table(name = "ee_schedules")
@CCTypeConfig(label = "Schedule", uniqueKey = "@hospital.name-@doctor.name-@day, @timeBegin-@timeEnd", parameter = true)
public class EeSchedule extends CCEntity {
	@ManyToOne
	@CCFieldConfig(accessible = Accessible.MANDATORY, maxLength = 64, uniqueKey = "name")
	private EeHospital hospital;
	@ManyToOne
	@CCFieldConfig(accessible = Accessible.MANDATORY, maxLength = 64, uniqueKey = "name")
	private EeDoctor doctor;
	@CCFieldConfig(label = "Day", accessible = Accessible.MANDATORY, optionSource = "model.obj.emergency.EeEnum$Day", optionReflectKey = "day", visible = false)
	private Integer dayIndex;
	@Column(name = "day_", length = 12)
	@CCFieldConfig(maxLength = 12, visible2 = false)
	private String day;
	@CCFieldConfig(accessible = Accessible.MANDATORY, maxLength = 6, compareRule = CompareRule.SMALLER_OR_EQUAL_THAN, compareWith = "txtTimeEnd")
	private Time timeBegin = new Time(getMinTime(new Date()).getTime());
	@CCFieldConfig(accessible = Accessible.MANDATORY, maxLength = 6)
	private Time timeEnd = new Time(getMaxTime(new Date()).getTime());

	public EeHospital getHospital() {
		return hospital;
	}

	public void setHospital(EeHospital hospital) {
		this.hospital = hospital;
	}

	public EeDoctor getDoctor() {
		return doctor;
	}

	public void setDoctor(EeDoctor doctor) {
		this.doctor = doctor;
	}

	public Integer getDayIndex() {
		return dayIndex;
	}

	public void setDayIndex(Integer dayIndex) {
		this.dayIndex = dayIndex;
	}

	public String getDay() {
		return day;
	}

	public void setDay(String day) {
		this.day = day;
	}

	public Time getTimeBegin() {
		return timeBegin;
	}

	public void setTimeBegin(Time timeBegin) {
		this.timeBegin = timeBegin;
	}

	public Time getTimeEnd() {
		return timeEnd;
	}

	public void setTimeEnd(Time timeEnd) {
		this.timeEnd = timeEnd;
	}
}