package model.obj.emergency;

import static cococare.common.CCFormat.getBoolean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import cococare.common.CCFieldConfig;
import cococare.common.CCTypeConfig;
import cococare.common.CCFieldConfig.Accessible;
import cococare.common.CCFieldConfig.Type;
import cococare.database.CCEntity;
import cococare.framework.model.obj.util.UtilProvince;
import cococare.framework.model.obj.util.UtilRegency;
import cococare.framework.model.obj.util.UtilUser;

@Entity
@Table(name = "ee_hospitals")
@CCTypeConfig(label = "Hospital", uniqueKey = "name", parameter = true, controllerClass = "controller.pseudo.emergency.EeHospitalCtrl")
public class EeHospital extends CCEntity {
	@Column(length = 8)
	@CCFieldConfig(group = "Profile", accessible = Accessible.MANDATORY, sequence = "H000", unique = true, visible = false)
	private String code;
	@Column(length = 64)
	@CCFieldConfig(group = "Profile", accessible = Accessible.MANDATORY, requestFocus = true)
	private String name;
	@ManyToOne
	@CCFieldConfig(group = "Profile", accessible = Accessible.MANDATORY, maxLength = 32, uniqueKey = "name")
	private EeHospitalType hospitalType;
	@CCFieldConfig(group = "Profile", label = "Owner Type", accessible = Accessible.MANDATORY, optionSource = "model.obj.emergency.EeEnum$OwnerType", optionReflectKey = "ownerType", visible = false)
	private Integer ownerTypeIndex;
	@Column(length = 12)
	@CCFieldConfig(group = "Profile", maxLength = 12, visible2 = false)
	private String ownerType;
	// ----------------------------------------------------------------------------------------------
	@Lob
	@Column(length = Integer.MAX_VALUE)
	@CCFieldConfig(group = "Profile", type = Type.THUMB_FILE, optionReflectKey = "photoName", visible = false)
	private byte[] photo;
	@Column(length = 255)
	@CCFieldConfig(group = "Profile", visible = false, visible2 = false)
	private String photoName;
	// ----------------------------------------------------------------------------------------------
	@Column(length = 255)
	@CCFieldConfig(group = "Address", maxLength = Short.MAX_VALUE)
	private String address;
	@ManyToOne
	@CCFieldConfig(group = "Address", tooltiptext = "Kabupaten", accessible = Accessible.MANDATORY, maxLength = 48, uniqueKey = "name")
	private UtilRegency regency;
	@ManyToOne
	@CCFieldConfig(group = "Address", tooltiptext = "Propinsi", accessible = Accessible.MANDATORY, maxLength = 48, uniqueKey = "name")
	private UtilProvince province;
	@Column(length = 5)
	@CCFieldConfig(group = "Address", minLength = 5, type = Type.NUMBER_ONLY, visible = false)
	private String zipCode;
	// ----------------------------------------------------------------------------------------------
	@Column(length = 16)
	@CCFieldConfig(group = "Contact", type = Type.PHONE_NUMBER, visible = false)
	private String phone;
	@Column(length = 16)
	@CCFieldConfig(group = "Contact", type = Type.PHONE_NUMBER, visible = false)
	private String fax;
	@Column(length = 32)
	@CCFieldConfig(group = "Contact", type = Type.EMAIL, visible = false)
	private String email;
	@Column(length = 48)
	@CCFieldConfig(group = "Contact", visible = false)
	private String website;
	// ----------------------------------------------------------------------------------------------
	@CCFieldConfig(accessible = Accessible.READONLY, type = Type.NUMERIC, visible = false, visible2 = false)
	private Integer totalRoom = 0;
	@CCFieldConfig(accessible = Accessible.READONLY, type = Type.NUMERIC, visible = false, visible2 = false)
	private Integer totalDoctor = 0;
	@CCFieldConfig(accessible = Accessible.READONLY, type = Type.NUMERIC, visible = false, visible2 = false)
	private Integer totalRating = 0;
	@CCFieldConfig(accessible = Accessible.READONLY, type = Type.NUMERIC, visible = false, visible2 = false)
	private Integer totalRater = 0;
	// ----------------------------------------------------------------------------------------------
	@CCFieldConfig(maxLength = 4, visible = false)
	private Boolean partnership = false;
	@ManyToOne
	@CCFieldConfig(maxLength = 32, uniqueKey = "username", visible = false, visible2 = false)
	private UtilUser user;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public EeHospitalType getHospitalType() {
		return hospitalType;
	}

	public void setHospitalType(EeHospitalType hospitalType) {
		this.hospitalType = hospitalType;
	}

	public Integer getOwnerTypeIndex() {
		return ownerTypeIndex;
	}

	public void setOwnerTypeIndex(Integer ownerTypeIndex) {
		this.ownerTypeIndex = ownerTypeIndex;
	}

	public String getOwnerType() {
		return ownerType;
	}

	public void setOwnerType(String ownerType) {
		this.ownerType = ownerType;
	}

	public byte[] getPhoto() {
		return photo;
	}

	public void setPhoto(byte[] photo) {
		this.photo = photo;
	}

	public String getPhotoName() {
		return photoName;
	}

	public void setPhotoName(String photoName) {
		this.photoName = photoName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public UtilRegency getRegency() {
		return regency;
	}

	public void setRegency(UtilRegency regency) {
		this.regency = regency;
	}

	public UtilProvince getProvince() {
		return province;
	}

	public void setProvince(UtilProvince province) {
		this.province = province;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getWebsite() {
		return website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	public Integer getTotalRoom() {
		return totalRoom;
	}

	public void setTotalRoom(Integer totalRoom) {
		this.totalRoom = totalRoom;
	}

	public Integer getTotalDoctor() {
		return totalDoctor;
	}

	public void setTotalDoctor(Integer totalDoctor) {
		this.totalDoctor = totalDoctor;
	}

	public Integer getTotalRating() {
		return totalRating;
	}

	public void setTotalRating(Integer totalRating) {
		this.totalRating = totalRating;
	}

	public Integer getTotalRater() {
		return totalRater;
	}

	public void setTotalRater(Integer totalRater) {
		this.totalRater = totalRater;
	}

	public Boolean getPartnership() {
		return partnership;
	}

	public boolean isPartnership() {
		return getBoolean(partnership);
	}

	public void setPartnership(Boolean partnership) {
		this.partnership = partnership;
	}

	public UtilUser getUser() {
		return user;
	}

	public void setUser(UtilUser user) {
		this.user = user;
	}
}