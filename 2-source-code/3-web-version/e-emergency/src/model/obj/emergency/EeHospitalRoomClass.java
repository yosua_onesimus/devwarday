package model.obj.emergency;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import cococare.common.CCFieldConfig;
import cococare.common.CCTypeConfig;
import cococare.common.CCFieldConfig.Accessible;
import cococare.common.CCFieldConfig.CompareRule;
import cococare.common.CCFieldConfig.Type;
import cococare.database.CCEntity;

@Entity
@Table(name = "ee_hospital_room_classes")
@CCTypeConfig(label = "Hospital Room Class", uniqueKey = "@hospital.name-@roomClass.name", parameter = true)
public class EeHospitalRoomClass extends CCEntity {
	@ManyToOne
	@CCFieldConfig(accessible = Accessible.MANDATORY, maxLength = 64, uniqueKey = "name", visible = false)
	private EeHospital hospital;
	@ManyToOne
	@CCFieldConfig(accessible = Accessible.MANDATORY, maxLength = 32, uniqueKey = "name")
	private EeRoomClass roomClass;
	@CCFieldConfig(accessible = Accessible.MANDATORY, type = Type.NUMERIC)
	private Integer totalBed;
	@CCFieldConfig(label = "Rate", accessible = Accessible.MANDATORY, type = Type.NUMBER_FORMAT)
	private Double price;
	@Lob
	@Column(length = Integer.MAX_VALUE)
	@CCFieldConfig(type = Type.THUMB_FILE, optionReflectKey = "photoName", visible = false)
	private byte[] photo;
	@Column(length = 255)
	@CCFieldConfig(visible = false, visible2 = false)
	private String photoName;
	@CCFieldConfig(accessible = Accessible.MANDATORY, type = Type.NUMERIC)
	private Integer availableRoom;
	@CCFieldConfig(accessible = Accessible.MANDATORY, type = Type.NUMERIC, compareRule = CompareRule.GREATER_OR_EQUAL_THAN, compareWith = "txtAvailableRoom")
	private Integer totalRoom;

	public EeHospital getHospital() {
		return hospital;
	}

	public void setHospital(EeHospital hospital) {
		this.hospital = hospital;
	}

	public EeRoomClass getRoomClass() {
		return roomClass;
	}

	public void setRoomClass(EeRoomClass roomClass) {
		this.roomClass = roomClass;
	}

	public Integer getTotalBed() {
		return totalBed;
	}

	public void setTotalBed(Integer totalBed) {
		this.totalBed = totalBed;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public byte[] getPhoto() {
		return photo;
	}

	public void setPhoto(byte[] photo) {
		this.photo = photo;
	}

	public String getPhotoName() {
		return photoName;
	}

	public void setPhotoName(String photoName) {
		this.photoName = photoName;
	}

	public Integer getAvailableRoom() {
		return availableRoom;
	}

	public void setAvailableRoom(Integer availableRoom) {
		this.availableRoom = availableRoom;
	}

	public Integer getTotalRoom() {
		return totalRoom;
	}

	public void setTotalRoom(Integer totalRoom) {
		this.totalRoom = totalRoom;
	}
}