package model.obj.emergency;

import java.util.ArrayList;
import java.util.List;

import model.obj.emergency.EeEnum.Searching;
import cococare.common.CCFieldConfig;
import cococare.framework.model.obj.util.UtilProvince;
import cococare.framework.model.obj.util.UtilRegency;

public class EeSearch {

	private Searching searching = Searching.HOSPITAL;
	@CCFieldConfig(maxLength = 64, requestFocus = true)
	private String name;
	@CCFieldConfig(maxLength = 32, uniqueKey = "name")
	private EeHospitalType hospitalType;
	@CCFieldConfig(label = "Owner Type", optionSource = "model.obj.emergency.EeEnum$OwnerType")
	private Integer ownerTypeIndex = 0;
	@CCFieldConfig(maxLength = 32, uniqueKey = "name")
	private EeSpecialist specialist;
	@CCFieldConfig(tooltiptext = "Kabupaten", maxLength = 48, uniqueKey = "name")
	private UtilRegency regency;
	@CCFieldConfig(tooltiptext = "Propinsi", maxLength = 48, uniqueKey = "name")
	private UtilProvince province;
	private List<EeRoomClass> roomClasses = new ArrayList();
	private EeHospital hospital;
	private EeDoctor doctor;

	public void setSearching(Searching searching) {
		this.searching = searching;
	}

	public Searching getSearching() {
		return searching;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public EeHospitalType getHospitalType() {
		return hospitalType;
	}

	public void setHospitalType(EeHospitalType hospitalType) {
		this.hospitalType = hospitalType;
	}

	public Integer getOwnerTypeIndex() {
		return ownerTypeIndex;
	}

	public void setOwnerTypeIndex(Integer ownerTypeIndex) {
		this.ownerTypeIndex = ownerTypeIndex;
	}

	public EeSpecialist getSpecialist() {
		return specialist;
	}

	public void setSpecialist(EeSpecialist specialist) {
		this.specialist = specialist;
	}

	public UtilRegency getRegency() {
		return regency;
	}

	public void setRegency(UtilRegency regency) {
		this.regency = regency;
	}

	public UtilProvince getProvince() {
		return province;
	}

	public void setProvince(UtilProvince province) {
		this.province = province;
	}

	public List<EeRoomClass> getRoomClasses() {
		return roomClasses;
	}

	public void setRoomClasses(List<EeRoomClass> roomClasses) {
		this.roomClasses = roomClasses;
	}

	public EeHospital getHospital() {
		return hospital;
	}

	public void setHospital(EeHospital hospital) {
		this.hospital = hospital;
	}

	public EeDoctor getDoctor() {
		return doctor;
	}

	public void setDoctor(EeDoctor doctor) {
		this.doctor = doctor;
	}
}