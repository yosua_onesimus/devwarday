package model.obj.emergency;

import cococare.database.CCHibernateFilter;

public class EeFilter {
	public static abstract class isHospitalType extends CCHibernateFilter {
		@Override
		public String getFieldName() {
			return "hospitalType";
		}
	};

	public static abstract class isOwnerTypeIndex extends CCHibernateFilter {
		@Override
		public String getFieldName() {
			return "ownerTypeIndex";
		}
	};

	public static abstract class isSpecialist extends CCHibernateFilter {
		@Override
		public String getFieldName() {
			return "specialist";
		}
	};

	public static abstract class isRegency extends CCHibernateFilter {
		@Override
		public String getFieldName() {
			return "regency";
		}
	};

	public static abstract class isHospital extends CCHibernateFilter {
		@Override
		public String getFieldName() {
			return "hospital";
		}
	};

	public static abstract class isDoctor extends CCHibernateFilter {
		@Override
		public String getFieldName() {
			return "doctor";
		}
	};
}