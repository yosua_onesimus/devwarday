package model.obj.emergency;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import cococare.common.CCFieldConfig;
import cococare.common.CCTypeConfig;
import cococare.common.CCFieldConfig.Accessible;
import cococare.database.CCEntity;

@Entity
@Table(name = "ee_hospital_partnerships")
@CCTypeConfig(label = "Hospital Partnership", uniqueKey = "name", parameter = true)
public class EeHospitalPartnership extends CCEntity {
	@ManyToOne
	@CCFieldConfig(accessible = Accessible.MANDATORY, maxLength = 64, uniqueKey = "name", visible = false)
	private EeHospital hospital;
	@Column(length = 64)
	@CCFieldConfig(accessible = Accessible.MANDATORY, requestFocus = true)
	private String name;

	public EeHospital getHospital() {
		return hospital;
	}

	public void setHospital(EeHospital hospital) {
		this.hospital = hospital;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}