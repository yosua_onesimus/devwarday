package model.mdl.emergency;

import java.util.Arrays;
import java.util.List;

import model.obj.emergency.EeDoctor;
import model.obj.emergency.EeDoctorRating;
import model.obj.emergency.EeHospital;
import model.obj.emergency.EeHospitalGallery;
import model.obj.emergency.EeHospitalPartnership;
import model.obj.emergency.EeHospitalRating;
import model.obj.emergency.EeHospitalRoomClass;
import model.obj.emergency.EeHospitalType;
import model.obj.emergency.EeRoomClass;
import model.obj.emergency.EeSchedule;
import model.obj.emergency.EeSpecialist;
import cococare.database.CCHibernateModule;

public class EmergencyModule extends CCHibernateModule {

	public static EmergencyModule INSTANCE = new EmergencyModule();

	@Override
	protected List<Class> _getAnnotatedClasses() {
		return (List) Arrays.asList(EeSpecialist.class, EeDoctor.class, EeDoctorRating.class, EeHospitalType.class, EeHospital.class, EeRoomClass.class, EeHospitalRoomClass.class, EeSchedule.class, EeHospitalPartnership.class, EeHospitalGallery.class, EeHospitalRating.class);
	}
}