package model.mdl.emergency;

import java.util.List;

import cococare.database.CCHibernate;
import cococare.database.CCHibernateDao;
import cococare.database.CCHibernateFilter;

public abstract class EmergencyDao extends CCHibernateDao {

	@Override
	protected CCHibernate getCCHibernate() {
		return EmergencyModule.INSTANCE.getCCHibernate();
	}

	@Override
	protected List<CCHibernateFilter> getFilters() {
		return EmergencyModule.INSTANCE.getFilters();
	}
}