package model.mdl.emergency;

import static cococare.common.CCConfig.APPL_UTIL_INCLUDED_PERSON_ENTITIES;
import static cococare.common.CCConfig.TBL_COLUMN_MAX_WIDTH;
import static cococare.common.CCLogic.isNullOrEmpty;
import static cococare.database.CCLoginInfo.INSTANCE_hasLogged;
import static cococare.zk.CCSession.getAttribute;
import static cococare.zk.CCSession.setAttribute;
import static model.mdl.emergency.EmergencyLanguage.Admin_Archive;
import static model.mdl.emergency.EmergencyLanguage.Doctor;
import static model.mdl.emergency.EmergencyLanguage.Doctor_Archive;
import static model.mdl.emergency.EmergencyLanguage.Ee;
import static model.mdl.emergency.EmergencyLanguage.Hospital;
import static model.mdl.emergency.EmergencyLanguage.Hospital_Archive;
import static model.mdl.emergency.EmergencyLanguage.My_Profile;

import java.util.Arrays;

import model.bo.emergency.EeConfigBo;
import model.obj.emergency.EeConfig;
import model.obj.emergency.EeSearch;
import cococare.common.CCLanguage;
import cococare.framework.common.CFApplUae;
import cococare.framework.model.bo.util.UtilConfigBo;
import cococare.framework.zk.CFZkMain;
import cococare.framework.zk.CFZkMap;
import cococare.zk.CCZk;
import controller.zul.emergency.ZulDoctorListCtrl;
import controller.zul.emergency.ZulDoctorProfileCtrl;
import controller.zul.emergency.ZulHospitalListCtrl;
import controller.zul.emergency.ZulHospitalProfileCtrl;
import controller.zul.preLogin.ZulHomeCtrl;

public class EmergencyMain extends CFZkMain {
	@Override
	protected void _loadInternalSetting() {
		APPL_CODE = "e-emergency-zk";
		APPL_LOGO = "/resource/img-e-mergency-2.png";
		APPL_NAME = "e-mergency: Medical Regions's Encyclopedia";
		APPL_UTIL_INCLUDED_PERSON_ENTITIES = true;
		TBL_COLUMN_MAX_WIDTH = 150;
		super._loadInternalSetting();
	}

	@Override
	protected void _loadExternalSetting() {
		CCLanguage.init(false, EmergencyLanguage.class);
		super._loadExternalSetting();
	}

	@Override
	protected void _initDatabaseEntity() {
		super._initDatabaseEntity();
		EmergencyModule.INSTANCE.init(HIBERNATE);
	}

	@Override
	protected boolean _initInitialData() {
		UtilConfigBo configBo = new UtilConfigBo();
		confAppl = configBo.loadConfAppl();
		confAppl.setUtilAdditionalSettingClass(Arrays.asList(//
				EeConfig.class.getName()));
		return super._initInitialData()//
				&& configBo.saveConf(confAppl);
	}

	@Override
	protected void _initInitialUaeBody(CFApplUae uae) {
		uae.reg(Ee, Hospital, ZulHospitalListCtrl.class);
		uae.reg(Ee, Doctor, ZulDoctorListCtrl.class);
	}

	@Override
	protected void _applyUserConfigUaeBody(CFApplUae uae) {
		EeConfigBo configBo = new EeConfigBo();
		if (configBo.isGroupHospital()) {
			uae.addMenuRoot(ZulHospitalProfileCtrl.class);
			uae.addMenuParent(Hospital_Archive, "/img/icon-menu-archive.png", null);
			uae.addMenuChild(My_Profile, "/img/icon-menu-hospital.png", ZulHospitalProfileCtrl.class);
		} else if (configBo.isGroupDoctor()) {
			uae.addMenuRoot(ZulDoctorProfileCtrl.class);
			uae.addMenuParent(Doctor_Archive, "/img/icon-menu-archive.png", null);
			uae.addMenuChild(My_Profile, "/img/icon-menu-doctor.png", ZulDoctorProfileCtrl.class);
		} else {
			uae.addMenuParent(Admin_Archive, "/img/icon-menu-archive.png", null);
			uae.addMenuChild(Hospital, "/img/icon-menu-hospital.png", ZulHospitalListCtrl.class);
			uae.addMenuChild(Doctor, "/img/icon-menu-doctor.png", ZulDoctorListCtrl.class);
		}
	}

	@Override
	public void updateNonContent(Object object) {
		super.updateNonContent(object);
		boolean hasLogged = INSTANCE_hasLogged();
		CCZk.getA(CFZkMap.getMainScreen(), "aLogIn").setVisible(!hasLogged);
		CCZk.getA(CFZkMap.getMainScreen(), "aLogOut").setVisible(hasLogged);
	}

	@Override
	protected boolean _showLoginScreen() {
		// initial search session
		if (isNullOrEmpty(getAttribute(EeSearch.class))) {
			setAttribute(new EeSearch());
		}
		// initial BookmarkEvent to enable backward action / forward action
		CCZk.initBookmarkEvent(CFZkMap.getMainScreen(), ZulHomeCtrl.class, "init");
		return true;
	}
}