package custom.component;

import static cococare.common.CCClass.getValue;
import static cococare.common.CCFormat.getString4View;
import static cococare.common.CCFormat.mailto;
import static cococare.common.CCFormat.parseInt;
import static cococare.common.CCLogic.coalesce;
import static cococare.common.CCMath.calculate;
import static cococare.zk.CCSession.getAttribute;
import static cococare.zk.CCZk.addListener;
import static cococare.zk.CCZk.setDimension;
import static cococare.zk.CCZk.setHeight;
import static cococare.zk.CCZk.setHflex;
import static cococare.zk.CCZk.setImageContent;
import static cococare.zk.CCZk.setSclass;
import static cococare.zk.CCZk.setStyle;
import static cococare.zk.CCZk.setWidth;
import model.bo.emergency.EeConfigBo;
import model.obj.emergency.EeConfig;
import model.obj.emergency.EeDoctor;
import model.obj.emergency.EeHospital;
import model.obj.emergency.EeSearch;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zul.A;
import org.zkoss.zul.Hbox;
import org.zkoss.zul.Image;
import org.zkoss.zul.Label;
import org.zkoss.zul.Space;
import org.zkoss.zul.Vbox;

import cococare.zk.CCRating;
import controller.zul.preLogin.ZulDoctorProfileCtrl;
import controller.zul.preLogin.ZulHospitalProfileCtrl;

public class CustomView {
	private static String getString(Object object, String fieldName) {
		return coalesce(getString4View(getValue(object, fieldName)), "~");
	}

	private static Space newSpaceH5() {
		Space space = new Space();
		setHeight(space, 5);
		return space;
	}

	private static Space newSpaceW5() {
		Space space = new Space();
		setWidth(space, 5);
		return space;
	}

	private static A newAEmail(String label) {
		A a = new A(label);
		a.setHref(mailto(label, null, null, null, null));
		return a;
	}

	private static A newAWebsite(String label) {
		A a = new A(label);
		a.setHref("http://" + label.replaceFirst("http://", ""));
		a.setTarget("_blank");
		return a;
	}

	/**
	 * Create a new component from hospital object or doctor object.
	 * 
	 * @param object
	 *            Hospital or Doctor.
	 * @return the new component.
	 */
	public static Component newComponent(final Object object) {
		Hbox hbox = new Hbox();
		{
			setHflex(hbox, 1);
			newSpaceW5().setParent(hbox);
			Vbox vbox = new Vbox();
			{
				setHflex(vbox, 1);
				setStyle(vbox, "background:#EFFFEF;");
				newSpaceH5().setParent(vbox);
				Hbox hbox2 = new Hbox();
				{
					setHflex(hbox2, 1);
					new Space().setParent(hbox2);
					Image image = new Image();
					setDimension(image, 100, 100);
					EeConfig config = new EeConfigBo().getConfig();
					setImageContent(image, (byte[]) coalesce(//
							getValue(object, "photo"),// 
							object instanceof EeHospital// 
							? config.getPhotoHospital() //
									: config.getPhotoDoctor()));
					image.setParent(hbox2);
					new Space().setParent(hbox2);
					Vbox vbox2 = new Vbox();
					{
						setHeight(vbox2, 100);
						setHflex(vbox2, 1);
						Hbox hbox3 = new Hbox();
						{
							setHflex(hbox3, 1);
							Vbox vbox3 = new Vbox();
							{
								setHflex(vbox3, 1);
								new Label(getString(object, "name")).setParent(vbox3);
								new Label(getString(object, "address")).setParent(vbox3);
								new Label(getString(object, "regency.name")).setParent(vbox3);
								new Label(getString(object, "province.name")).setParent(vbox3);
							}
							vbox3.setParent(hbox3);
							Vbox vbox4 = new Vbox();
							{
								setHflex(vbox4, 1);
								CCRating rating = new CCRating(new Hbox());
								rating.setRating(parseInt(calculate("totalRating/totalRater", object)));
								setSclass(rating.getBox(), "starRating");
								rating.getBox().setTooltiptext(getString(object, "@totalRating/@totalRater"));
								rating.getBox().setParent(vbox4);
								new Label(getString(object, "phone")).setParent(vbox4);
								newAEmail(getString(object, "email")).setParent(vbox4);
								newAWebsite(getString(object, "website")).setParent(vbox4);
							}
							vbox4.setParent(hbox3);
						}
						hbox3.setParent(vbox2);
						A a = new A("More...");
						addListener(a, new EventListener() {
							public void onEvent(Event event) throws Exception {
								EeSearch search = getAttribute(EeSearch.class);
								if (object instanceof EeHospital) {
									search.setHospital((EeHospital) object);
									new ZulHospitalProfileCtrl().init();
								} else if (object instanceof EeDoctor) {
									search.setDoctor((EeDoctor) object);
									new ZulDoctorProfileCtrl().init();
								}
							}
						});
						a.setParent(vbox2);
					}
					vbox2.setParent(hbox2);
					new Space().setParent(hbox2);
				}
				hbox2.setParent(vbox);
				newSpaceH5().setParent(vbox);
			}
			vbox.setParent(hbox);
			newSpaceW5().setParent(hbox);
		}
		return hbox;
	}
}