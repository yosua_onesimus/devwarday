package controller.pseudo.emergency;

import static cococare.framework.model.obj.util.UtilFilter.isUserGroupNotRoot;
import cococare.framework.zk.controller.zul.util.ZulApplicationSettingCtrl;
import cococare.zk.CCBandbox;

public class EeConfigCtrl extends ZulApplicationSettingCtrl {
	private CCBandbox txtGroupDoctor;
	private CCBandbox txtGroupHospital;

	@Override
	protected void _initEditor() {
		super._initEditor();
		txtGroupDoctor.getTable().setHqlFilters(isUserGroupNotRoot);
		txtGroupHospital.getTable().setHqlFilters(isUserGroupNotRoot);
	}
}