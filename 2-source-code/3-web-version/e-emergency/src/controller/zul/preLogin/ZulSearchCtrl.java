package controller.zul.preLogin;

import static cococare.common.CCLogic.isNotNull;
import static cococare.zk.CCSession.getAttribute;
import static cococare.zk.CCZk.addListener;
import static cococare.zk.CCZk.initSpecialComponent;
import model.obj.emergency.EeDoctor;
import model.obj.emergency.EeHospital;
import model.obj.emergency.EeRoomClass;
import model.obj.emergency.EeSearch;
import model.obj.emergency.EeEnum.Searching;
import model.obj.emergency.EeFilter.isHospitalType;
import model.obj.emergency.EeFilter.isOwnerTypeIndex;
import model.obj.emergency.EeFilter.isRegency;
import model.obj.emergency.EeFilter.isSpecialist;

import org.zkoss.zul.Button;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Grid;
import org.zkoss.zul.Paging;
import org.zkoss.zul.Radio;
import org.zkoss.zul.Textbox;

import cococare.common.CCCustomField;
import cococare.framework.model.obj.util.UtilFilter.isProvince;
import cococare.framework.zk.CFZkCtrl;
import cococare.zk.CCCombobox;
import cococare.zk.CCEditor;
import cococare.zk.CCOptionbox;
import cococare.zk.CCTable;
import cococare.zk.CCZk;
import custom.component.CustomView;

public class ZulSearchCtrl extends CFZkCtrl {
	private CCEditor edtSearch;
	private CCOptionbox optRoomClass;

	private CCTable tblResult;

	private Radio rdoHospital;
	private Radio rdoDoctor;
	private Textbox txtName;
	private Grid grdSearch;
	private CCCombobox txtHospitalType;
	private Combobox txtOwnerTypeIndex;
	private CCCombobox txtSpecialist;
	private CCCombobox txtRegency;
	private CCCombobox txtProvince;
	private Grid grdRoomClass;
	private Button btnSearch;

	private Grid grdResult;
	private Paging pgnResult;

	@Override
	protected Class _getEntity() {
		if (isNotNull(rdoHospital)) {
			return rdoHospital.isSelected() ? EeHospital.class : EeDoctor.class;
		} else {
			return null;
		}
	}

	@Override
	protected BaseFunction _getBaseFunction() {
		// to enable backward action / forward action
		CCZk.setCurrentDesktopBookmark();
		return BaseFunction.LIST_FUNCTION;
	}

	@Override
	protected void _initComponent() {
		super._initComponent();

		edtSearch = new CCEditor(grdSearch, EeSearch.class);
		// to init special component coz _getBaseFunction is LIST_FUNCTION
		initSpecialComponent(getContainer(), this);
		optRoomClass = new CCOptionbox(grdRoomClass, false, EeRoomClass.class, "name");
		optRoomClass.setColumnCount(1);
		optRoomClass.autoList();
	}

	@Override
	protected void _initTable() {
		tblResult = new CCTable(grdResult, _getEntity());
		tblResult.setHeadersVisible(false);
		tblResult.setNaviElements(pgnResult, txtName);
		tblResult.setHqlFilters(new isHospitalType() {
			@Override
			public Object getFieldValue() {
				return txtHospitalType.getSelectedObject();
			}
		}, new isOwnerTypeIndex() {
			@Override
			public Object getFieldValue() {
				if (txtOwnerTypeIndex.getSelectedIndex() == 0) {
					return null;
				} else {
					return txtOwnerTypeIndex.getSelectedIndex() - 1;
				}
			}
		}, new isSpecialist() {
			@Override
			public Object getFieldValue() {
				return txtSpecialist.getSelectedObject();
			}
		}, new isRegency() {
			@Override
			public Object getFieldValue() {
				return txtRegency.getSelectedObject();
			}
		}, new isProvince() {
			@Override
			public Object getFieldValue() {
				return txtProvince.getSelectedObject();
			}
		});
		tblResult.setHqlMaxResults(10);
	}

	@Override
	protected void _initListener() {
		super._initListener();

		addListener(btnSearch, elSearch);
	}

	@Override
	protected void _doSearch() {
		EeSearch search = getAttribute(EeSearch.class);
		search.setSearching(rdoHospital.isChecked() ? Searching.HOSPITAL : Searching.DOCTOR);
		edtSearch.getValueFromEditor();
		search.setRoomClasses(optRoomClass.getSelectedItems());

		super._doSearch();
	}

	@Override
	protected void _doUpdateComponent() {
		EeSearch search = getAttribute(EeSearch.class);
		rdoHospital.setSelected(Searching.HOSPITAL.equals(search.getSearching()));
		rdoDoctor.setSelected(Searching.DOCTOR.equals(search.getSearching()));
		edtSearch.setValueToEditor(search);
		optRoomClass.setSelectedItem(true, search.getRoomClasses().toArray());

		super._doUpdateComponent();
	}

	@Override
	public void doUpdateTable() {
		tblResult.setEntity(_getEntity());
		tblResult.initFields();
		tblResult.addField(0, new CCCustomField() {
			@Override
			public Object getCustomView(Object object) {
				return CustomView.newComponent(object);
			}
		});
		tblResult.search();
	}
}