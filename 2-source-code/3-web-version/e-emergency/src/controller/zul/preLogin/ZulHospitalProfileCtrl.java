package controller.zul.preLogin;

import static cococare.common.CCClass.getValue;
import static cococare.common.CCFormat.getString4View;
import static cococare.common.CCFormat.parseInt;
import static cococare.common.CCLogic.coalesce;
import static cococare.common.CCMath.calculate;
import static cococare.zk.CCSession.getAttribute;
import static cococare.zk.CCZk.addListener;
import static cococare.zk.CCZk.initComponent;
import static cococare.zk.CCZk.setImageContent;
import model.bo.emergency.EeConfigBo;
import model.obj.emergency.EeHospital;
import model.obj.emergency.EeHospitalPartnership;
import model.obj.emergency.EeHospitalRoomClass;
import model.obj.emergency.EeRoomClass;
import model.obj.emergency.EeSchedule;
import model.obj.emergency.EeSearch;
import model.obj.emergency.EeFilter.isHospital;

import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zul.A;
import org.zkoss.zul.Button;
import org.zkoss.zul.Grid;
import org.zkoss.zul.Hbox;
import org.zkoss.zul.Image;
import org.zkoss.zul.Label;
import org.zkoss.zul.Vbox;
import org.zkoss.zul.Window;

import cococare.common.CCCustomField;
import cococare.database.CCHibernateFilter;
import cococare.framework.zk.CFZkCtrl;
import cococare.zk.CCEditor;
import cococare.zk.CCOptionbox;
import cococare.zk.CCRating;
import cococare.zk.CCTable;
import cococare.zk.CCZk;
import cococare.zk.CCTable.DynamicStyle;

public class ZulHospitalProfileCtrl extends CFZkCtrl {
	private CCEditor edtSearch;
	private CCOptionbox optRoomClass;

	private CCEditor edtHospital;

	private CCTable tblHospitalRoomClass;

	private CCTable tblSchedule;

	private CCTable tblHospitalPartnership;

	private Window winSearch;
	private Grid grdRoomClass;
	private Button btnSearch;

	private Window winHospital;
	private Image imgPhoto;
	private Hbox boxRating;

	private Grid grdHospitalRoomClass;

	private Grid grdSchedule;

	private Grid grdHospitalPartnership;

	@Override
	protected Class _getEntity() {
		return EeHospital.class;
	}

	@Override
	protected BaseFunction _getBaseFunction() {
		// to enable backward action / forward action
		CCZk.setCurrentDesktopBookmark();
		return BaseFunction.LIST_FUNCTION;
	}

	@Override
	protected void _initComponent() {
		super._initComponent();

		edtSearch = new CCEditor(winSearch, EeSearch.class);
		initComponent(winSearch, this, null);
		optRoomClass = new CCOptionbox(grdRoomClass, false, EeRoomClass.class, "name");
		optRoomClass.setColumnCount(1);
		optRoomClass.autoList();

		edtHospital = new CCEditor(winHospital, EeHospital.class);
		initComponent(winHospital, this, null);

		CCHibernateFilter byHospital = new isHospital() {
			@Override
			public Object getFieldValue() {
				return edtHospital.getValueFromEditor();
			}
		};

		tblHospitalRoomClass = new CCTable(grdHospitalRoomClass, EeHospitalRoomClass.class);
		tblHospitalRoomClass.setVisibleField(false, "roomClass", "availableRoom", "totalRoom");
		tblHospitalRoomClass.addField(0, new CCCustomField() {
			@Override
			public String getLabel() {
				return "Room Class";
			}

			@Override
			public Object getCustomView(Object object) {
				Vbox vbox = new Vbox();
				EeHospitalRoomClass hospitalRoomClass = (EeHospitalRoomClass) object;
				Label label = new Label(hospitalRoomClass.getRoomClass().getName());
				label.setParent(vbox);
				Image image = new Image();
				CCZk.setImageContent(image, hospitalRoomClass.getPhoto());
				image.setParent(vbox);
				return vbox;
			}
		});

		tblHospitalRoomClass.addField(3, new CCCustomField() {
			@Override
			public String getLabel() {
				return "Status";
			}

			@Override
			public Object getCustomView(Object object) {
				EeHospitalRoomClass hospitalRoomClass = (EeHospitalRoomClass) object;
				String status;
				int roomInUse = parseInt(calculate("totalRoom-availableRoom", hospitalRoomClass));
				if (roomInUse == hospitalRoomClass.getTotalRoom()) {
					status = "Full";
				} else {
					status = "Available";
				}
				return status + " (" + roomInUse + " of " + hospitalRoomClass.getTotalRoom() + ")";
			}
		});
		tblHospitalRoomClass.setHorizontalAlignmentColumn("center", 0, 1, 2, 3);
		tblHospitalRoomClass.setColumnWidth(100, 30, 150, 70);
		tblHospitalRoomClass.setDynamicStyles(new DynamicStyle() {
			private EeHospitalRoomClass hospitalRoomClass;

			@Override
			public boolean isApply(int column, int row, Object object, Object columnValue) {
				hospitalRoomClass = (EeHospitalRoomClass) object;
				return column == 3;
			}

			@Override
			public String getStyle() {
				if (hospitalRoomClass.getAvailableRoom() == 0) {
					return "color:red;";
				} else {
					return "color:green;";
				}
			}
		});
		tblHospitalRoomClass.setHqlFilters(byHospital);

		tblSchedule = new CCTable(grdSchedule, EeSchedule.class);
		tblSchedule.setVisibleField(false, "hospital", "doctor");
		tblSchedule.addField(0, new CCCustomField() {
			@Override
			public String getLabel() {
				return "Doctor";
			}

			@Override
			public Object getCustomView(Object object) {
				final EeSchedule schedule = (EeSchedule) object;
				A a = new A(schedule.getDoctor().getName());
				CCZk.addListener(a, new EventListener() {
					public void onEvent(Event event) throws Exception {
						EeSearch search = getAttribute(EeSearch.class);
						search.setDoctor(schedule.getDoctor());
						new ZulDoctorProfileCtrl().init();
					}
				});
				return a;
			}
		});
		tblSchedule.setGroupBy("doctor.specialist");
		tblSchedule.setHqlFilters(byHospital);

		tblHospitalPartnership = new CCTable(grdHospitalPartnership, EeHospitalPartnership.class);
		tblHospitalPartnership.setHqlFilters(byHospital);
		tblHospitalPartnership.setColumnLabel(0, "Insurance Partner");
	}

	@Override
	protected void _initListener() {
		super._initListener();

		addListener(btnSearch, elSearch);
	}

	@Override
	protected void _doSearch() {
		EeSearch search = getAttribute(EeSearch.class);
		edtSearch.getValueFromEditor();
		search.setRoomClasses(optRoomClass.getSelectedItems());

		new ZulSearchCtrl().init();
	}

	@Override
	protected void _doUpdateComponent() {
		EeSearch search = getAttribute(EeSearch.class);
		edtSearch.setValueToEditor(search);
		optRoomClass.setSelectedItem(true, search.getRoomClasses().toArray());

		edtHospital.setValueToEditor(search.getHospital());
		setImageContent(imgPhoto, coalesce(search.getHospital().getPhoto(), new EeConfigBo().getConfig().getPhotoHospital()));

		CCRating rating = new CCRating(boxRating);
		rating.setRating(parseInt(calculate("totalRating/totalRater", search.getHospital())));
		rating.getBox().setTooltiptext(getString4View(getValue(search.getHospital(), "@totalRating/@totalRater")));

		tblHospitalRoomClass.search();

		tblSchedule.search();

		tblHospitalPartnership.search();

		super._doUpdateComponent();
	}
}