package controller.zul.preLogin;

import static cococare.common.CCClass.getValue;
import static cococare.common.CCFormat.getString4View;
import static cococare.common.CCFormat.parseInt;
import static cococare.common.CCLogic.coalesce;
import static cococare.common.CCMath.calculate;
import static cococare.zk.CCSession.getAttribute;
import static cococare.zk.CCZk.addListener;
import static cococare.zk.CCZk.initComponent;
import static cococare.zk.CCZk.setImageContent;
import model.bo.emergency.EeConfigBo;
import model.obj.emergency.EeDoctor;
import model.obj.emergency.EeSchedule;
import model.obj.emergency.EeSearch;
import model.obj.emergency.EeFilter.isDoctor;

import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zul.A;
import org.zkoss.zul.Button;
import org.zkoss.zul.Grid;
import org.zkoss.zul.Hbox;
import org.zkoss.zul.Image;
import org.zkoss.zul.Window;

import cococare.common.CCCustomField;
import cococare.database.CCHibernateFilter;
import cococare.framework.zk.CFZkCtrl;
import cococare.zk.CCEditor;
import cococare.zk.CCRating;
import cococare.zk.CCTable;
import cococare.zk.CCZk;

public class ZulDoctorProfileCtrl extends CFZkCtrl {
	private CCEditor edtSearch;

	private CCEditor edtDoctor;

	private CCTable tblSchedule;

	private Window winSearch;
	private Button btnSearch;

	private Window winDoctor;
	private Image imgPhoto;
	private Hbox boxRating;

	private Grid grdSchedule;

	@Override
	protected Class _getEntity() {
		return EeDoctor.class;
	}

	@Override
	protected BaseFunction _getBaseFunction() {
		// to enable backward action / forward action
		CCZk.setCurrentDesktopBookmark();
		return BaseFunction.LIST_FUNCTION;
	}

	@Override
	protected void _initComponent() {
		super._initComponent();

		edtSearch = new CCEditor(winSearch, EeSearch.class);
		initComponent(winSearch, this, null);

		edtDoctor = new CCEditor(winDoctor, EeDoctor.class);
		initComponent(winDoctor, this, null);

		CCHibernateFilter byDoctor = new isDoctor() {
			@Override
			public Object getFieldValue() {
				return edtDoctor.getValueFromEditor();
			}
		};

		tblSchedule = new CCTable(grdSchedule, EeSchedule.class);
		tblSchedule.setVisibleField(false, "hospital", "doctor");
		tblSchedule.addField(0, new CCCustomField() {
			@Override
			public String getLabel() {
				return "Hospital";
			}

			@Override
			public Object getCustomView(Object object) {
				final EeSchedule schedule = (EeSchedule) object;
				A a = new A(schedule.getHospital().getName());
				CCZk.addListener(a, new EventListener() {
					public void onEvent(Event event) throws Exception {
						EeSearch search = getAttribute(EeSearch.class);
						search.setHospital(schedule.getHospital());
						new ZulHospitalProfileCtrl().init();
					}
				});
				return a;
			}
		});
		tblSchedule.setHqlFilters(byDoctor);
	}

	@Override
	protected void _initListener() {
		super._initListener();

		addListener(btnSearch, elSearch);
	}

	@Override
	protected void _doSearch() {
		EeSearch search = getAttribute(EeSearch.class);
		edtSearch.getValueFromEditor();

		new ZulSearchCtrl().init();
	}

	@Override
	protected void _doUpdateComponent() {
		EeSearch search = getAttribute(EeSearch.class);
		edtSearch.setValueToEditor(search);

		edtDoctor.setValueToEditor(search.getDoctor());
		setImageContent(imgPhoto, coalesce(search.getDoctor().getPhoto(), new EeConfigBo().getConfig().getPhotoDoctor()));

		CCRating rating = new CCRating(boxRating);
		rating.setRating(parseInt(calculate("totalRating/totalRater", search.getDoctor())));
		rating.getBox().setTooltiptext(getString4View(getValue(search.getDoctor(), "@totalRating/@totalRater")));

		tblSchedule.search();

		super._doUpdateComponent();
	}
}