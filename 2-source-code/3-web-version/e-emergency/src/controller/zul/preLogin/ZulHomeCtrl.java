package controller.zul.preLogin;

import static cococare.zk.CCSession.getAttribute;
import static cococare.zk.CCZk.addListener;
import static cococare.zk.CCZk.getBandbox;
import model.obj.emergency.EeDoctor;
import model.obj.emergency.EeHospital;
import model.obj.emergency.EeSearch;
import model.obj.emergency.EeEnum.Searching;

import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zul.Button;
import org.zkoss.zul.Radio;

import cococare.framework.model.obj.util.UtilProvince;
import cococare.framework.model.obj.util.UtilRegency;
import cococare.framework.zk.CFZkCtrl;
import cococare.zk.CCAutoComplete;
import cococare.zk.CCZk;

public class ZulHomeCtrl extends CFZkCtrl {
	private Radio rdoHospital;
	private Radio rdoDoctor;
	private CCAutoComplete bndAutoComplete;
	private Button btnSearch;

	@Override
	protected Class _getEntity() {
		return null;
	}

	@Override
	protected BaseFunction _getBaseFunction() {
		// to enable backward action / forward action
		CCZk.setCurrentDesktopBookmark();
		return BaseFunction.LIST_FUNCTION;
	}

	@Override
	protected void _initComponent() {
		super._initComponent();

		bndAutoComplete = new CCAutoComplete(getBandbox(getContainer(), "bndAutoComplete"));
	}

	@Override
	protected void _initListener() {
		super._initListener();

		EventListener elUpdateBndAutoComplete = new EventListener() {
			public void onEvent(Event event) throws Exception {
				_doUpdateBndAutoComplete();
			}
		};
		addListener(rdoHospital, elUpdateBndAutoComplete);
		addListener(rdoDoctor, elUpdateBndAutoComplete);
		addListener(btnSearch, elSearch);
	}

	private void _doUpdateBndAutoComplete() {
		bndAutoComplete.setText(null);
		if (rdoHospital.isChecked()) {
			bndAutoComplete.setEntity(EeHospital.class, UtilRegency.class, UtilProvince.class);
		} else if (rdoDoctor.isChecked()) {
			bndAutoComplete.setEntity(EeDoctor.class, UtilRegency.class, UtilProvince.class);
		}
		bndAutoComplete.search();
	}

	@Override
	protected void _doSearch() {
		EeSearch search = getAttribute(EeSearch.class);
		search.setSearching(rdoHospital.isChecked() ? Searching.HOSPITAL : Searching.DOCTOR);
		if (bndAutoComplete.getObject() instanceof UtilProvince) {
			search.setProvince((UtilProvince) bndAutoComplete.getObject());
		} else if (bndAutoComplete.getObject() instanceof UtilRegency) {
			search.setRegency((UtilRegency) bndAutoComplete.getObject());
		} else {
			search.setName(bndAutoComplete.getText());
		}

		new ZulSearchCtrl().init();
	}

	@Override
	protected void _doUpdateComponent() {
		super._doUpdateComponent();

		EeSearch search = getAttribute(EeSearch.class);
		rdoHospital.setSelected(Searching.HOSPITAL.equals(search.getSearching()));
		rdoDoctor.setSelected(Searching.DOCTOR.equals(search.getSearching()));
		_doUpdateBndAutoComplete();
	}
}