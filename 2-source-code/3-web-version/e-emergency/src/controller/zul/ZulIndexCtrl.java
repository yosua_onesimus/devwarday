package controller.zul;

import static cococare.zk.CCMessage.showError;
import static cococare.zk.CCZk.addListener;
import static cococare.zk.CCZk.initComponent;
import model.mdl.emergency.EmergencyMain;

import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zul.A;
import org.zkoss.zul.Button;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

import cococare.framework.model.obj.util.UtilUser;
import cococare.framework.zk.CFZkMain;
import cococare.framework.zk.controller.zul.util.ZulLoginCtrl;
import cococare.zk.CCEditor;

public class ZulIndexCtrl extends Window {
	private CCEditor edtLogin;
	private A aLogIn;
	// ---planning2 ---begin
	private Window winLogin;
	private Textbox txtUsername;
	private Textbox txtPassword;
	private Button btnLogIn;
	// ---planning2 ---end
	private A aLogOut;

	public void onCreate() {
		CFZkMain.start();
		initComponent(this, this, null);
		// ---planning2 ---begin
		// initComponent(winLogin, this, null);
		// edtLogin = new CCEditor(winLogin, UtilUser.class);
		// ---planning2 ---end
		EventListener elLogin = new EventListener() {
			public void onEvent(Event event) throws Exception {
				// ---planning1
				new ZulLoginCtrl().init();
				// ---planning2
				if (false && edtLogin.isValueValid()) {
					UtilUser user = edtLogin.getValueFromEditor();
					if (EmergencyMain.INSTANCE.login(user.getUsername(), user.getPassword())) {
						// show home screen
					} else {
						showError();
					}
				}
			}
		};
		// ---planning1
		addListener(aLogIn, elLogin);
		// ---planning2
		// addListener(txtUsername, elLogin);
		// addListener(txtPassword, elLogin);
		// addListener(btnLogIn, elLogin);
		addListener(aLogOut, new EventListener() {
			public void onEvent(Event event) throws Exception {
				EmergencyMain.INSTANCE.logout();
			}
		});
	}
}