package controller.zul.emergency;

import model.obj.emergency.EeHospitalGallery;
import cococare.framework.zk.CFZkCtrl;

public class ZulHospitalGalleryCtrl extends CFZkCtrl {
	@Override
	protected Class _getEntity() {
		return EeHospitalGallery.class;
	}

	@Override
	protected BaseFunction _getBaseFunction() {
		return BaseFunction.FORM_FUNCTION;
	}
}