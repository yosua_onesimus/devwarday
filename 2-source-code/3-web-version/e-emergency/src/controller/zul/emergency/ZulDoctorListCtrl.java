package controller.zul.emergency;

import model.bo.emergency.EeDoctorBo;
import model.obj.emergency.EeDoctor;
import cococare.framework.zk.CFZkCtrl;

public class ZulDoctorListCtrl extends CFZkCtrl {
	private EeDoctorBo doctorBo;

	@Override
	protected Class _getEntity() {
		return EeDoctor.class;
	}

	@Override
	protected BaseFunction _getBaseFunction() {
		return BaseFunction.LIST_FUNCTION;
	}

	@Override
	protected boolean _doDeleteEntity() {
		return doctorBo.deleteById((EeDoctor) _getSelectedItem());
	}
}