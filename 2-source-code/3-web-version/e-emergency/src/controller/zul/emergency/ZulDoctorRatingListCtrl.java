package controller.zul.emergency;

import model.obj.emergency.EeDoctorRating;
import cococare.framework.zk.CFZkCtrl;

public class ZulDoctorRatingListCtrl extends CFZkCtrl {
	@Override
	protected Class _getEntity() {
		return EeDoctorRating.class;
	}

	@Override
	protected BaseFunction _getBaseFunction() {
		return BaseFunction.LIST_FUNCTION;
	}
}