package controller.zul.emergency;

import model.bo.emergency.EeConfigBo;
import model.obj.emergency.EeDoctor;

import org.zkoss.zul.Checkbox;
import org.zkoss.zul.Textbox;

import cococare.common.CCFieldConfig.Accessible;
import cococare.framework.zk.CFZkCtrl;

public class ZulDoctorProfileCtrl extends CFZkCtrl {
	private EeConfigBo configBo;
	private Textbox txtCode;
	private Checkbox txtActive;
	private Checkbox txtPartnership;

	@Override
	protected Class _getEntity() {
		return EeDoctor.class;
	}

	@Override
	protected BaseFunction _getBaseFunction() {
		return BaseFunction.FORM_FUNCTION;
	}

	@Override
	protected void _initObject() {
		super._initObject();
		objEntity = configBo.getDoctorLogin();
	}

	@Override
	protected void _doUpdateAccessible() {
		super._doUpdateAccessible();
		edtEntity.setAccessible(txtCode, Accessible.MANDATORY_READONLY);
		edtEntity.setAccessible(txtActive, Accessible.MANDATORY_READONLY);
		edtEntity.setAccessible(txtPartnership, Accessible.MANDATORY_READONLY);
	}
}