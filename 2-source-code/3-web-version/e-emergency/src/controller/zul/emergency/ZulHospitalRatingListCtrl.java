package controller.zul.emergency;

import model.obj.emergency.EeHospitalRating;
import cococare.framework.zk.CFZkCtrl;

public class ZulHospitalRatingListCtrl extends CFZkCtrl {
	@Override
	protected Class _getEntity() {
		return EeHospitalRating.class;
	}

	@Override
	protected BaseFunction _getBaseFunction() {
		return BaseFunction.LIST_FUNCTION;
	}
}