package controller.zul.emergency;

import model.obj.emergency.EeHospitalRoomClass;
import cococare.framework.zk.CFZkCtrl;

public class ZulHospitalRoomClassCtrl extends CFZkCtrl {
	@Override
	protected Class _getEntity() {
		return EeHospitalRoomClass.class;
	}

	@Override
	protected BaseFunction _getBaseFunction() {
		return BaseFunction.FORM_FUNCTION;
	}
}