package controller.zul.emergency;

import model.bo.emergency.EeHospitalBo;
import model.obj.emergency.EeHospital;
import cococare.framework.zk.CFZkCtrl;

public class ZulHospitalListCtrl extends CFZkCtrl {
	private EeHospitalBo hospitalBo;

	@Override
	protected Class _getEntity() {
		return EeHospital.class;
	}

	@Override
	protected BaseFunction _getBaseFunction() {
		return BaseFunction.LIST_FUNCTION;
	}

	@Override
	protected boolean _doDeleteEntity() {
		return hospitalBo.deleteById((EeHospital) _getSelectedItem());
	}
}