package controller.zul.emergency;

import model.obj.emergency.EeHospitalRoomClass;
import cococare.framework.zk.CFZkCtrl;

public class ZulHospitalRoomClassListCtrl extends CFZkCtrl {
	@Override
	protected Class _getEntity() {
		return EeHospitalRoomClass.class;
	}

	@Override
	protected BaseFunction _getBaseFunction() {
		return BaseFunction.LIST_FUNCTION;
	}
}