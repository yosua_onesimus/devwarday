package controller.zul.emergency;

import model.obj.emergency.EeSchedule;
import cococare.framework.zk.CFZkCtrl;

public class ZulScheduleCtrl extends CFZkCtrl {
	@Override
	protected Class _getEntity() {
		return EeSchedule.class;
	}

	@Override
	protected BaseFunction _getBaseFunction() {
		return BaseFunction.FORM_FUNCTION;
	}
}