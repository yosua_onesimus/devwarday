package controller.zul.emergency;

import model.obj.emergency.EeHospitalPartnership;
import cococare.framework.zk.CFZkCtrl;

public class ZulHospitalPartnershipListCtrl extends CFZkCtrl {
	@Override
	protected Class _getEntity() {
		return EeHospitalPartnership.class;
	}

	@Override
	protected BaseFunction _getBaseFunction() {
		return BaseFunction.LIST_FUNCTION;
	}
}